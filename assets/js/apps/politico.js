'use strict';

var checkUrl = function(){
    var location = document.location,
        decodedHref = decodeURI(location.href);
    if(location.href != decodedHref){
        location.href = decodedHref;
    }
}
checkUrl();

angular.module('politico', [
        'ngRoute',
        'repolitica.server',
        'repolitica.crossdata',
        'repolitica.global',
        'repolitica.input',
        'repolitica.directives'
    ]).directive("ngFileSelect",function(){
        return {
            link: function($scope,el){
                el.bind("change", function(e){
                    $scope.getFile(el[0].files[0]);
                })
            }
        }
    }).
    config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) {

        //$locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');

        $routeProvider.when('/', {templateUrl: '/partials/politico.html', controller: 'politicoCtrl'});
        //$routeProvider.otherwise({redirectTo: '/home'});
        $routeProvider.when('/pergunta', {templateUrl: '/partials/pergunta.html', controller: 'perguntaCtrl'});
        $routeProvider.when('/:urlEdicao', {templateUrl: '/partials/edicao/template.html', controller: 'templateCtrl'});
    }])
    .run(function (Server, $rootScope, $location, $routeParams) {

        Server.getCidades(function (data) {
            $rootScope.locations = data;
        });

        Server.getEnum(function (data) {
            $rootScope.enum = data;
        });
    });