'use strict';


var checkUrl = function(){
    var location = document.location,
        decodedHref = decodeURI(location.href);
    if(location.href != decodedHref){
        location.href = decodedHref;
    }
}
checkUrl();

angular.module('repolitica', [
        'ngSanitize',
        'ngRoute',
        'ui.bootstrap',
        'repolitica.server',
        'repolitica.global',
        'repolitica.crossdata',
        'repolitica.directives'
    ]).
    config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) {

        //$locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');

        $routeProvider.when('/', {templateUrl: 'partials/home.html', controller: 'homeCtrl'});
        $routeProvider.when('/sem-politico', {templateUrl: 'partials/404.html', controller: 'inexCtrl'});
        $routeProvider.when('/lista', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/busca/:busca', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/lista/:estado', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/lista/partido/:partido', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/lista/filtro/:filtro/', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/lista/filtro/:filtro/:estado', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/lista/:estado/:cidade', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/lista/:estado/partido/:partido', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/lista/:estado/:cidade/:partido', {templateUrl: 'partials/lista.html', controller: 'listaCtrl'});
        $routeProvider.when('/teste', {templateUrl: 'partials/teste/teste.html', controller: 'testeCtrl'});
        $routeProvider.when('/teste/resultado', {templateUrl: 'partials/teste/resultado.html', controller: 'testeResultadoCtrl'});

        // --------------------- estáticas -------------------
        $routeProvider.when('/contato', {templateUrl: 'partials/estaticas/contato.html', controller: 'contatoCtrl'});
        $routeProvider.when('/na-midia', {templateUrl: 'partials/estaticas/namidia.html'});
        $routeProvider.when('/politicas-privacidade', {templateUrl: 'partials/estaticas/politicasdeprivacidade.html'});
        $routeProvider.when('/quem-somos', {templateUrl: 'partials/estaticas/quemsomos.html'});
        $routeProvider.when('/termos-uso', {templateUrl: 'partials/estaticas/termosdeuso.html'});
        $routeProvider.when('/como-usar', {templateUrl: 'partials/estaticas/comousar.html'});
        $routeProvider.when('/sobre', {templateUrl: 'partials/estaticas/oquee.html'});

        // --------------------- cadastro/login --------------
        $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'loginCtrl'});
        $routeProvider.when('/loginpolitico', {templateUrl: 'partials/loginpolitico.html', controller: 'loginPoliticoCtrl'});
        $routeProvider.when('/login/reset', {templateUrl: 'partials/esqueciminhasenha.html', controller: 'enviarResetPassCtrl'});
        $routeProvider.when('/login/reset/:userid/:token', {templateUrl: 'partials/resetpass.html', controller: 'resetPassCtrl'});
        $routeProvider.when('/cadastropolitico', {templateUrl: 'partials/cadastro/cadastropolitico.html', controller: 'cadastroPoliticoCtrl'});
        $routeProvider.when('/cadastropolitico/:polid', {templateUrl: 'partials/cadastro/cadastropolitico.html', controller: 'cadastroPoliticoCtrl'});
        $routeProvider.when('/cadastrocidadao', {templateUrl: 'partials/cadastro/cadastrocidadao.html', controller: 'cadastroCidadaoCtrl'});
        $routeProvider.when('/logout', {template: ' ', controller: 'logoutCtrl'});
        $routeProvider.when('/cadastrocidadao/confirmacao', {templateUrl: 'partials/cadastro/confirmacaoeleitor.html'});
        $routeProvider.when('/cadastrocidadao/confirmacao/:userid/:token', {templateUrl: 'partials/cadastro/validareleitor.html', controller: 'validarCadastroCtrl'});
        //$routeProvider.when('/cadastropolitico/confirmacao', {templateUrl: 'partials/cadastro/confirmacaopolitico.html'});
        //$routeProvider.when('/cadastropolitico/confirmacao/:userid/:token', {templateUrl: 'partials/cadastro/validaremailpolitico.html', controller: 'validarCadastroCtrl'});

        // --------------------- admin -----------------------
        $routeProvider.when('/admin/alias', {templateUrl: 'partials/admin/alias.html', controller: 'aliasCtrl'});
        $routeProvider.when('/admin/votos', {templateUrl: 'partials/admin/votos.html', controller: 'votosCtrl'});
        $routeProvider.when('/admin/blacklist', {templateUrl: 'partials/admin/blacklist.html', controller: 'blacklistCtrl'});
        $routeProvider.when('/admin/perguntas', {templateUrl: 'partials/admin/perguntas.html', controller: 'perguntasCtrl'});
        $routeProvider.when('/admin/testes', {templateUrl: 'partials/admin/testes.html', controller: 'testesCtrl'});
        $routeProvider.when('/admin/politico', {templateUrl: 'partials/admin/politico.html', controller: 'criadorPoliticoCtrl'});
        $routeProvider.when('/admin/mudancas', {templateUrl: 'partials/admin/mudancas.html', controller: 'mudancasCtrl'});
        $routeProvider.when('/admin/conflitos', {templateUrl: 'partials/admin/conflito.html', controller: 'conflitoCtrl'});
        $routeProvider.when('/admin/semestados', {templateUrl: 'partials/admin/semestados.html', controller: 'semestadosCtrl'});
        $routeProvider.when('/admin/naturalcidade', {templateUrl: 'partials/admin/naturalcidade.html', controller: 'naturalcidadeCtrl'});
        $routeProvider.when('/admin/merger', {templateUrl: 'partials/admin/mergerlista.html', controller: 'nomesiguaislistaCtrl'});
        $routeProvider.when('/admin/merger/:estado/:nome', {templateUrl: 'partials/admin/merger.html', controller: 'nomesiguaisCtrl'});
        $routeProvider.when('/admin/merger/:nome', {templateUrl: 'partials/admin/merger.html', controller: 'nomesiguaisCtrl'});
        $routeProvider.when('/admin/conflitolocal/:estado/:cidade/:urlpolitico', {templateUrl: 'partials/admin/conflitolocal.html', controller: 'conflitoLocalCtrl'});
        $routeProvider.when('/admin/conflitolocal/:estado/:urlpolitico', {templateUrl: 'partials/admin/conflitolocal.html', controller: 'conflitoLocalCtrl'});
        $routeProvider.when('/admin/conflitolocal/:urlpolitico', {templateUrl: 'partials/admin/conflitolocal.html', controller: 'conflitoLocalCtrl'});
        $routeProvider.when('/admin', {templateUrl: 'partials/admin/index.html', controller: 'adminCtrl'});

        //$routeProvider.when('/:politicoId', {templateUrl: 'partials/politico.html', controller: 'politicoCtrl'});

        //$routeProvider.otherwise({redirectTo: '/'});
    }])
    .run(function (Server, $rootScope, $location, $routeParams) {
        Server.getCidades(function (data) {
            $rootScope.locations = data;
        });

        Server.getEnum(function (data) {
            $rootScope.enum = data;
        });
    });
