'use strict';

var checkUrl = function(){
    var location = document.location,
        decodedHref = decodeURI(location.href);
    if(location.href != decodedHref){
        location.href = decodedHref;
    }
}
checkUrl();

angular.module('quemelegi', [
        'ngRoute',
        'repolitica.crossdata',
        'repolitica.server'
    ]).
    config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) {

        //$locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('!');

        $routeProvider.when('/', {templateUrl: '/partials/quemelegi/home.html', controller: 'quemelegihomeCtrl'});
        $routeProvider.when('/resultado', {templateUrl: '/partials/quemelegi/resultado.html', controller: 'quemelegiresultadoCtrl'});
    }])
    .run(function (Server, $rootScope, $location, $routeParams) {

        Server.getCidades(function (data) {
            $rootScope.locations = data;
        });

        Server.getEnum(function (data) {
            $rootScope.enum = data;
        });
    });