var globalService = angular.module('repolitica.global', ['repolitica.crossdata']);

globalService.factory("Global", ["$rootScope", "$http", "$window", "CrossData", function ($rootScope, $http, $window, CrossData) {

    var getCidade = function (estado, id, cb) {
        if (estado && id && id != -1 &&
            $rootScope.locations && $rootScope.locations[estado]) {
            cb($rootScope.locations[estado][id]);
        }
        else {
            cb({});
        }
    };

    return {
        loadCidades: function (estado, cb) {
            if (estado) {
                var cidades = $rootScope.locations[estado];
                var results = [];
                for (var cidadeId in cidades) {
                    if (cidades.hasOwnProperty(cidadeId))
                        results.push({ id: parseInt(cidadeId), nome: cidades[cidadeId].nome })
                }
                cb(results);
            }
            else {
                cb([]);
            }
        },
        getCidade: getCidade,
        getEnum: function(type){
            if (type) {
                cb($rootScope.enum[type]);
            }
            else {
                cb([]);
            }
        },
        updatePolitico: function(politico, userid, cb, errorcb){
            $http.post('/politicos/update', { politico:politico})
                .success(function (response) {
                    if (!response.error) {
                        cb();
                    }
                    else {
                        if(errorcb != null)
                            errorcb({error:response.error});
                    }
                }).error(function (response) {
                    if(errorcb != null)
                        errorcb({error: "Um erro inesperado ocorreu"});
                });
        },
        politico: function(cb){
            var polDiv = document.getElementById("politicodiv");
            var politico = JSON.parse(polDiv.innerHTML);

            var urlpolitico = politico.politicoid;
            var cidade = politico.cidade;
            var estado = politico.estado;
            var polid = politico.id;

            var key = "";
            if(polid) key = polid;
            else{
                if(estado) key += estado;
                if(cidade) key += cidade;
                key += urlpolitico;
            }
            var polData = CrossData.getPerfil(key);
            if(polData) return cb(polData);

            // vindo do AP
            var email = politico.email;
            var token = politico.token;

            $http.post('/politicos/getByPoliticoUrl',{
                    urlpolitico: urlpolitico,
                    cidade:cidade,
                    estado:estado,
                    perfil: true,
                    id: polid,
                    email: email,
                    token: token
                })
                .success(function (response) {
                    cb(response);
                }).error(function (response) {
                    $window.location.href = "/";
                });
        },
        linkPolitico: function(pol, cb){
            if(pol.cidadecandidatura && pol.cidadecandidatura != -1 && pol.estadocandidatura){
                getCidade(pol.estadocandidatura, pol.cidadecandidatura, function(c){
                    var cid = "";
                    if(c!=null){
                        cid = c.nome
                        if(cid == null) cid = "";
                        var re = new RegExp(' ', 'g');
                        cid = cid.replace(re, '-').toLowerCase();
                        cid = "/" + removeDiacritics(cid);
                    }
                    cb(pol.estadocandidatura.toLowerCase() + cid + "/" + pol.politicourl);
                })
            }
            else if(pol.estadocandidatura){
                cb(pol.estadocandidatura + "/" + pol.politicourl);
            }
            else {
                cb(pol.politicourl);
            }
        },
        buildPoliticoUrl: function (politico) {
            if(politico == null) return "";
            var url = "";
            if(politico.estadocandidatura) {
                url += "/" + politico.estadocandidatura;
                var cidade = politico.cidadecandidatura;
                if(cidade && cidade != -1) {
                    getCidade(politico.estadocandidatura, cidade, function(c){
                        if(c && c.nome){
                            var cid = c.nome.toLowerCase();
                            cid = removeDiacritics(cid);
                            cid = cid.replace(new RegExp(" ", 'g'), '-');
                            url += "/" + cid;
                        }
                        return url + "/" + politico.politicourl;

                    })
                }
                else
                    return url + "/" + politico.politicourl;
            }
            else
                return url + "/" + politico.politicourl;
        }
    };
}]);