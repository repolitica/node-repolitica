


var crossService = angular.module('repolitica.crossdata', []);

crossService.factory("CrossData", [function () {
    var savedData = {}
    function set(data) { savedData = data; }
    function get() { return savedData; }

    var pols = {
        "BR":{
            BR:{
                Presidente:[],
                "Vice-presidente":[],
            },
        },
        RJ:{
            RJ:{
                Governador:[]
            },
            "3658":{
                Prefeito:[]
            }
        }
    };
    function setCandidatos(ps, cargo,estado,cidade){
        if(!estado || cargo == "Presidente") {
            estado = "BR";
            cidade = "BR";
        }
        else if(!cidade) cidade = estado;

        if(pols[estado] == null) pols[estado] = {};
        if(pols[estado][cidade] == null) pols[estado][cidade] = {};
        if(pols[estado][cidade][cargo] == null) pols[estado][cidade][cargo] = {};
        pols[estado][cidade][cargo] = ps;
    }
    function getCandidatos(cargo,estado,cidade){
        if(!estado || cargo == "Presidente") {
            estado = "BR";
            cidade = "BR";
        }
        else if(!cidade) cidade = estado;

        if(pols[estado] == null) pols[estado] = {};
        if(pols[estado][cidade] == null) pols[estado][cidade] = {};
        if(pols[estado][cidade][cargo] == null) pols[estado][cidade][cargo] = [];

        return pols[estado][cidade][cargo];
    }

    var perfis = {};
    function getPerfil(key){
        return perfis[key];
    }
    function setPerfil(key,perfil){
        perfis[key] = perfil;
    }


    return {
        set: set,
        get: get,
        getCandidatos: getCandidatos,
        setCandidatos: setCandidatos,
        getPerfil: getPerfil,
        setPerfil: setPerfil,
    }

}]);