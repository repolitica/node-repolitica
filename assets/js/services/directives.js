angular.module('repolitica.directives', [])
    .directive('validateNumber', function() {
        return function(scope, element, attr) {

            element.on('keypress', function(evt) {
                var theEvent = evt || window.event;
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode( key );
                var regex = /[0-9]/;
                if(!regex.test(key)){
                    theEvent.returnValue = false;
                    if(theEvent.preventDefault) theEvent.preventDefault();
                }
            });
        };
    });