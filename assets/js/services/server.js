'use strict';

var serverService = angular.module('repolitica.server', []);

serverService.factory("Server", ["$rootScope", "$http", function ($rootScope, $http) {

    return {
        getEstados: function (cb) {
            $http.get('/staticdb/estados.json').success(function (data) {
                cb(data);
            });
        },
        getCidades: function (cb) {
            $http.get('/staticdb/cidades.json').success(function (data) {
                cb(data);
            });
        },
        getCep: function (cep, cb) {
            $http.post('/global/cep', { cep: cep }).success(function (data) {
                cb(data);
            });
        },
        getEnum: function (cb) {
            $http.get('/staticdb/enum.json').success(function (data) {
                cb(data);
            });
        },
        getPrioridades: function (cb) {
            $http.get('/staticdb/prioridades.json').success(function (data) {
                cb(data);
            });
        },
        getPartidos: function (extintos, cb) {
            $http.get('/staticdb/partidos.json').success(function (data) {
                if(!extintos){
                    for(var k in data){
                        if(data[k].extinto && data[k].extinto == "true")
                        delete data[k];
                    }
                }
                cb(data);
            });
        },
        getCargos: function (cb) {
            $http.get('/staticdb/cargos.json').success(function (data) {
                cb(data);
            });
        },
        getQuestoes: function (q,cb) {
            $http.post('/questoes/tipo', q).success(function (data) {
                cb(data);
            });
        },
        authenticated: function (cb) {
            // verifica se o usuário está logado
            $http({method: 'GET', url: '/auth/isauthenticated'}).
                success(function(data, status, headers, config) {
                    cb(data);
                }).
                error(function(data, status, headers, config) {
                    cb();
                });
        }
    };
}]);