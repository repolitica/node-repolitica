'use strict';


function testeCtrl($scope, $location, $http, $window, Server, Global, CrossData) {

    $scope.loadingFim = false;

    // tooltips
    var mainHelp = "Passe o mouse por cima das alternativas para ver a explicação.";
    $scope.help = mainHelp;
    $scope.hideTooltip = function(help) {
        if(help)
            $scope.help = help;
        else
            $scope.help = mainHelp;
    }

    $scope.showTooltip = function(tip) {
        if(tip)
            $scope.help = tip;
    }

    var info = CrossData.get();
    CrossData.set({});

    Server.getEstados(function (data) {
        var defEstado = { "sigla": "", "nome": "Nenhum" };
        if(!$scope.estado)
            $scope.estado = defEstado;
        data.splice(0, 0, defEstado)
        $scope.estados = data;
        $scope.cargos = cargosFederais;
        if(!$scope.cargo)
            $scope.cargo = "Presidente";

        if(info.estado)
            for(var i = 0; i < $scope.estados.length; i++){
                if(info.estado == $scope.estados[i].sigla)
                    $scope.estado = $scope.estados[i];
            }

        if(info.cargo)
            $scope.cargo = info.cargo;

        $scope.changeEstado();
    });

    var cargosFederais = [ "Presidente" ];
    var cargosEstaduais = [ "Presidente", "Governador", "Senador", "Deputado Federal", "Deputado Estadual" ];
    var cargosDF = [ "Presidente", "Governador", "Senador", "Deputado Federal", "Deputado Distrital" ];
    $scope.changeEstado = function(){
        /*
        Global.loadCidades($scope.estado.sigla,function(res){
            $scope.cidades = res;
        });
        */
        if($scope.estado && $scope.estado.sigla == "DF"){
            $scope.cargos = cargosDF;
        }
        else if($scope.estado && $scope.estado.sigla){
            $scope.cargos = cargosEstaduais;
        }
        else {
            $scope.cargos = cargosFederais;
            $scope.cargo = "Presidente";
        }
    }

    $scope.introForm = true;
    $scope.otherForms = false;
    $scope.finalForm = false;
    $scope.step = 2;

    var idTeste = "";
    $scope.secondForm = function(){
        var estado = ""
        if($scope.estado.sigla != null) estado = $scope.estado.sigla;
        else estado = $scope.estado;

        $http.post('/users/inicioTeste', { teste:{cargo: $scope.cargo, estado: estado} })
            .success(function (response) {
                if (!response.error) {
                    idTeste = response.id;
                }
                renderForm();
            }).error(function (response) {
                renderForm();
            });

    }

    // function to build forms
    var forms = [];
    var getEnum = function(cb){
        Server.getEnum(function (enumData) {

            var econ = {
                title: "Qual é a sua posição sobre a economia?",
                key:"economia",
                items:[]
            };
            for(var i in enumData["economia"]){
                econ.items.push({
                    text:enumData.economia[i].tipo,
                    value:i,
                    help:enumData.economia[i].descricao,
                });
            }
            forms.push(econ);

            var rad = {
                title: "Como defende suas idéias?",
                key:"radicalismo",
                items:[]
            };
            for(var i in enumData["radicalismo"]){
                rad.items.push({
                    text:enumData.radicalismo[i].tipo,
                    value:i,
                    help:enumData.radicalismo[i].descricao,
                });
            }
            forms.push(rad);

            var soc = {
                title: "Postura sobre a sociedade",
                key:"sociedade",
                items:[]
            };
            for(var i in enumData["sociedade"]){
                soc.items.push({
                    text:enumData.sociedade[i].tipo,
                    value:i,
                    help:enumData.sociedade[i].descricao,
                });
            }
            forms.push(soc);

            async.auto({
                    prioridades: function(cback){
                        Server.getPrioridades(function(prioridades){

                            var p1 = {
                                title: "Principal prioridade",
                                key: "prioridade1",
                                items: []
                            };
                            var p2 = {
                                title: "Outra prioridade",
                                key: "prioridade2",
                                items: []
                            };

                            var its = [];
                            for(var i in prioridades){
                                its.push({
                                    text: prioridades[i].prioridade,
                                    value: i,
                                    help: prioridades[i].texto
                                });
                            }

                            p1.items = its;
                            p2.items = its;
                            forms.push(p1);
                            forms.push(p2);

                            cback();
                        })
                    },
                    questoes: ['prioridades', function(cback){

                        var q = {};

                        if($scope.cargo != "Senador" && $scope.cargo != "Deputado Federal" && $scope.cargo != "Presidente"){
                            if($scope.estado && $scope.estado.sigla) q.estado = $scope.estado.sigla;
                            if($scope.cidade) q.cidade = $scope.cidade;
                        }

                        $http.post('/questoes/teste', q).success(function (qs) {
                            /*
                            var count = qs.length;

                            var is = _.range(count);
                            var selQs = [];
                            var qsLength = 12;
                            if(count < qsLength) qsLength = count;

                            while(selQs.length != qsLength){
                                var selQ = _.random(0, is.length - 1);
                                selQs.push(is.splice(selQ,1)[0]);
                            }

                            for(var i = 0; i < selQs.length; i++){
                                var f = {
                                    title: qs[selQs[i]].titulo,
                                    key: qs[selQs[i]]._id,
                                    help: qs[selQs[i]].texto,
                                    items: []
                                };

                                for(var j in enumData["questoes"]){
                                    if(j == "0") continue;
                                    f.items.push({
                                        text: enumData.questoes[j],
                                        value: j
                                    });
                                }
                                forms.push(f);
                            }
                             */

                            for(var i = 0; i < qs.length; i++){
                                var f = {
                                    title: qs[i].titulo,
                                    key: qs[i]._id,
                                    help: qs[i].texto,
                                    items: []
                                };

                                for(var j in enumData["questoes"]){
                                    if(j == "0") continue;
                                    f.items.push({
                                        text: enumData.questoes[j],
                                        value: j
                                    });
                                }
                                forms.push(f);
                            }
                            cback();


                        });

                    }],
                },
                function(err ,r){
                    $scope.total = forms.length + 1;
                    cb();
                }
            );

        });
    }

    $scope.answers = {};
    $scope.showFim = false;
    $scope.nextForm = function(value){
        $scope.answers[forms[$scope.step - 2].key] = value;
        $scope.showFim = true;
        if($scope.step < $scope.total){
            $scope.step++;
            renderForm();
        }
        else $scope.finishTest();
    }

    var candOk = false;
    var renderForm = function(){
        var render = function(){
            var form = forms[$scope.step - 2];
            $scope.title = form.title;
            $scope.items = form.items; // [{ text,value,help  }]
            $scope.qHelp = form.help;
            $scope.help = form.help;

            $scope.introForm = false;
            $scope.otherForms = true;
            $scope.loading = false;
        }
        // passagem do primeiro para o segundo form
        if(forms.length == 0) {
            $scope.loading = true;
            getEnum(function(){
                $scope.$apply(function(){
                    render();
                });
            });

            // pegando os candidatos assincronamente
            var cands = $scope.cargo != "Presidente" ? CrossData.getCandidatos($scope.cargo, $scope.estado.sigla):
                CrossData.getCandidatos($scope.cargo);
            var ultCand;
            var getCand = function(){
                var q = {
                    anocandidatura: 2014,
                    cargocandidatura: $scope.cargo
                };

                if($scope.cargo != "Presidente"){
                    if($scope.estado) q.estadocandidatura = $scope.estado.sigla;
                    if($scope.cidade) q.cidadecandidatura = $scope.cidade;
                }

                var ultTexto = "";
                var limit = 100;
                var cands = [];
                var buscaBanco = function(){
                    var postVar = {query:q, limit: limit};
                    if(ultTexto) postVar.ultTexto = ultTexto;
                    $http.post('/politicos/buscaSimples', postVar).success(function (ps) {
                        var cP = ps.length;
                        ultTexto = ps[cP-1].textobusca;
                        cands = cands.concat(ps);
                        ps = null;

                        if(cP < limit) fimBusca();
                        else buscaBanco();
                    });
                }

                var fimBusca = function(){
                    if($scope.cargo.indexOf("Deputado") > -1){
                        $http.post('/politicos/coligacoes', {estado: $scope.estado.sigla, cargo: $scope.cargo}).success(function (cs) {
                            cands = cands.concat(cs);
                            CrossData.setCandidatos(cands,$scope.cargo,$scope.estado.sigla,$scope.cidade);
                            candOk = true;
                        });
                    }
                    else{
                        if($scope.cargo == "Presidente")
                            CrossData.setCandidatos(cands,$scope.cargo);
                        else
                            CrossData.setCandidatos(cands,$scope.cargo,$scope.estado.sigla,$scope.cidade);
                        candOk = true;
                    }
                }
                buscaBanco();
            }
            if(cands.length == 0){
                getCand();
            }
            else candOk = true;
        }
        else render();
    }

    $scope.returnForm = function(){
        $scope.step--;
        if($scope.step == 1){
            $scope.showFim = false;

            $scope.introForm = true;
            $scope.otherForms = false;
            $scope.finalForm = false;
            $scope.step = 2;
        }
        else renderForm();
    }

    $scope.finishTest = function(){
        if($scope.loadingFim) return;
        $scope.loadingFim = true;
        var finishTest = function(){
            CrossData.set({answers: $scope.answers, estado: $scope.estado.sigla, cargo: $scope.cargo, cidade: $scope.cidade, idTeste: idTeste});
            $location.path('/teste/resultado');
        }
        var waitToFinish = function(){
            setTimeout(function(){
                if(candOk) finishTest();
                else waitToFinish();
            }, 500);
        }

        if(candOk) finishTest();
        else waitToFinish();
    }

    // previously answered on home
    if(info.cargo && info.estado) {
        $scope.loadingHome = true;
        $scope.cargo = info.cargo;
        if(info.estado && info.estado != "Nenhum")
            $scope.estado = info.estado;
        $scope.secondForm();
    }
}

function testeResultadoCtrl($scope, $location, $http, $window, Server, Global, CrossData) {

    $scope.showCompartilhar = true;
    $scope.loading = false;

    var info = CrossData.get();
    CrossData.set({});

    var pols = CrossData.getCandidatos(info.cargo,info.estado,info.cidade);

    if(pols.length == 0) {
        $window.location.href = "/teste";
        return;
    }

    var cargosFederais = [ "Presidente" ];
    var cargosEstaduais = [ "Presidente", "Governador", "Senador", "Deputado Federal", "Deputado Estadual" ];
    var cargosDF = [ "Presidente", "Governador", "Senador", "Deputado Federal", "Deputado Distrital" ];
    Server.getEstados(function (data) {
        $scope.estado = { "sigla": "", "nome": "Nenhum" };
        data.splice(0, 0, $scope.estado)
        $scope.estados = data;

        if(info.estado){
            for(var i = 0; i < $scope.estados.length; i++){
                if(info.estado == $scope.estados[i].sigla)
                    $scope.estado = $scope.estados[i];
            }
            if(info.estado != "DF")
                $scope.cargos = cargosEstaduais;
            else $scope.cargos = cargosDF;
        }
        else $scope.cargos = cargosFederais;

        if(info.cargo)
            $scope.cargo = info.cargo;
    });

    var federais = [];
    var gerais = [];
    var estaduais = {};

    var lerQuestoes = function(){

        var buscar = false;
        var query = {};
        if(gerais.length == 0){
            buscar = true;
            query.gerais = true;
        }

        if(info.cargo == "Presidente" || info.cargo == "Senador" || info.cargo == "Deputado Federal"){
            if(federais.length == 0){
                query.federais = true;
                buscar = true;
            }
        }
        else {
            if((estaduais[info.estado] || []).length == 0){
                query.estado = info.estado;
                buscar = true;
            }
        }

        if(buscar)
            $http.post('/questoes/resultadoTeste', query).success(function (qs) {
                if(federais.length == 0) federais = qs.federais;
                if(gerais.length == 0) gerais = qs.gerais;
                if(qs.estaduais && (qs.estaduais[query.estado] || []).length > 0 &&
                    (estaduais[query.estado] || []).length == 0) estaduais[query.estado] = qs.estaduais[query.estado];
                setQs();
            });
        else setQs();
    }

    var setQs = function(){
        $scope.questoes = gerais;
        if($scope.estado && $scope.estado.sigla &&
            info.cargo != "Presidente" && info.cargo != "Senador" && info.cargo != "Deputado Federal"){
            if(estaduais[$scope.estado.sigla])
                $scope.questoes = $scope.questoes.concat(estaduais[$scope.estado.sigla]);
        }
        else
            $scope.questoes = $scope.questoes.concat(federais);
    }
    lerQuestoes();

    var getCand = function(){
        var q = { anocandidatura: 2014, cargocandidatura: $scope.cargo };

        if($scope.cargo != "Presidente"){
            if($scope.estado) q.estadocandidatura = $scope.estado.sigla;
            if($scope.cidade) q.cidadecandidatura = $scope.cidade;
        }



        var ultTexto = "";
        var limit = 100;
        var cands = [];
        var buscaBanco = function(){
            var postVar = {query:q, limit: limit};
            if(ultTexto) postVar.ultTexto = ultTexto;
            $http.post('/politicos/buscaSimples', postVar).success(function (ps) {
                var cP = ps.length;
                ultTexto = ps[cP-1].textobusca;
                cands = cands.concat(ps);
                ps = null;

                if(cP < limit) fimBusca();
                else buscaBanco();
            });
        }

        var fimBusca = function(){
            if($scope.cargo.indexOf("Deputado") > -1){
                $http.post('/politicos/coligacoes', {estado: $scope.estado.sigla, cargo: $scope.cargo}).success(function (cs) {
                    cands = cands.concat(cs);
                    pols = cands;
                    CrossData.setCandidatos(cands,$scope.cargo,$scope.estado.sigla,$scope.cidade);
                    calcPols();
                    initPag();
                    $scope.loading = false;
                });
            }
            else{
                pols = cands;
                if($scope.cargo == "Presidente")
                    CrossData.setCandidatos(cands,$scope.cargo);
                else
                    CrossData.setCandidatos(cands,$scope.cargo,$scope.estado.sigla,$scope.cidade);
                calcPols();
                initPag();
                $scope.loading = false;
            }
        }
        buscaBanco();
    }

    $scope.changeCargoLocal = function(){
        /*
         Global.loadCidades($scope.estado.sigla,function(res){
         $scope.cidades = res;
         });
         */
        if($scope.estado && $scope.estado.sigla == "DF"){
            $scope.cargos = cargosDF;
        }
        else if($scope.estado && $scope.estado.sigla){
            $scope.cargos = cargosEstaduais;
        }
        else {
            $scope.cargos = cargosFederais;
            $scope.cargo = "Presidente";
        }

        if(info.cargo != $scope.cargo || info.cargo != "Presidente"){
            $scope.loading = true;
            $scope.politicos = [];

            pols = $scope.cargo != "Presidente" ? CrossData.getCandidatos($scope.cargo, $scope.estado.sigla):
                CrossData.getCandidatos($scope.cargo);

            if(pols.length == 0) getCand();
            else {
                calcPols();
                initPag();
                $scope.loading = false;
            }
        }
        info.cargo = $scope.cargo;
        info.estado = $scope.estado.sigla;
        lerQuestoes();
    }


    Server.getPrioridades(function(prioData){
        $scope.prioridades = [{key:"", value:"Não sei"}];
        for(var key in prioData){
            var d = prioData[key];
            $scope.prioridades.push({key:key, value: d.prioridade});
        }
    });

    var ecoBase = 200;
    var radBase = 50;
    var socBase = 100;
    var qsBase = 100;
    var prio1 = 150;
    var prio2 = 100;

    // coligacoes devem entrar normalizadas
    var setPontos = function(pol){
        var teste = 0;

        //---- economia
        if(pol.economia != null && answers.economia != null){
            var polResp = parseInt(pol.economia);
            var usResp = parseInt(answers.economia);
            var dif = Math.abs(polResp - usResp);
            teste += ecoBase * (1 - (dif / 2) );
        }

        //---- radicalismo
        if(pol.radicalismo != null && answers.radicalismo != null){
            var polResp = parseInt(pol.radicalismo);
            var usResp = parseInt(answers.radicalismo);
            var dif = Math.abs(polResp - usResp);
            teste += radBase * (1 - dif);
        }

        //---- sociedade
        if(pol.sociedade != null && answers.sociedade != null){
            var polResp = parseInt(pol.sociedade);
            var usResp = parseInt(answers.sociedade);
            var dif = Math.abs(polResp - usResp);
            teste += socBase * (1 - dif);
        }

        //---- questoes
        var qs = false
        for(var id in pol.questoes){
            qs = true;
            if(!answers[id]) continue;

            var polResp = 0;
            var dif = 0;
            var usResp = parseInt(answers[id]);
            if(pol.questoes[id] != null){
                if(pol.questoes[id].resposta != null)
                    polResp = parseInt(pol.questoes[id].resposta);
                else
                    polResp = parseInt(pol.questoes[id]);
                dif = Math.abs(polResp - usResp);
            }

            if(polResp == 0) continue;

            if(usResp == 1 || usResp == 4){
                if(dif < 1)
                    teste += qsBase * (1 - dif / 2);
                else if(dif > 2)
                    teste += qsBase * (1 / 2 - dif / 2);
                else teste += qsBase * (3 / 2 - dif);
            }
            // resposta moderada
            else{
                // resposta contrária
                if(dif > 1) teste += qsBase * (1 / 2 - dif / 2);
                else if (dif == 0) teste += qsBase;
                else if (usResp == 2 && polResp < 2 ||
                    usResp == 3 && polResp > 3)
                    teste += qsBase * (1 - dif / 2);
                else teste += qsBase * (1 - dif);
            }

        }

        //---- prioridades
        if(pol.prioridades){
            if(pol.prioridades.length > 0 && pol.prioridades[0] != null){
                var polResp = parseInt(pol.prioridades[0]);
                if(answers.prioridade1 != null){
                    var usResp = parseInt(answers.prioridade1);
                    if(polResp == usResp) teste += prio1;
                }
                if(answers.prioridade2 != null){
                    var usResp = parseInt(answers.prioridade2);
                    if(polResp == usResp) teste += prio2;
                }
            }
            if(pol.prioridades.length > 1 && pol.prioridades[1] != null){
                var polResp = parseInt(pol.prioridades[1]);
                if(answers.prioridade1 != null){
                    var usResp = parseInt(answers.prioridade1);
                    if(polResp == usResp) teste += prio2;
                }
                if(answers.prioridade2 != null){
                    var usResp = parseInt(answers.prioridade2);
                    if(polResp == usResp) teste += prio2;
                }
            }
        }
        // pol.prioridadeN = { "4":0.73, "5": 0.05}
        else {
            if(pol.prioridade1){
                if(answers.prioridade1 != null && pol.prioridade1[answers.prioridade1.toString()] != null)
                    teste += prio1 * pol.prioridade1[answers.prioridade1.toString()];
                if(answers.prioridade2 != null && pol.prioridade1[answers.prioridade2.toString()] != null)
                    teste += prio2 * pol.prioridade1[answers.prioridade2.toString()];
            }
            if(pol.prioridade2){
                if(answers.prioridade1 != null && pol.prioridade2[answers.prioridade1.toString()] != null)
                    teste += prio2 * pol.prioridade2[answers.prioridade1.toString()];
                if(answers.prioridade2 != null && pol.prioridade2[answers.prioridade2.toString()] != null)
                    teste += prio2 * pol.prioridade2[answers.prioridade2.toString()];
            }
        }

        if(pol.economia == null && pol.radicalismo == null && pol.sociedade == null &&
            !pol.questoes && !pol.prioridades && !pol.prioridade1 && !pol.prioridade2 && !qs)
            teste = -1000000;
        return teste;
    }

    var partidos = { };
    var answers = info.answers;
    var polSort = [];
    var calcPols = function(){
        var maxPts = 0;
        var minPts = 0;

        // cálculo dos limites
        for(var k in answers){
            switch(k){
                case "economia":
                    maxPts += ecoBase;
                    minPts -= ecoBase;
                    break;
                case "radicalismo":
                    maxPts += radBase;
                    minPts -= radBase;
                    break;
                case "sociedade":
                    maxPts += socBase;
                    minPts -= socBase;
                    break;
                case "prioridade1":
                    maxPts += prio1;
                    break;
                case "prioridade2":
                    maxPts += prio2;
                    break;
                default:
                    maxPts += qsBase;
                    minPts -= qsBase;
                    break;
            }
        }
        partidos = {
            // PARTIDO:{ economia:{"0":12, "1": 17}, questoes:{"fe123a2...": {"1": 5}} }
        };

        for(var i = 0; i < pols.length; i++){
            if(pols[i].nomecandidatura == null || pols[i].partidocandidatura == null) continue;
            var pol = pols[i];
            var partido = pol.partidocandidatura;

            // salvar o consolidado dos partidos
            if(partidos[partido] == null)
                partidos[partido] = {
                    economia: { "0": 0, "1": 0, "2": 0, "3": 0, "4": 0 },
                    radicalismo: { "0": 0, "1": 0, "2": 0 },
                    sociedade: { "0": 0, "1": 0, "2": 0 },
                    prioridade1: { "0": 0, "1": 0, "2": 0, "3": 0 , "4": 0 , "5": 0 , "6": 0 , "7": 0 , "8": 0 , "9": 0 , "10": 0  },
                    prioridade2: { "0": 0, "1": 0, "2": 0, "3": 0 , "4": 0 , "5": 0 , "6": 0 , "7": 0 , "8": 0 , "9": 0 , "10": 0  },
                    questoes: {}
                };
            if(pol.economia != null) partidos[partido].economia[pol.economia.toString()]++;
            if(pol.radicalismo != null) partidos[partido].radicalismo[pol.radicalismo.toString()]++;
            if(pol.sociedade != null) partidos[partido].sociedade[pol.sociedade.toString()]++;
            if(pol.prioridades != null && pol.prioridades[0]) partidos[partido].prioridade1[pol.prioridades[0].toString()]++;
            if(pol.prioridades != null && pol.prioridades[1]) partidos[partido].prioridade2[pol.prioridades[1].toString()]++;
            for(var q in pol.questoes){
                if(partidos[partido].questoes[q] == null) partidos[partido].questoes[q] = { "0": 0, "1": 0, "2": 0, "3": 0, "4": 0 };
                partidos[partido].questoes[q][pol.questoes[q].resposta.toString()]++;
            }

            pol.teste = setPontos(pol);

            // texto do cargo atual
            // Governador - Rio de Janeiro, RJ - PMDB
            pol.textoCargoLocal = "";
            if(pol.cargo && pol.cargo != "Nenhum") pol.textoCargoLocal = pol.cargo;
            if(pol.textoCargoLocal && (pol.cidade || pol.estado))
                pol.textoCargoLocal += " - ";

            var callback = function(){
                var partido = pol.partido || pol.partido || "";
                if(pol.textoCargoLocal && partido) pol.textoCargoLocal += " - ";
                if(partido) pol.textoCargoLocal += partido;
                pol.textoCargoLocal = pol.textoCargoLocal.replace(" -  - ", " - ");
            }
            if(pol.cidade && pol.cidade != -1 && pol.estado){
                Global.getCidade(pol.estado, pol.cidade, function(c){
                    var cidade = "";
                    if(c!=null) cidade = c.nome + ", ";
                    pol.textoCargoLocal += cidade + pol.estado;
                    callback();
                })
            }
            else {
                if(pol.estado) pol.textoCargoLocal += pol.estado
                callback();
            }
        }

        // cálculo da pontuação das coligações
        for(var i = 0; i < pols.length; i++){
            if(pols[i].nomecandidatura != null ||
               pols[i].partidos == null || pols[i].partidos.length == 0) continue;

            var colg = {
                economia: { "0": 0, "1": 0, "2": 0, "3": 0, "4": 0 },
                radicalismo: { "0": 0, "1": 0, "2": 0 },
                sociedade: { "0": 0, "1": 0, "2": 0 },
                prioridade1: { "0": 0, "1": 0, "2": 0, "3": 0 , "4": 0 , "5": 0 , "6": 0 , "7": 0 , "8": 0 , "9": 0 , "10": 0  },
                prioridade2: { "0": 0, "1": 0, "2": 0, "3": 0 , "4": 0 , "5": 0 , "6": 0 , "7": 0 , "8": 0 , "9": 0 , "10": 0  },
                questoes: {}
            };

            // consolidar o perfil da coligação
            for(var j = 0; j < pols[i].partidos.length; j++){
                var partido = partidos[pols[i].partidos[j]];
                if(partido == null) continue;

                for(var r in partido.economia)
                    colg.economia[r] += partido.economia[r];
                for(var r in partido.radicalismo)
                    colg.radicalismo[r] += partido.radicalismo[r];
                for(var r in partido.sociedade)
                    colg.sociedade[r] += partido.sociedade[r];
                for(var r in partido.prioridade1)
                    colg.prioridade1[r] += partido.prioridade1[r];
                for(var r in partido.prioridade2)
                    colg.prioridade2[r] += partido.prioridade2[r];

                for(var q in partido.questoes){
                    if(colg.questoes[q] == null) colg.questoes[q] = { "1": 0, "2": 0, "3": 0, "4": 0 };
                    for(var r in partido.questoes[q]){
                        if(parseInt(r) == 0) continue;
                        colg.questoes[q][r] += partido.questoes[q][r];
                    }
                }
            }

            // perfil do teste
            var total = 0;
            var soma = 0;
            for(var r in colg.economia){
                total += colg.economia[r];
                soma += parseInt(r) * colg.economia[r];
            }
            if(total > 0)
                pols[i].economia = soma / total;

            total = 0;
            soma = 0;
            for(var r in colg.radicalismo){
                total += colg.radicalismo[r];
                soma += parseInt(r) * colg.radicalismo[r];
            }
            if(total > 0)
                pols[i].radicalismo = soma / total;

            total = 0;
            soma = 0;
            for(var r in colg.sociedade){
                total += colg.sociedade[r];
                soma += parseInt(r) * colg.sociedade[r];
            }
            if(total > 0)
                pols[i].sociedade = soma / total;

            for(var q in colg.questoes){
                if(pols[i].questoes ==null) pols[i].questoes = {};
                total = 0;
                soma = 0;
                for(var r in colg.questoes[q]){
                    total += colg.questoes[q][r];
                    soma += parseInt(r) * colg.questoes[q][r];
                }
                if(total > 0)
                    pols[i].questoes[q] = soma / total;
            }

            // pol.prioridadeN = { "4":0.73, "5": 0.05}
            total = 0;
            pols[i].prioridade1 = { "0": 0, "1": 0, "2": 0, "3": 0 , "4": 0 , "5": 0 , "6": 0 , "7": 0 , "8": 0 , "9": 0 , "10": 0  };
            for(var r in colg.prioridade1){
                if(colg.prioridade1[r] <= 0) continue;
                total += colg.prioridade1[r];
                if(pols[i].prioridade1[r] == null) pols[i].prioridade1[r] = 0;
                pols[i].prioridade1[r] += colg.prioridade1[r];
            }
            if(total > 0){
                for(var r in colg.prioridade1)
                    pols[i].prioridade1[r] /= total;
            }
            else delete pols[i].prioridade1;

            total = 0;
            pols[i].prioridade2 = { "0": 0, "1": 0, "2": 0, "3": 0 , "4": 0 , "5": 0 , "6": 0 , "7": 0 , "8": 0 , "9": 0 , "10": 0  };
            for(var r in colg.prioridade2){
                if(colg.prioridade2[r] <= 0) continue;
                total += colg.prioridade2[r];
                if(pols[i].prioridade2[r] == null) pols[i].prioridade2[r] = 0;
                pols[i].prioridade2[r] += colg.prioridade2[r];
            }
            if(total > 0){
                for(var r in colg.prioridade2)
                    pols[i].prioridade2[r] /= total;
            }
            else delete pols[i].prioridade2;

            pols[i].composicao = pols[i].partidos[0];
            for(var p = 1; p < pols[i].partidos.length; p++)
                pols[i].composicao += " / " + pols[i].partidos[p];

            pols[i].teste = setPontos(pols[i]);
        }

        var polsPont = [];
        var polsSemPont = [];
        for(var i = 0; i < pols.length; i++){
            var pol = pols[i];
            if(pol.teste > -1000000)
                polsPont.push(pol)
            else polsSemPont.push(pol);
        }

        polSort = _.sortBy(polsPont, function(pol){ return -pol.teste; });

        if(polsSemPont.length > 0){
            polSort.push({});
            polsSemPont = _.sortBy(polsSemPont, function(pol){ return pol.nomecandidatura; });
            polSort = polSort.concat(polsSemPont);
        }
        polsSemPont = null;
        polsPont = null;

        if(polSort.length > 10)
            $scope.politicos = _.first(polSort, 10);
        else
            $scope.politicos = polSort;

        $scope.maxPts = maxPts;
        $scope.minPts = minPts;
        for(var p = 0; p < polSort.length; p++){
            if(!polSort[p].nomecandidatura) continue;
            $scope.numeroUm = polSort[p];
            break;
        }

        var resultado = [];
        for(var i = 0; i < $scope.politicos.length; i++){
            resultado.push($scope.politicos[i].id || $scope.politicos[i]._id);
        }

        $http.post('/users/fimTeste', { teste:{respostas: info.answers, id: info.idTeste, resultado: resultado} });
    }

    $scope.Math = Math;
    $scope.parseInt = parseInt;
    $scope.answers = answers;
    calcPols();
    var tamPag = 18;

    var initPag = function(){
        $scope.pagina = 1;
        $scope.total = polSort.length;
        $scope.totalPagina = Math.ceil(polSort.length/10);
        var ultPag = $scope.totalPagina > tamPag ? tamPag : $scope.totalPagina;
        $scope.pags = _.range(1, ultPag + 1);
        $scope.paddingIndex = 0;
    }
    initPag();

    $scope.mudarPagina = function(pag){
        if(pag == 0) return;
        $scope.paddingIndex = 10 * (pag - 1);
        if(_.first($scope.pags) > pag){
            var ult = pag + tamPag - 1;
            var primPag = pag;
            if($scope.total < ult) ult = $scope.total;

            // ajuste de layout
            if(ult >= 1011) ult -= 7;
            else if(ult >= 1005) ult -= 6;
            else if(ult >= 113) ult -= 5;
            else if(ult >= 109) ult -= 4;
            else if(ult >= 100) ult -= 3;
            else if(ult >= 25) ult -= 2;
            else if(ult >= 21) ult -= 1;
            $scope.pags = _.range(primPag, ult + 1);
        }
        else if(_.last($scope.pags) < pag){
            var prim = pag - tamPag + 1;
            var ult = prim + tamPag - 1;
            if(prim < 1) {
                var ultPag = $scope.totalPagina > tamPag ? tamPag : $scope.totalPagina;
                $scope.pags = _.range(1, ultPag + 1);
            }
            else {
                // ajuste de layout
                if(ult >= 1011) prim += 7;
                else if(ult >= 1005) prim += 6;
                else if(ult >= 113) prim += 5;
                else if(ult >= 109) prim += 4;
                else if(ult >= 100) prim += 3;
                else if(ult >= 25) prim += 2;
                else if(ult >= 21) prim += 1;
                $scope.pags = _.range(prim, ult + 1);
            }
        }
        $scope.pagina = pag;
        $scope.politicos = polSort.slice((pag-1) * 10, pag * 10);
    }

    $scope.mudarIdeologia = function(resp,ideologia){

        if(resp != -1){
            $scope.answers[ideologia] = resp;
            answers[ideologia] = resp;
        }
        else{
            delete answers[ideologia];
            delete $scope.answers[ideologia];
        }

        calcPols();
        initPag();
    }

    var twitter = function(texto){
        $http.post('/users/compartilhouTeste', { teste:{twitter: true, id: info.idTeste} });
        window.open("https://twitter.com/home?status=" + encodeURI(texto));
    }

    var facebook = function(img,name,description){
        $http.post('/users/compartilhouTeste', { teste:{facebook: true, id: info.idTeste} });

        var obj = {
            method: 'feed',
            link: 'https://repolitica.com.br/teste',
            display: 'popup',
            picture: img,
            // primeira linha
            name: name,
            // segunda linha
            caption: ' ',
            // terceira linha
            description: description
        };

        function callback(response) {
        }

        FB.ui(obj, callback);
    }

    $scope.shareTwitter = function(){
        var cargo = $scope.numeroUm.cargocandidatura;
        cargo = cargo.replace("Deputado", "Dep.");
        var nome = $scope.numeroUm.nomecandidatura;
        twitter("O candidato a " + cargo + " mais compatível comigo no teste do Repolítica foi " + nome + "! repolitica.com.br/teste");
    }

    $scope.shareFB = function(){

        var cargo = $scope.numeroUm.cargocandidatura;
        var nome = $scope.numeroUm.nomecandidatura;
        var img = $scope.numeroUm.imagemurl;
        facebook(img,
            'Pra mim deu ' + nome + '!<center></center>Qual é o candidato a ' + cargo + ' mais compatível com você?',
            "Fiz o teste de compatibilidade do Repolítica para descobrir os candidatos que pensam mais parecido comigo, e o resultado foi " +
                nome + "!");
    }

    $scope.shareTesteFB = function(){
        facebook("https://www.repolitica.com.br/img/compartilhamento-teste.jpg", 'Teste de compatibilidade política',
            "Responda 20 perguntas e descubra quais os candidatos mais compatíveis com você.");
    }

    $scope.shareTesteTwitter = function(){
        twitter("Responda 20 perguntas e descubra quais os candidatos mais compatíveis com você. repolitica.com.br/teste");
    }

    $scope.pergunte = function(url){
        window.location.href = url + "#!/pergunta";
    }
}