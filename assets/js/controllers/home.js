'use strict';


function homeCtrl($scope, $location, $window, Server, Global, CrossData, $http) {
    $scope.teste = "Home!";

    $scope.questoes = [];

    $http.post('/questoes/home')
        .success(function (response) {
            if (!response.error) {
                for(var i = 0; i < 6; i++){
                    var f = Math.floor((Math.random() * response.length));
                    if(f == response.length) f--;
                    $scope.questoes.push(response.splice(f,1)[0]);
                }
            }
        }).error(function (response) {
        });
    $scope.estadoCadastrado = {sigla: "SP"};
    $scope.ultCad = function(cbFunction){
        $http.post('/politicos/ultimosCadastrados',{limit: 4, estado:$scope.estadoCadastrado.sigla})
            .success(function (response) {
                if (!response.error) {

                    for(var i = 0; i < response.politicos.length; i++){
                        var pol = response.politicos[i];
                        // *********** candidatura ************
                        var anoAtual = new Date().getFullYear();
                        if(pol.cargocandidatura){
                            var partido = pol.partidocandidatura;
                            var estado = pol.estadocandidatura;
                            pol.textoCandidato = "Candidato a " + pol.cargocandidatura.toLowerCase();

                            if(pol.anocandidatura != null && anoAtual != pol.anocandidatura)
                                pol.textoCandidato += " em " + pol.anocandidatura;

                            if(pol.numero) pol.textoCandidato += ", número " + pol.numero;
                            if(partido) pol.textoCandidato += ", " + partido;
                            if(estado) pol.textoCandidato += " - " + estado;
                        }
                        if(pol.textoCandidato)
                            pol.textoCandidato += "<br />";

                        // Governador - Rio de Janeiro, RJ - PMDB
                        pol.textoCargoLocal = "";
                        if(pol.cargo && pol.cargo != "Nenhum") pol.textoCargoLocal = pol.cargo;
                        if(pol.textoCargoLocal && (pol.cidade || pol.estado))
                            pol.textoCargoLocal += " - ";

                        var callback = function(){
                            var partido = pol.partido || pol.partido || "";
                            if(pol.textoCargoLocal && partido) pol.textoCargoLocal += " - ";
                            if(partido) pol.textoCargoLocal += partido;
                            pol.textoCargoLocal = pol.textoCargoLocal.replace(" -  - ", " - ");
                            if(cbFunction != null) cbFunction();
                        }

                        if(pol.cidade && pol.cidade != -1 && pol.estado){
                            Global.getCidade(pol.estado, pol.cidade, function(c){
                                var cidade = "";
                                if(c!=null) cidade = c.nome + ", ";
                                pol.textoCargoLocal += cidade + pol.estado;
                                callback();
                            })
                        }
                        else {
                            if(pol.estado) pol.textoCargoLocal += pol.estado
                            callback();
                        }
                    }

                    $scope.ultimoscadastrados = response.politicos;
                }
            });
    }

    var init = function(){
        $scope.ultCad(function(){
            // like box do facebook
            /*
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=327721127316035";
                fjs.parentNode.insertBefore(js, fjs);
            } (document, 'script', 'facebook-jssdk'));
            */
        });
    }
    init();


    jQuery(function(){
        setAutcomplete();
    });

    var cargosFederais = [ "Presidente" ];
    var cargosEstaduais = [ "Presidente", "Governador", "Senador", "Deputado Federal", "Deputado Estadual" ];
    var cargosDF = [ "Presidente", "Governador", "Senador", "Deputado Federal", "Deputado Distrital" ];
    Server.getEstados(function (data) {
        $scope.estado = { "sigla": "", "nome": "Nenhum" };
        data.splice(0, 0, $scope.estado)
        $scope.estados = data;
        $scope.cargos = cargosFederais;
        $scope.cargo = "Presidente";

        if($scope.estadoCadastrado.sigla)
            for(var i = 0; i < data.length; i++){
                if($scope.estadoCadastrado.sigla == data[i].sigla)
                    $scope.estadoCadastrado = data[i];
            }
    });

    $scope.changeEstado = function(){
        //Global.loadCidades($scope.estado.sigla,function(res){
        //    $scope.cidades = res;
        //});
        if($scope.estado && $scope.estado.sigla){
            if($scope.estado.sigla == "DF")
                $scope.cargos = cargosDF;
            else $scope.cargos = cargosEstaduais;
        }
        else {
            $scope.cargos = cargosFederais;
            $scope.cargo = "Presidente";
        }
    }

    $scope.goToTeste = function(){
        CrossData.set({ estado: $scope.estado, cidade: $scope.cidade, cargo: $scope.cargo });
        $location.path('/teste');
    }

    $scope.selecionarQuestao = function(questao){
        CrossData.set({questao:questao});
        $location.path('/lista');
    }

    $scope.keyPress = function(e){
        if(e.which == 13){

            while($scope.busca.indexOf("?") == 0){
                $scope.busca = $scope.busca.substring(1);
            }
            if($scope.busca)
                $window.location.href ="/#!/busca/" + $scope.busca;
        }
    }

    var authenticated = function(){
        Server.authenticated(function(data){
            if(data.user == null){
                $scope.showLogin = true;
            }
            else {
                $scope.showLogin = false;
            }
        });
    }
    authenticated();

    $scope.logout = function() {
        $http.post('/auth/logout').success(function() {
            authenticated();
        });
    }

    // roleta de cocorutos
    var roleta = [
        {
            link: '/dilma-roussef',
            imagem: '/img/home/politico1.png'
        },
        {
            link: '/lula',
            imagem: '/img/home/politico2.png'
        },
        {
            link: '/jose-serra',
            imagem: '/img/home/politico3.png'
        },
        {
            link: '/marina-silva',
            imagem: '/img/home/politico4.png'
        },
        {
            link: '/eduardo-campos',
            imagem: '/img/home/politico5.png'
        },
        {
            link: '/aecio-neves',
            imagem: '/img/home/politico6.png'
        },
        {
            link: '/romario',
            imagem: '/img/home/politico7.png'
        },
        {
            link: '/geraldo-alckmin',
            imagem: '/img/home/politico8.png'
        },
        {
            link: '/chico-alencar',
            imagem: '/img/home/politico9.png'
        },
        {
            link: '/ciro-gomes',
            imagem: '/img/home/politico10.png'
        },
        {
            link: '/michel-temer',
            imagem: '/img/home/politico11.png'
        }
    ];

    var r = _.range(11);
    $scope.cocorutos = [];
    while($scope.cocorutos.length != 5){
        var f = Math.floor((Math.random() * r.length));
        if(f==r.length) f--;
        $scope.cocorutos.push(roleta[r.splice(f,1)]);
    }

    $scope.pergunte = function(url){
        window.location.href = url + "#!/pergunta";
    }

}

function inexCtrl($scope, $location, $window, Server, Global, $http) {

    jQuery(function(){
        setAutcomplete('inputBusca', false, "col-sm-3");
    });

    $scope.keyPress = function(e){
        if(e.which == 13){

            while($scope.busca.indexOf("?") == 0){
                $scope.busca = $scope.busca.substring(1);
            }
            if($scope.busca)
                $window.location.href ="/#!/busca/" + $scope.busca;
        }
    }

}