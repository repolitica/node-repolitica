'use strict';

var verificarAdmin = function(scope, server, window, cb){

    scope.mostrar = false;

    server.authenticated(function(data){
        if(data.user == null || !data.user.isAdmin)
            window.location.href = "/#!/";
        else {
            scope.mostrar = true;
            if(cb) cb();
        }
    });
}

function adminCtrl($scope, $location, $http, $window, Server){
    verificarAdmin($scope, Server, $window);
}

function aliasCtrl($scope, $location, $http, $window, Server){
    var politico;
    var init = function(){

        jQuery(function(){
            setAutcomplete('inputBusca', false, "", function(value,data){
                // value = politico
                politico = value;
            });
        });

        $scope.adicionar = function(){
            $scope.msg = "";
            if(!$scope.url || politico == null || politico.id == null){
                $scope.msg = "Faltou preencher coisas";
            }
            else{
                $http.post('/politicos/addalias', { alias:$scope.url, id:politico.id})
                    .success(function (response) {
                        if (!response.error) {
                            $scope.msg = "Salvo!";
                            $scope.nome = "";
                            $scope.url = "";
                        }
                        else {
                            alert(JSON.stringify(response.error));
                        }
                    }).error(function (response) {
                        alert(JSON.stringify(response));
                    });

            }
        }
    }
    verificarAdmin($scope, Server, $window, init);
}

function votosCtrl($scope, $location, $http, $window, Server){
    $scope.ano = 2014;
    $scope.turno = 1;
    var init = function(){
        async.auto({
                estados: function(cb){
                    Server.getEstados(function (data) {
                        cb('',data);
                    });
                },
                cargos: function(cb){
                    Server.getCargos(function (data) {
                        for(var i = data.length -1; i >=0; i--){
                            if(data[i] == "Prefeito" || data[i] == "Vice-prefeito" || data[i] == "Vereador")
                                data.splice(i,1);
                            if(data[i] == "Suplente") data[i] = "Suplente de Senador";
                        }
                        cb('',data);
                    });
                },
            },
            function (err, r) {
                $scope.$apply(function () {
                    $scope.estados = r.estados;
                    $scope.estados.splice(0,0,{ "nome": "" });
                    $scope.cargos = r.cargos;
                    $scope.cargos.splice(0, 0, "Nenhum");
                });
            });

        $http.get('/votos').success(function(resp, status, headers, config){
            resp = _.sortBy(resp, function(p){ return p.estado});
            $scope.votos = resp;
        })
    }

    $scope.changeCargo = function(){
        if($scope.cargocandidatura == "Presidente" || $scope.cargocandidatura == "Vice-presidente"){
            delete $scope.estado;
            $scope.showEstado = false;
        }
        else $scope.showEstado = true;
        $scope.buscarPolitico();
    }

    $scope.buscarPolitico = function(){
        delete $scope.politico
        $scope.msg = "";
        if($scope.partido == null || $scope.partido.length < 2) return;
        $scope.politicoSelecionado = false;

        var cargo = $scope.cargo;
        if(cargo == "Suplente de Senador") cargo = "Suplente";
        $scope.loading = true;

        $http.post('/politicos/buscaCadastro', {
            partido: $scope.partido,
            estado: $scope.estado,
            cargocandidatura: cargo
        }).success(function (response) {
                if (!response.error) {
                    $scope.msg = "";
                    delete $scope.politico;
                    if(response.length == 0) $scope.msg = "Candidato não cadastrado no Repolitica";
                    else if(response.length > 1) $scope.msg = "Foi encontrado mais de um candidato.";
                    else {
                        var pol = response[0];
                        if(!pol.id) pol.id = pol._id;

                        pol.textoCandidato = "";
                        if(pol.cargocandidatura && !pol.eleito){
                            pol.textoCandidato = "Candidato";
                            pol.textoCandidato += " a " + pol.cargocandidatura.toLowerCase();
                            if(pol.partido) pol.textoCandidato += ", " + pol.partido;
                            var anoAtual = new Date().getFullYear();
                            if(pol.anocandidatura && pol.anocandidatura != anoAtual) pol.textoCandidato += ", em " + pol.anocandidatura;
                        }
                        if(pol.textoCandidato)
                            pol.textoCandidato += "<br />";

                        pol.textoCargoLocal = "";
                        if(pol.cargo) pol.textoCargoLocal = pol.cargo;
                        if(pol.textoCargoLocal && pol.estado)
                            pol.textoCargoLocal += " - ";
                        if(pol.estado)
                            pol.textoCargoLocal += pol.estado;
                        $scope.politico = pol;
                        $scope.politicoSelecionado = true;

                        setTimeout(function(){
                            $scope.$apply(function(){
                                $scope.loading = false;
                                $scope.politicoSelecionado = true;
                            });
                        }, 1000);
                    }
                }
                else {
                }
            }).error(function (response) {
            });
    }

    $scope.adicionar = function(){

        var novovoto = {
            idpolitico: $scope.politico.id,
            ano: $scope.ano,
            turno: $scope.turno,
            votos: $scope.votosn,
            partido: $scope.partido,
            cargo: $scope.cargo,
            estado: $scope.estado
        }

        $http.post('/votos/create', novovoto)
            .success(function (response) {
                if (!response.error) {
                    $scope.votos = [];
                    init();
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }

    verificarAdmin($scope, Server, $window, init);
}

function blacklistCtrl($scope, $location, $http, $window, Server){
    var init = function(){
        $http.get('/users/blacklist').success(function(resp, status, headers, config){
            resp = _.sortBy(resp, function(p){ return p});
            $scope.emails = [];
            for(var i = 0; i < resp.length; i++){
                $scope.emails.push(resp[i].email);
            }
        })
    }

    $scope.adicionar = function(){
        $http.post('/users/addblacklist', { email:$scope.emailblacklist})
            .success(function (response) {
                if (!response.error) {
                    $scope.emails = [];
                    init();
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }

    verificarAdmin($scope, Server, $window, init);
}

function perguntasCtrl($scope, $location, $http, $window, Server){
    verificarAdmin($scope, Server, $window);

    $http.post('/Alo/getAPUrl').success(function(data){
        $http.get(data + '/repolitica/allstats').success(function(resp, status, headers, config){
            $scope.pols = _.sortBy(resp, function(p){ return -p.demandas - p.respondidos});
        })
    });

    $scope.select = function(p){
        $scope.lista = p.lista;
    }
}

function criadorPoliticoCtrl($scope, $location, $http, $routeParams, $window, Server) {
    verificarAdmin($scope, Server, $window, init);

    var inputs = [];
    var init = function(){
        inputs = [];
        // geração de inputs para attributos estáticos
        inputs.push({ texto:"Url do politico", atributo:"politicourl" });
        inputs.push({ texto:"Nome de candidatura", atributo:"nomecandidatura" });
        inputs.push({ texto:"Cargo Atual", atributo:"cargo" });
        inputs.push({ texto:"Partido atual", atributo:"partido" });
        inputs.push({ texto:"Estado", atributo:"estado" });
        inputs.push({ texto:"Número", atributo:"numero" });
        inputs.push({ texto:"Coligação", atributo:"coligacao" });
        inputs.push({ texto:"Ano da última candidatura", atributo:"anocandidatura" });
        inputs.push({ texto:"Eleito? (true/false)", atributo:"eleito" });
        inputs.push({ texto:"Cargo da candidatura", atributo:"cargocandidatura" });
        inputs.push({ texto:"Partido da candidatura", atributo:"partidocandidatura" });
        inputs.push({ texto:"Estado da candidatura", atributo:"estadocandidatura" });
        inputs.push({ texto:"Biografia", atributo:"biografia" });
        inputs.push({ texto:"Url Excelências", atributo:"urlexcelencias" });
        inputs.push({ texto:"Projetos apresentados", atributo:"projetosapresentados"});
        inputs.push({ texto:"Progetos irrelevantes", atributo:"projetosirrelevantes"});
        inputs.push({ texto:"Presenças", atributo:"presencas" });
        inputs.push({ texto:"Faltas não justificadas", atributo:"faltasnaojustificadas" });
        inputs.push({ texto:"Faltas justificadas", atributo:"faltasjustificadas" });
        inputs.push({ texto:"Nome completo", atributo:"nomecompleto" });
        inputs.push({ texto:"Dia do Nascimento", atributo:"nascimentodia" });
        inputs.push({ texto:"Mês", atributo:"nascimentomes" });
        inputs.push({ texto:"Ano", atributo:"nascimentoano" });
        inputs.push({ texto:"Estado civil", atributo:"estadocivil" });
        inputs.push({ texto:"Ocupação", atributo:"ocupacao" });
        inputs.push({ texto:"Escolaridade", atributo:"escolaridade" });

        $scope.staticInfo = inputs;
    }

    async.auto({
        estados: function(cb){
            Server.getEstados(function (data) {
                $scope.estados = data;
                cb();
            });
        },
        cargos: function(cb){
            Server.getCargos(function(cargos){
                $scope.cargos = cargos;
                cb();
            });
        },
        partidos: function(cb){
            Server.getPartidos(false,function(partidos){
                $scope.partidos = partidos;
                cb();
            });
        }
    },function(err,r){
        verificarAdmin($scope, Server, $window, init);
    });

    $scope.criar = function(){
        var politico = {};
        for(var i=0; i< inputs.length; i++){
            politico[inputs[i].atributo] = inputs[i].valor;
        }

        $http.post('/politicos/create', { politico:politico })
            .success(function (response) {
                if (!response.error) {
                    init();
                    alert(JSON.stringify(response));
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }
}

function mudancasCtrl($scope, $location, $http, $window, Server){
    $scope.loading = true;
    var ultData = null;
    $scope.request = function(){
        $scope.loading = true;
        $http.post('/politicos/mudancas', {data: ultData})
            .success(function (response) {
                if (!response.error) {
                    $scope.selPropriedade = null;
                    $scope.modificacoes = null;
                    $scope.selUsuario = null;
                    $scope.selPolitico = null;
                    $scope.propriedades = null;
                    $scope.modificacoes = null;
                    $scope.polDict = {};
                    $scope.userDict = {};
                    $scope.usuarios = [];
                    $scope.politicos = [];

                    for(var i = 0; i < response.length; i++){
                        if(!$scope.polDict[response[i].politicoid]) $scope.polDict[response[i].politicoid] = [];
                        $scope.polDict[response[i].politicoid].push(response[i]);
                        if(!$scope.userDict[response[i].usuarioid]) $scope.userDict[response[i].usuarioid] = [];
                        $scope.userDict[response[i].usuarioid].push(response[i]);
                    }

                    var usIds = [];
                    for(var id in $scope.userDict)
                        usIds.push(id);

                    var pIds = [];
                    for(var id in $scope.polDict)
                        pIds.push(id);

                    $http.post('/users/lista', {lista: usIds}).success(function(response){
                        for(var j = 0; j < response.length; j++)
                            response[j].modificacoes = $scope.userDict[response[j]._id];
                        $scope.usuarios = _.sortBy(response, function(r){ return -new Date(r.modificacoes[0].datacriacao).getTime()});
                    });

                    $http.post('/politicos/lista', {lista: pIds}).success(function(response){
                        for(var j = 0; j < response.length; j++)
                            response[j].modificacoes = $scope.polDict[response[j]._id];
                        $scope.politicos = _.sortBy(response, function(r){ return -new Date(r.modificacoes[0].datacriacao).getTime()});
                        $scope.loading = false;
                    });


                    if(response.length > 0)
                        ultData = response[response.length - 1].datacriacao;
                }
                else {
                }
            }).error(function (response) {
            });
    }
    verificarAdmin($scope, Server, $window, $scope.request);

    $scope.selMod = function(pol){
        $scope.selPropriedade = null;
        $scope.modificacoes = null;
        $scope.selPolitico = pol;
        $scope.selUsuario = null;
        var mods = pol.modificacoes;
        var props = {};
        for(var i = 0; i < mods.length; i++){
            if(props[mods[i].nomepropriedade] == null) props[mods[i].nomepropriedade] = [];
            props[mods[i].nomepropriedade].push(mods[i]);
        }
        $scope.propriedades = [];
        for(var p in props)
            $scope.propriedades.push({nome: p, modificacoes: props[p]});
        props = null;
    }
    $scope.selModUs = function(us){
        $scope.selPropriedade = null;
        $scope.modificacoes = null;
        $scope.selUsuario = us;
        $scope.selPolitico = null;
        var mods = us.modificacoes;
        var props = {};
        for(var i = 0; i < mods.length; i++){
            if(props[mods[i].nomepropriedade] == null) props[mods[i].nomepropriedade] = [];
            props[mods[i].nomepropriedade].push(mods[i]);
        }
        $scope.propriedades = [];
        for(var p in props)
            $scope.propriedades.push({nome: p, modificacoes: props[p]});
        props = null;
    }
    $scope.selProp = function(prop){
        $scope.selPropriedade = prop;

        for(var i = 0; i < prop.modificacoes.length; i++){
            if($scope.selUsuario){
                var pol = {};
                for(var j = 0; j < $scope.politicos.length; j++){
                    if($scope.politicos[j]._id == prop.modificacoes[i].politicoid)
                        pol = $scope.politicos[j];
                }
                prop.modificacoes[i].texto1 = pol.nomecandidatura;
                prop.modificacoes[i].texto2 = pol.cargocandidatura;
                prop.modificacoes[i].texto2 += " - " + pol.estadocandidatura;
                prop.modificacoes[i].texto2 += " " + pol.partidocandidatura;
            }
            else if($scope.selPolitico){
                var us = {};
                for(var j = 0; j < $scope.usuarios.length; j++){
                    if($scope.usuarios[j]._id == prop.modificacoes[i].usuarioid)
                        us = $scope.usuarios[j];
                }
                prop.modificacoes[i].texto1 = us.nome || " ";
                prop.modificacoes[i].texto1 += " - " + us.email;
            }
        }

        $scope.modificacoes = prop.modificacoes;
    }
}

function conflitoCtrl($scope, $http, Global, $window, Server){
    var request = function(){
        $http.post('/politicos/conflitosAgrupados')
            .success(function (response) {
                if (!response.error) {

                    async.eachSeries(response,function(grupo,cb){
                        if(grupo._id.estado && grupo._id.cidade!=-1)
                            Global.getCidade(grupo._id.estado, grupo._id.cidade,function(c){
                                grupo.cidadeNome = c.nome;
                                cb();
                            })
                        else cb();

                    },function(){
                        for(var i = 0; i < response.length; i++){
                            var texto = "Url do político: " + response[i]._id.politicourl;
                            var link = "/#!/admin/conflitolocal";
                            if(response[i]._id.estado) {
                                texto += " - Estado: " + response[i]._id.estado;
                                link += "/" + response[i]._id.estado;
                                if(response[i].cidadeNome){
                                    texto += " - Cidade: " + response[i].cidadeNome;
                                    link += "/" + response[i]._id.cidade;
                                }
                            }
                            response[i].texto = texto;
                            response[i].link = link + "/" + response[i]._id.politicourl;
                        }
                        $scope.grupos = response;
                    });

                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }
    verificarAdmin($scope, Server, $window, request);
}

function conflitoLocalCtrl($scope, $routeParams, $http, Global, $route, Server, $window){


    var estado = $routeParams["estado"];
    var cidade = $routeParams["cidade"];
    var urlpolitico = $routeParams["urlpolitico"];

    var request = function(){
        $http.post('/politicos/conflitos', { urlpolitico:urlpolitico, estado: estado, cidade: cidade })
            .success(function (response) {
                if (!response.error) {
                    for(var i = 0; i < response.politicos.length; i++){
                        if(!response.politicos[i].imagemurl) response.politicos[i].imagemurl = "/img/noimage.png";
                    }
                    $scope.politicos = response.politicos;
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }
    verificarAdmin($scope, Server, $window, request);

    $scope.mudarUrl = function(politico){
        Global.updatePolitico(politico, "admin", function(){
            $route.reload();
        })
    }
}

function semestadosCtrl($scope, $routeParams, $http, Global, $route, Server, $window){

    var request = function(){
        $http.post('/politicos/semestados')
            .success(function (response) {
                if (!response.error) {

                    async.eachSeries(response.politicos,function(pol,cb){
                        if(!pol.imagemurl) pol.imagemurl = "/img/noimage.png";
                        if(pol.naturalcidade && pol.naturalestado){
                            Global.getCidade(pol.naturalestado,pol.naturalcidade,
                                function(cid){
                                    if(cid)
                                        pol.naturalcidadestring = cid.nome;
                                    cb();
                                }
                            )
                        }
                        else cb();

                    },function(){
                        Server.getEstados(function (data) {
                            $scope.politicos = response.politicos;
                            $scope.estados = data;
                        });
                    })
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }
    verificarAdmin($scope, Server, $window, request);

    $scope.mudarEstado = function(politico){
        delete politico.naturalcidadestring;
        Global.updatePolitico(politico, "admin", function(){
            $route.reload();
        })
    }
}


function nomesiguaislistaCtrl($scope, $routeParams, $http, Global, $route, Server, $window){

    var request = function(){
        $http.post('/politicos/nomesiguais')
            .success(function (response) {
                if (!response.error) {
                    $scope.nomes = response.nomes;
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }
    verificarAdmin($scope, Server, $window, request);
}

function nomesiguaisCtrl($scope, $routeParams, $http, Global, $route, Server, $window){
    var pols = [];
    var nome = $routeParams["nome"];
    var estado = $routeParams["estado"];

    var loadAttrs = function(){
        var keys = {
            imagemurl:1,
            nascimentodia:1,
            nascimentomes:1,
            nascimentoano:1,
            naturalcidadestring:1,
            naturalcidade:1,
            naturalestado:1,
            tituloeleitor:1,
            politicianId:1,
            politicianId2:1
        };
        for(var key in pols[0]) if(key!="_id") keys[key] = 1;
        for(var key in pols[1]) if(key!="_id") keys[key] = 1;

        var attrs = [];
        for(var key in keys){
            var attr = {};
            attr.nome = key;
            if(key == "imagemurl"){
                attr.imagemurl0 = pols[0].imagemurl;
                attr.imagemurl1 = pols[1].imagemurl;
            }
            else{
                attr.pol0 = pols[0][key];
                attr.pol1 = pols[1][key];
            }
            attrs.push(attr);
        }

        $scope.atributos = attrs;

    }

    var request = function(){
        $http.post('/politicos/nomesiguais',{nome:nome, estado:estado})
            .success(function (response) {
                if (!response.error) {
                    if(response.politicos.length > 2)
                        alert("cuidado! Existem " + response.politicos.length +
                        " políticos com esses critérios!");
                    for(var i = 0; i < response.politicos.length; i++){
                        if(!response.politicos[i].imagemurl) response.politicos[i].imagemurl = "/img/noimage.png";
                        if(response.politicos[i].naturalcidade &&
                            response.politicos[i].naturalestado){
                            Global.getCidade(response.politicos[i].naturalestado,response.politicos[i].naturalcidade,function(cid){
                                if(cid)
                                    response.politicos[i].naturalcidadestring = cid.nome;
                            })
                        }
                    }

                    pols = response.politicos;
                    if(pols.length < 2) return alert("Sem repetições!");

                    loadAttrs();
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }
    verificarAdmin($scope, Server, $window, request);
    $scope.apagar = function(index){
        if(!confirm("Tem certeza que não quer ver mais a cara desse sujeito?")) return;

        $http.post('/politicos/remove',{id:pols[index]._id})
            .success(function (response) {
                if (!response.error) {
                    $window.location.href = "/#!/admin/merger";
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }

    $scope.copiarpara = function(index, attr){
        if(index == 0) pols[0][attr] = pols[1][attr];
        else if(index == 1) pols[1][attr] = pols[0][attr];
        loadAttrs();
    }

    $scope.copiarpoliticianid2 = function(index){
        if(index == 0) pols[0].politicianId2 = pols[1].politicianId;
        else if(index == 1) pols[1].politicianId2 = pols[0].politicianId;
        loadAttrs();
    }

    $scope.salvar = function(index){
        if(!confirm("Tem certeza que vai salvar as modificações?")) return;
        Global.updatePolitico(pols[index], "admin", function(){
            alert("politico atualizado!");
        })
    }

    $scope.naoMostrar = function(index){
        if(!confirm("Tem certeza que ele é único?")) return;
        pols[index].naomostrarnomesiguais = true;
        Global.updatePolitico(pols[index], "admin", function(){
            $window.location.href = "/#!/admin/merger";
        })
    }
}

function naturalcidadeCtrl($scope, $routeParams, $http, Global, $route, Server, $window){

    var request = function(){
        $http.post('/politicos/semcidadenatal')
            .success(function (response) {
                if (!response.error) {

                    async.eachSeries(response.politicos,function(pol,cb){
                        if(!pol.imagemurl) pol.imagemurl = "/img/noimage.png";
                        if(pol.naturalestado){
                            Global.loadCidades(pol.naturalestado,
                                function(cidades){
                                    pol.cidades = cidades;
                                    cb();
                                }
                            )
                        }
                        else cb();

                    },function(){
                        Server.getEstados(function (data) {
                            $scope.politicos = response.politicos;
                            $scope.estados = data;
                        });
                    })
                }
                else {
                    alert(JSON.stringify(response.error));
                }
            }).error(function (response) {
                alert(JSON.stringify(response));
            });
    }
    verificarAdmin($scope, Server, $window, request);

    $scope.mudarcidade = function(politico){
        if(!politico.naturalcidade) return alert("coloca alguma cidade Mané!");
        delete politico.cidades;
        delete politico.naturalcidadestring;
        Global.updatePolitico(politico, "admin", function(){
            $route.reload();
        })
    }

    $scope.mudarstring = function(politico){
        delete politico.cidades;
        politico.naturalcidade = -1;
        Global.updatePolitico(politico, "admin", function(){
            $route.reload();
        })

    }
}

function testesCtrl($scope, $location, $http, $window, Server){

    var hj = new Date();
    $scope.Dia = hj.getDate();
    $scope.Mes = hj.getMonth() + 1;
    $scope.Ano = hj.getFullYear();

    var decimal = function(n){
        var aFim = n.toString().split(".");
        var pFim = aFim[0];
        if(aFim.length > 1) pFim += "." + aFim[1].substring(0,1);
        return pFim;
    }

    $scope.reqTeste = function(){
        $scope.loading = true;
        $http.post('/Users/consultaTeste', {data:{dia: $scope.Dia, mes: $scope.Mes, ano: $scope.Ano}})
            .success(function (response) {
                if (!response.error) {

                    $scope.total = response.total;
                    $scope.data = response.data;
                    $scope.fim = response.fim + " (" + decimal(response.fim * 100 / response.total) + "%)";
                    $scope.tempo = decimal((response.tempo / response.fim) / 1000);
                    $scope.respondidas = decimal(response.respostas / response.fim);
                    $scope.fb = response.fb + " (" + decimal(response.fb * 100 / response.fim) + "%)";
                    $scope.twitter = response.twitter + " (" + decimal(response.twitter * 100 / response.fim) + "%)";

                    var ests = []
                    for(var estado in response.estados){
                        ests.push({estado:estado || "Nenhum", n: response.estados[estado]});
                    }
                    $scope.estados = _.sortBy(ests, function(est){ return -est.n; });

                    var cs = []
                    for(var c in response.cargos){
                        cs.push({c:c, n: response.cargos[c]});
                    }
                    $scope.cargos = _.sortBy(cs, function(c){ return -c.n; });

                    $scope.loading = false;

                }
                else {
                    $scope.loading = false;
                    alert(response.error);
                }
            }).error(function (response) {
                $scope.loading = false;
                alert(response);
            });
    }
    verificarAdmin($scope, Server, $window);
}
