'use strict';


function headerCtrl($scope, $location, $http, $routeParams, $window, Server) {
    $scope.busca = $routeParams["busca"];
    $scope.keyPress = function(e){
        if(e.which == 13 && $scope.busca != $routeParams["busca"]) {

            while($scope.busca.indexOf("?") == 0){
                $scope.busca = $scope.busca.substring(1);
            }

            if($scope.busca)
                $window.location.href = "/#!/busca/" + $scope.busca;
            else
                $window.location.href = "/#!/lista";
        }
    }

    var authenticated = function(){
        Server.authenticated(function(data){
            if(data == null || data.user == null){
                $scope.showLogin = true;
            }
            else {
                $scope.showLogin = false;
            }
        });
    }
    authenticated();

    $scope.logout = function() {
        $http.post('/auth/logout').success(function() {
            authenticated();
        });
    }

    jQuery(function(){
        setAutcomplete('inputBusca', false, "col-sm-3");
    });
}