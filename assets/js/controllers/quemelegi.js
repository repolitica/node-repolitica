'use strict';

var emailNovidades = function(email,http,cb){
    http.post('/contato/novidadesquemelegi', {email:email}).success(function (res) {
        cb(res);
    });
}

function quemelegihomeCtrl($scope, $location, $http, $window, Server, CrossData){

    $scope.cargos = ["Deputado Estadual", "Deputado Federal"];
    $scope.changeEstado = function(){
        if($scope.estado.sigla != "DF")
            $scope.cargos = ["Deputado Estadual", "Deputado Federal"];
        else
            $scope.cargos = ["Deputado Distrital", "Deputado Federal"];
    }
    $scope.enviando = false;
    $scope.cadastraremail = function(){
        $scope.enviando = true;
        emailNovidades($scope.userEmail,$http, function(res){
            $scope.enviando = false;
            $scope.emailError = res.error;
            $scope.emailMsg = res.msg;
        });
    }

    var candOk = false;
    var cands = [];
    $scope.selecionarEstado = function(){
        if($scope.estado.sigla && $scope.cargo){

            jQuery(function(){
                setAutcomplete(null,{cargocandidatura: $scope.cargo, estadocandidatura: $scope.estado.sigla, limit: 3},null,
                    function(value,data){
                        $scope.candidato = value.nome;
                    });
            });

            $scope.estadoselecionado = true;
            $scope.error = "";

            // pegando os candidatos assincronamente
            cands = CrossData.getCandidatos($scope.cargo, $scope.estado.sigla);
            var ultCand;
            var getCand = function(){
                var q = {
                    anocandidatura: 2014,
                    cargocandidatura: $scope.cargo
                };

                if($scope.estado) q.estadocandidatura = $scope.estado.sigla;
                //if($scope.cidade) q.cidadecandidatura = $scope.cidade;

                var ultTexto = "";
                var limit = 250;
                cands = [];
                var buscaBanco = function(){
                    var postVar = {query:q, limit: limit};
                    if(ultTexto) postVar.ultTexto = ultTexto;
                    $http.post('/politicos/buscaSimples', postVar).success(function (ps) {
                        var cP = ps.length;
                        ultTexto = ps[cP-1].textobusca;
                        cands = cands.concat(ps);
                        ps = null;

                        if(cP < limit) fimBusca();
                        else buscaBanco();
                    });
                }

                var fimBusca = function(){
                    $http.post('/politicos/coligacoes', {estado: $scope.estado.sigla, cargo: $scope.cargo}).success(function (cs) {
                        cands = cands.concat(cs);
                        CrossData.setCandidatos(cands,$scope.cargo,$scope.estado.sigla);
                        candOk = true;
                    });
                }
                buscaBanco();
            }
            if(cands.length == 0){
                getCand();
            }
            else candOk = true;
        }
        else{
            if(!$scope.estado.sigla)
                $scope.error = "Selecione um estado";
            else $scope.error = "Selecione um cargo";
        }
    }

    $scope.voto = "candidato";
    Server.getEstados(function (data) {
        $scope.estado = { "sigla": "", "nome": "Escolha" };
        data.splice(0, 0, $scope.estado)
        $scope.estados = data;
    });

    Server.getPartidos(false, function(partidos){
        $scope.partidos = ["Escolha"];
        for(var key in partidos) {
            if(key == "Sem partido") continue;
            $scope.partidos.push(key);
        }
        $scope.partido = "Escolha";
    });

    var resultadoFinal = function(){
        $scope.error = "";
        $scope.loadingFim = false;
        var candSelec = null;

        if($scope.voto == "candidato")
        {
            if(!$scope.candidato){
                $scope.error = "Campo do candidato deve estar preenchido";
            }
            else{
                var nMatch = $scope.candidato.match(/^[0-9]{4,5}$/);

                $scope.error = "Candidato não encontrado";
                for(var i = 0; i < cands.length; i++){
                    if(nMatch){
                        if((cands[i].numero || "").toString() == $scope.candidato){
                            candSelec = cands[i];
                            $scope.error = "";
                            break;
                        }
                    }
                    else{
                        if(cands[i].textobusca && cands[i].textobusca.indexOf(removeDiacritics($scope.candidato.toLowerCase()))>=0){
                            if(candSelec != null){
                                $scope.error = "Mais de um candidato encontrado para \"" + $scope.candidato + "\".";
                                break;
                            }
                            candSelec = cands[i];
                            $scope.error = "";
                        }
                    }
                }

            }
        }
        else
            if(!$scope.partido || $scope.partido == "Escolha"){
                $scope.error = "Selecione um partido";
            }
        if(!$scope.error){
            if($scope.voto == "candidato")
                CrossData.set({candidato:candSelec, cargo: $scope.cargo, estado: $scope.estado.sigla});
            else
                CrossData.set({partido:$scope.partido, cargo: $scope.cargo, estado: $scope.estado.sigla});
            $window.location.href = "#!/resultado";
        }
    }

    $scope.resultado = function(){
        if($scope.loadingFim) return;
        $scope.loadingFim = true;
        var waitToFinish = function(){
            setTimeout(function(){
                if(candOk)
                    $scope.$apply(function(){
                        resultadoFinal();
                    });
                else waitToFinish();
            }, 500);
        }

        if(candOk) resultadoFinal();
        else waitToFinish();
    }
}

function quemelegiresultadoCtrl($scope, $location, $http, $window, Server, CrossData){
    $scope.enviando = false;
    $scope.cadastraremail = function(){
        $scope.enviando = true;
        emailNovidades($scope.userEmail,$http, function(res){
            $scope.enviando = false;
            $scope.emailError = res.error;
            $scope.emailMsg = res.msg;
        });
    }

    $scope.Math = Math;
    $scope.maxPts = 0.0001;
    $scope.minPts = 0;
    $scope.teste = 0;

    var data = CrossData.get();
    CrossData.set({});
    $scope.politico = {};
    $scope.coligacao = {};
    var pols = CrossData.getCandidatos(data.cargo,data.estado);
    if(pols.length == 0)
        $window.location.href = "#!";
    else{

        if(data.candidato){
            $scope.politico = data.candidato;
            $scope.politico.cargoLocal = $scope.politico.partidocandidatura + ", " + $scope.politico.estadocandidatura;
            $scope.showCandidato = true;

            $http.post('/votos/idpolitico', {id:$scope.politico.id || $scope.politico._id}).success(function (voto) {
                if(voto == null) return;
                $scope.politico.totalVotos = voto.votos;
            });

            var colig = false;
            for(var i = pols.length - 1; i >= 0; i--){
                if(!pols[i].partidos) continue;
                for(var j = 0; j < pols[i].partidos.length; j++){
                    if(pols[i].partidos[j] != $scope.politico.partidocandidatura) continue;
                    colig = true;
                    $scope.coligacao = pols[i];
                    break;
                }
                if(colig) break;
            }

            $scope.coligacao.composicao = $scope.coligacao.partidos.join(' / ');

            $scope.polsEleitos = [];
            //var todosPols = [];
            for(var i = 0; i < pols.length; i++){
                for(var j = 0; j < $scope.coligacao.partidos.length; j++){
                    if($scope.coligacao.partidos[j] != pols[i].partidocandidatura) continue;
                    if(pols[i].eleito) $scope.polsEleitos.push(pols[i]);
                    //todosPols.push(pols[i]);
                    break;
                }
            }

            var q = {};
            q.estado = data.estado;


            Server.getPrioridades(function(enumPrioridades){
                Server.getEnum(function (enumData) {

                    $http.post('/questoes/teste', q).success(function (qs) {
                        var ecoBase = 200;
                        var radBase = 50;
                        var socBase = 100;
                        var qsBase = 100;
                        var prio1 = 150;
                        var prio2 = 100;
                        var teste = 0;

                        var eco = null;
                        var soc = null;
                        var rad = null;

                        if($scope.politico.economia != null)
                            eco = enumData.economia[$scope.politico.economia.toString()].tipo;
                        if($scope.politico.sociedade != null)
                            soc = enumData.sociedade[$scope.politico.sociedade.toString()].tipo;
                        if($scope.politico.radicalismo != null)
                            rad = enumData.radicalismo[$scope.politico.radicalismo.toString()].tipo;

                        var colEco = 0;
                        var colSoc = 0;
                        var colRad = 0;
                        var colPr = {};
                        var totEco = 0;
                        var totSoc = 0;
                        var totRad = 0;
                        var totPr = 0;
                        var nPols = $scope.polsEleitos.length;
                        for(var i = 0; i < $scope.polsEleitos.length; i++){
                            if($scope.polsEleitos[i].economia != null){
                                colEco += parseFloat($scope.polsEleitos[i].economia);
                                totEco++;
                            }
                            if($scope.polsEleitos[i].sociedade != null){
                                if(colSoc == -1) colSoc = 0;
                                colSoc += parseFloat($scope.polsEleitos[i].sociedade);
                                totSoc++;
                            }
                            if($scope.polsEleitos[i].radicalismo != null){
                                if(colRad == -1) colRad = 0;
                                colRad += parseFloat($scope.polsEleitos[i].radicalismo);
                                totRad++;
                            }
                            if($scope.polsEleitos[i].prioridades){
                                if($scope.polsEleitos[i].prioridades[0] != null){
                                    totPr += 1.5;
                                    if(colPr[$scope.polsEleitos[i].prioridades[0].toString()] == null)
                                        colPr[$scope.polsEleitos[i].prioridades[0].toString()] = 0;
                                    colPr[$scope.polsEleitos[i].prioridades[0].toString()] += 1.5;
                                }
                                if($scope.polsEleitos[i].prioridades[1] != null){
                                    totPr += 1;
                                    if(colPr[$scope.polsEleitos[i].prioridades[1].toString()] == null)
                                        colPr[$scope.polsEleitos[i].prioridades[1].toString()] = 0;
                                    colPr[$scope.polsEleitos[i].prioridades[1].toString()] += 1;
                                }
                            }
                        }
                        // normalizando as prioridades
                        if(totPr > 0) for(var p in colPr) colPr[p] /= totPr;

                        // prioridades ordenadas
                        var prs = [];
                        for(var p in colPr) if(p) prs.push({prio: p, peso: colPr[p]});
                        prs = _.sortBy(prs,function(p){ return -p.peso; });

                        // semelhança com o político
                        if($scope.politico.prioridades != null){
                            if(prs.length >= 1 && $scope.politico.prioridades.length >= 1 &&
                                prs[0].prio.toString() == $scope.politico.prioridades[0])
                                teste += prs[0].peso * prio1;
                            if(prs.length >= 2 && $scope.politico.prioridades.length >= 2 &&
                                prs[1].prio == $scope.politico.prioridades[1])
                                teste += prs[1].peso * prio2;
                            if(prs.length >= 1 && $scope.politico.prioridades.length >= 2 &&
                                prs[0].prio == $scope.politico.prioridades[1])
                                teste += prs[0].peso * prio2;
                            if(prs.length >= 2 && $scope.politico.prioridades.length >= 1 &&
                                prs[1].prio == $scope.politico.prioridades[0])
                                teste += prs[1].peso * prio2;

                            // pts max e min
                            if(prs.length >= 1 && $scope.politico.prioridades.length >= 1) {
                                $scope.maxPts += prio1;
                                $scope.minPts -= prio1;
                            }
                            if(prs.length >= 2 || $scope.politico.prioridades.length >= 2) {
                                $scope.maxPts += prio2;
                                $scope.minPts -= prio2;
                            }
                        }

                        var colEcoStr = "";
                        var colSocStr = "";
                        var colRadStr = "";
                        if(totEco > 0){
                            if($scope.politico.economia != null){
                                var polResp = parseInt($scope.politico.economia);
                                var dif = Math.abs(polResp - colEco/totEco);
                                teste += ecoBase * (1 - (dif / 2) );
                                $scope.maxPts += ecoBase;
                                $scope.minPts -= ecoBase;
                            }
                            colEco = parseInt(Math.round(colEco/totEco));
                            colEcoStr = enumData.economia[colEco.toString()].tipo;
                        }
                        else colEco = null;
                        if(totSoc > 0){
                            if($scope.politico.sociedade != null){
                                var polResp = parseInt($scope.politico.sociedade);
                                var dif = Math.abs(polResp - colSoc/totSoc);
                                teste += socBase * (1 - dif);
                                $scope.maxPts += socBase;
                                $scope.minPts -= socBase;
                            }
                            colSoc = parseInt(Math.round(colSoc/totSoc));
                            colSocStr = enumData.sociedade[colSoc.toString()].tipo;
                        }
                        else colSoc = null;
                        if(totRad > 0){
                            if($scope.politico.radicalismo != null){
                                var polResp = parseInt($scope.politico.radicalismo);
                                var dif = Math.abs(polResp - colRad/totRad);
                                teste += radBase * (1 - dif);
                                $scope.maxPts += radBase;
                                $scope.minPts -= radBase;
                            }
                            colRad = parseInt(Math.round(colRad/totRad));
                            colRadStr = enumData.radicalismo[colRad.toString()].tipo;
                        }
                        else colRad = null;

                        $scope.questoes = [
                            {
                                titulo: "Posição sobre a economia",
                                _id: "economia",
                                politico: eco,
                                pol: $scope.politico.economia,
                                coligacao: colEcoStr,
                                col: colEco
                            },
                            {
                                titulo: "Posição sobre a sociedade",
                                _id: "sociedade",
                                politico: soc,
                                pol: $scope.politico.sociedade,
                                coligacao: colSocStr,
                                col: colSoc
                            },
                            {
                                titulo: "Radicalismo",
                                _id: "radicalismo",
                                politico: rad,
                                pol: $scope.politico.radicalismo,
                                coligacao: colRadStr,
                                col: colRad
                            }
                        ];

                        // questoes
                        if(prs.length >= 1 || ($scope.politico.prioridades && $scope.politico.prioridades.length >= 1)) {
                            var q = { titulo: "Prioridade 1", _id: "prioridade" };
                            if($scope.politico.prioridades != null && $scope.politico.prioridades.length >= 1
                                && $scope.politico.prioridades[0]){
                                q.politico = enumPrioridades[$scope.politico.prioridades[0]].prioridade;
                                q.pol = 1;
                            }
                            if(prs.length >= 1){
                                q.coligacao = enumPrioridades[prs[0].prio.toString()].prioridade;
                                q.col = 1;
                            }
                            $scope.questoes.push(q);
                        }
                        if(prs.length >= 2 || ($scope.politico.prioridades && $scope.politico.prioridades.length >= 2)){
                            var q = { titulo: "Prioridade 2", _id: "prioridade" };
                            if($scope.politico.prioridades != null && $scope.politico.prioridades.length >= 2
                                && $scope.politico.prioridades[0]){
                                q.politico = enumPrioridades[$scope.politico.prioridades[1]].prioridade;
                                q.pol = 1;
                            }
                            if(prs.length >= 2){
                                q.coligacao = enumPrioridades[prs[1].prio.toString()].prioridade;
                                q.col = 1;
                            }
                            $scope.questoes.push(q);
                        }

                        for(var i = 0; i < qs.length; i++){
                            if(qs[i].caducou) continue;
                            var id = qs[i]._id || qs[i].id;
                            if($scope.politico.questoes != null && $scope.politico.questoes[id] != null){
                                qs[i].pol = parseInt($scope.politico.questoes[id].resposta);
                                qs[i].politico = enumData.questoes[qs[i].pol.toString()];
                            }

                            var col = 0;
                            var totCol = 0;
                            for(var j = 0; j < nPols; j++){
                                if($scope.polsEleitos[j].questoes == null) continue;
                                if($scope.polsEleitos[j].questoes[id] != null &&
                                    $scope.polsEleitos[j].questoes[id].resposta){
                                    col += parseFloat($scope.polsEleitos[j].questoes[id].resposta);
                                    totCol++;
                                }
                            }

                            var colStr = "";
                            if(totCol > 0){
                                if($scope.politico.questoes[id] != null){
                                    $scope.maxPts += qsBase;
                                    $scope.minPts -= qsBase;
                                    var polResp = parseInt($scope.politico.questoes[id].resposta);
                                    var colResp = col/totCol;
                                    dif = Math.abs(polResp - colResp);
                                    if(polResp == 1 || polResp == 4){
                                        if(dif < 1)
                                            teste += qsBase * (1 - dif / 2);
                                        else if(dif > 2)
                                            teste += qsBase * (1 / 2 - dif / 2);
                                        else teste += qsBase * (3 / 2 - dif);
                                    }
                                    // resposta moderada
                                    else{
                                        // resposta contrária
                                        if(dif > 1) teste += qsBase * (1 / 2 - dif / 2);
                                        else if (dif == 0) teste += qsBase;
                                        else if (colResp == 2 && polResp < 2 ||
                                            colResp == 3 && polResp > 3)
                                            teste += qsBase * (1 - dif / 2);
                                        else teste += qsBase * (1 - dif);
                                    }
                                }

                                col = parseInt(Math.round(col/totCol));
                                colStr = enumData.questoes[col.toString()];
                            }
                            else col = null;

                            qs[i].coligacao = colStr;
                            qs[i].col = col;

                            $scope.questoes.push(qs[i]);
                        }

                        for(var i = 0; i < $scope.questoes.length; i++){
                            var c = $scope.questoes[i].col;
                            var p = $scope.questoes[i].pol
                            if(c == null || p == null)
                                $scope.questoes[i].res = "duvida";
                            else if($scope.questoes[i].coligacao == $scope.questoes[i].politico)
                                $scope.questoes[i].res = "igual";
                            else{
                                if($scope.questoes[i]._id == "prioridade"){
                                    if($scope.questoes[i].politico == $scope.questoes[i].coligacao)
                                        $scope.questoes[i].res = "igual";
                                    else
                                        $scope.questoes[i].res = "diferente";

                                    continue;
                                }
                                var dif = Math.abs(c-p);
                                if(dif == 1)
                                    $scope.questoes[i].res = "parcialmente";
                                else
                                    $scope.questoes[i].res = "diferente";
                            }
                        }
                        $scope.teste = teste;
                    });
                });
            });
        }
        else{
            $scope.politico.imagemurl = "https://s3.amazonaws.com/prd-repolitica/0static/img/partidos/" +
                data.partido.toLowerCase() + ".png";
            $scope.politico.cargocandidatura = data.cargo;
            $scope.politico.nomecandidatura = data.partido;
            $scope.politico.partidocandidatura = data.partido;
            $scope.politico.cargoLocal = data.estado;
            $scope.politico.questoes = {};
            $scope.showCandidato = false;
            $scope.politico.prioridade = {};

            var tEco = 0;
            var tSoc = 0;
            var tRad = 0;
            var tPr = 0;
            var tQs = {};
            for(var i = pols.length - 1; i >= 0; i--){
                if(pols[i].partidocandidatura != data.partido) continue;
                if(pols[i].prioridades){
                    if(pols[i].prioridades[0] != null && pols[i].prioridades[0] != ""){
                        tPr += 1.5;
                        if($scope.politico.prioridade[pols[i].prioridades[0].toString()] == null)
                            $scope.politico.prioridade[pols[i].prioridades[0].toString()] = 0;
                        $scope.politico.prioridade[pols[i].prioridades[0].toString()] += 1.5;
                    }
                    if(pols[i].prioridades[1] != null && pols[i].prioridades[1] != ""){
                        tPr += 1;
                        if($scope.politico.prioridade[pols[i].prioridades[1].toString()] == null)
                            $scope.politico.prioridade[pols[i].prioridades[1].toString()] = 0;
                        $scope.politico.prioridade[pols[i].prioridades[1].toString()] += 1;
                    }
                }
                if(pols[i].economia != null){
                    tEco++;
                    if($scope.politico.economia == null) $scope.politico.economia = 0;
                    $scope.politico.economia += parseFloat(pols[i].economia);
                }
                if(pols[i].sociedade != null){
                    tSoc++;
                    if($scope.politico.sociedade == null) $scope.politico.sociedade = 0;
                    $scope.politico.sociedade += parseFloat(pols[i].sociedade);
                }
                if(pols[i].radicalismo != null){
                    tRad++;
                    if($scope.politico.radicalismo == null) $scope.politico.radicalismo = 0;
                    $scope.politico.radicalismo += parseFloat(pols[i].radicalismo);
                }
                if(pols[i].questoes != null){
                    for(var r in pols[i].questoes){
                        if($scope.politico.questoes[r] == null) $scope.politico.questoes[r] = 0;
                        if(tQs[r] == null) tQs[r] = 0;
                        tQs[r]++;
                        $scope.politico.questoes[r] += parseFloat(pols[i].questoes[r].resposta);
                    }
                }
            }
            if($scope.politico.economia != null){ $scope.politico.economia /= tEco; }
            if($scope.politico.sociedade != null){ $scope.politico.sociedade /= tSoc; }
            if($scope.politico.radicalismo != null){ $scope.politico.radicalismo /= tRad; }
            for(var r in tQs) $scope.politico.questoes[r] /= tQs[r];
            if(tPr > 0)
                for(var p in $scope.politico.prioridade) $scope.politico.prioridade[p] /= tPr;

            var colig = false;
            for(var i = pols.length - 1; i >= 0; i--){
                if(!pols[i].partidos) continue;
                for(var j = 0; j < pols[i].partidos.length; j++){
                    if(pols[i].partidos[j] != $scope.politico.partidocandidatura) continue;
                    colig = true;
                    $scope.coligacao = pols[i];
                    break;
                }
                if(colig) break;
            }

            if($scope.coligacao.partidos)
                $scope.coligacao.composicao = $scope.coligacao.partidos.join(' / ');
            else $scope.coligacao.composicao = "Nenhuma";

            $scope.polsEleitos = [];
            //var todosPols = [];
            for(var i = 0; i < pols.length; i++){
                for(var j = 0; j < $scope.coligacao.partidos.length; j++){
                    if($scope.coligacao.partidos[j] != pols[i].partidocandidatura) continue;
                    if(pols[i].eleito) $scope.polsEleitos.push(pols[i]);
                    //todosPols.push(pols[i]);
                    break;
                }
            }

            var q = {};
            q.estado = data.estado;


            Server.getPrioridades(function(enumPrioridades){
                Server.getEnum(function (enumData) {

                    $http.post('/questoes/teste', q).success(function (qs) {
                        var ecoBase = 200;
                        var radBase = 50;
                        var socBase = 100;
                        var qsBase = 100;
                        var prio1 = 150;
                        var prio2 = 100;
                        var teste = 0;

                        var eco = null;
                        var soc = null;
                        var rad = null;

                        if($scope.politico.economia != null)
                            eco = enumData.economia[parseInt(Math.round($scope.politico.economia)).toString()].tipo;
                        if($scope.politico.sociedade != null)
                            soc = enumData.sociedade[parseInt(Math.round($scope.politico.sociedade)).toString()].tipo;
                        if($scope.politico.radicalismo != null)
                            rad = enumData.radicalismo[parseInt(Math.round($scope.politico.radicalismo)).toString()].tipo;

                        var colEco = 0;
                        var colSoc = 0;
                        var colRad = 0;
                        var colPr = {};
                        var totEco = 0;
                        var totSoc = 0;
                        var totRad = 0;
                        var totPr = 0;
                        var nPols = $scope.polsEleitos.length;

                        // somando a quantidade em cada ideologia
                        for(var i = 0; i < $scope.polsEleitos.length; i++){
                            if($scope.polsEleitos[i].economia != null){
                                colEco += parseFloat($scope.polsEleitos[i].economia);
                                totEco++;
                            }
                            if($scope.polsEleitos[i].sociedade != null){
                                if(colSoc == -1) colSoc = 0;
                                colSoc += parseFloat($scope.polsEleitos[i].sociedade);
                                totSoc++;
                            }
                            if($scope.polsEleitos[i].radicalismo != null){
                                if(colRad == -1) colRad = 0;
                                colRad += parseFloat($scope.polsEleitos[i].radicalismo);
                                totRad++;
                            }
                            if($scope.polsEleitos[i].prioridades){
                                if($scope.polsEleitos[i].prioridades[0] != null){
                                    totPr += 1.5;
                                    if(colPr[$scope.polsEleitos[i].prioridades[0].toString()] == null)
                                        colPr[$scope.polsEleitos[i].prioridades[0].toString()] = 0;
                                    colPr[$scope.polsEleitos[i].prioridades[0].toString()] += 1.5;
                                }
                                if($scope.polsEleitos[i].prioridades[1] != null){
                                    totPr += 1;
                                    if(colPr[$scope.polsEleitos[i].prioridades[1].toString()] == null)
                                        colPr[$scope.polsEleitos[i].prioridades[1].toString()] = 0;
                                    colPr[$scope.polsEleitos[i].prioridades[1].toString()] += 1;
                                }
                            }
                        }
                        // normalizando as prioridades
                        if(totPr > 0) for(var p in colPr) colPr[p] /= totPr;

                        // prioridades ordenadas
                        var prs = [];
                        for(var p in colPr) prs.push({prio: p, peso: colPr[p]});
                        prs = _.sortBy(prs,function(p){ return -p.peso; });

                        // semelhança com o partido
                        $scope.politico.prs = [];
                        for(var p in $scope.politico.prioridade)
                            $scope.politico.prs.push({prio: p, peso: $scope.politico.prioridade[p]});
                        $scope.politico.prs = _.sortBy($scope.politico.prs,function(p){ return -p.peso; });
                        if(prs.length >= 1 && $scope.politico.prs.length >= 1 && prs[0].prio == $scope.politico.prs[0].prio)
                            teste += prs[0].peso * $scope.politico.prs[0].peso * prio1;
                        if(prs.length >= 2 && $scope.politico.prs.length >= 2 && prs[1].prio == $scope.politico.prs[1].prio)
                            teste += prs[1].peso * $scope.politico.prs[1].peso * prio1;
                        if(prs.length >= 1 && $scope.politico.prs.length >= 2 && prs[0].prio == $scope.politico.prs[1].prio)
                            teste += prs[0].peso * $scope.politico.prs[1].peso * prio1;
                        if(prs.length >= 2 && $scope.politico.prs.length >= 1 && prs[1].prio == $scope.politico.prs[0].prio)
                            teste += prs[1].peso * $scope.politico.prs[0].peso * prio1;

                        // pts max e min
                        if(prs.length >= 1 && $scope.politico.prs.length >= 1) {
                            $scope.maxPts += prio1;
                            $scope.minPts -= prio1;
                        }

                        var colEcoStr = "";
                        var colSocStr = "";
                        var colRadStr = "";
                        if(totEco > 0){
                            if($scope.politico.economia != null){
                                var polResp = parseFloat($scope.politico.economia);
                                var dif = Math.abs(polResp - colEco/totEco);
                                teste += ecoBase * (1 - (dif / 2) );
                                $scope.maxPts += ecoBase;
                                $scope.minPts -= ecoBase;
                            }
                            colEco = parseInt(Math.round(colEco/totEco));
                            colEcoStr = enumData.economia[colEco.toString()].tipo;
                        }
                        else colEco = null;
                        if(totSoc > 0){
                            if($scope.politico.sociedade != null){
                                var polResp = parseFloat($scope.politico.sociedade);
                                var dif = Math.abs(polResp - colSoc/totSoc);
                                teste += socBase * (1 - dif);
                                $scope.maxPts += socBase;
                                $scope.minPts -= socBase;
                            }
                            colSoc = parseInt(Math.round(colSoc/totSoc));
                            colSocStr = enumData.sociedade[colSoc.toString()].tipo;
                        }
                        else colSoc = null;
                        if(totRad > 0){
                            if($scope.politico.radicalismo != null){
                                var polResp = parseFloat($scope.politico.radicalismo);
                                var dif = Math.abs(polResp - colRad/totRad);
                                teste += radBase * (1 - dif);
                                $scope.maxPts += radBase;
                                $scope.minPts -= radBase;
                            }
                            colRad = parseInt(Math.round(colRad/totRad));
                            colRadStr = enumData.radicalismo[colRad.toString()].tipo;
                        }
                        else colRad = null;

                        $scope.questoes = [
                            {
                                titulo: "Posição sobre a economia",
                                _id: "economia",
                                politico: eco,
                                pol: $scope.politico.economia,
                                coligacao: colEcoStr,
                                col: colEco
                            },
                            {
                                titulo: "Posição sobre a sociedade",
                                _id: "sociedade",
                                politico: soc,
                                pol: $scope.politico.sociedade,
                                coligacao: colSocStr,
                                col: colSoc
                            },
                            {
                                titulo: "Radicalismo",
                                _id: "radicalismo",
                                politico: rad,
                                pol: $scope.politico.radicalismo,
                                coligacao: colRadStr,
                                col: colRad
                            }
                        ];

                        // questoes
                        if(prs.length >= 1 || $scope.politico.prs.length >= 1) {
                            var q = { titulo: "Prioridade 1", _id: "prioridade" };
                            if($scope.politico.prs.length >= 1
                                && $scope.politico.prs[0] != null && $scope.politico.prs[0].prio != ""){
                                q.politico = enumPrioridades[$scope.politico.prs[0].prio.toString()].prioridade;
                                q.pol = $scope.politico.prs[0].peso;
                            }
                            if(prs.length >= 1 && prs[0] != null && prs[0].prio != ""){
                                q.coligacao = enumPrioridades[prs[0].prio.toString()].prioridade;
                                q.col = prs[0].peso;
                            }
                            $scope.questoes.push(q);
                        }
                        if(prs.length >= 2 || $scope.politico.prs.length >= 2){
                            var q = { titulo: "Prioridade 2", _id: "prioridade" };
                            if($scope.politico.prs.length >= 2
                                && $scope.politico.prs[1] != null && $scope.politico.prs[1].prio != ""){
                                q.politico = enumPrioridades[$scope.politico.prs[1].prio.toString()].prioridade;
                                q.pol = $scope.politico.prs[1].peso;
                            }
                            if(prs.length >= 2 && prs[1] != null && prs[1].prio != ""){
                                q.coligacao = enumPrioridades[prs[1].prio.toString()].prioridade;
                                q.col = prs[1].peso;
                            }
                            $scope.questoes.push(q);
                        }


                        for(var i = 0; i < qs.length; i++){
                            if(qs[i].caducou) continue;
                            var id = qs[i]._id || qs[i].id;
                            if($scope.politico.questoes[id] != null){
                                qs[i].pol = parseFloat($scope.politico.questoes[id]);
                                var rInt = parseInt(Math.round(qs[i].pol + 1E-5));
                                qs[i].politico = enumData.questoes[rInt.toString()];
                            }

                            var col = 0;
                            var totCol = 0;
                            for(var j = 0; j < nPols; j++){
                                if($scope.polsEleitos[j].questoes == null) continue;
                                if($scope.polsEleitos[j].questoes[id] != null &&
                                    $scope.polsEleitos[j].questoes[id].resposta){
                                    col += parseFloat($scope.polsEleitos[j].questoes[id].resposta);
                                    totCol++;
                                }
                            }

                            var colStr = "";
                            if(totCol > 0){
                                if($scope.politico.questoes[id] != null){
                                    $scope.maxPts += qsBase;
                                    $scope.minPts -= qsBase;
                                    var polResp = parseFloat($scope.politico.questoes[id]);
                                    var colResp = col/totCol;
                                    dif = Math.abs(polResp - colResp);
                                    if(polResp == 1 || polResp == 4){
                                        if(dif < 1)
                                            teste += qsBase * (1 - dif / 2);
                                        else if(dif > 2)
                                            teste += qsBase * (1 / 2 - dif / 2);
                                        else teste += qsBase * (3 / 2 - dif);
                                    }
                                    // resposta moderada
                                    else{
                                        // resposta contrária
                                        if(dif > 1) teste += qsBase * (1 / 2 - dif / 2);
                                        else if (dif == 0) teste += qsBase;
                                        else if (colResp == 2 && polResp < 2 ||
                                            colResp == 3 && polResp > 3)
                                            teste += qsBase * (1 - dif / 2);
                                        else teste += qsBase * (1 - dif);
                                    }
                                }

                                col = parseInt(Math.round(col/totCol));
                                colStr = enumData.questoes[col.toString()];
                            }
                            else col = null;

                            qs[i].coligacao = colStr;
                            qs[i].col = col;

                            $scope.questoes.push(qs[i]);
                        }

                        for(var i = 0; i < $scope.questoes.length; i++){
                            var c = $scope.questoes[i].col;
                            var p = $scope.questoes[i].pol
                            var dif = Math.abs(c-p);
                            if(c == null || p == null)
                                $scope.questoes[i].res = "duvida";
                            else if($scope.questoes[i].coligacao == $scope.questoes[i].politico)
                                $scope.questoes[i].res = "igual";
                            else{
                                if($scope.questoes[i]._id == "prioridade"){
                                    if($scope.questoes[i].politico == $scope.questoes[i].coligacao)
                                        $scope.questoes[i].res = "igual";
                                    else
                                        $scope.questoes[i].res = "diferente";

                                    continue;
                                }
                                if(dif < 1.5)
                                    $scope.questoes[i].res = "parcialmente";
                                else
                                    $scope.questoes[i].res = "diferente";
                            }
                        }
                        $scope.teste = teste;
                    });
                });
            });
        }

        if($scope.polsEleitos.length == 0)
            $scope.ajudoueleger = "Ninguém";
        if($scope.polsEleitos.length >= 1)
            $scope.ajudoueleger = $scope.polsEleitos[0].nomecandidatura;
        if($scope.polsEleitos.length >= 2)
            $scope.ajudoueleger += ", " + $scope.polsEleitos[1].nomecandidatura;
        if($scope.polsEleitos.length == 3)
            $scope.ajudoueleger += " e " + $scope.polsEleitos[2].nomecandidatura;
        if($scope.polsEleitos.length > 3)
            $scope.ajudoueleger += " e outros";

        $scope.showCompartilhar = true;

        var facebook = function(img,name,description){

            var obj = {
                method: 'feed',
                link: 'https://quemelegi.repolitica.com.br',
                display: 'popup',
                picture: img,
                // primeira linha
                name: name,
                // segunda linha
                caption: ' ',
                // terceira linha
                description: description
            };

            function callback(response) {
            }

            FB.ui(obj, callback);
        }

        $scope.shareFB = function(){
            var cargo = $scope.politico.cargocandidatura;
            var nome = $scope.politico.nomecandidatura;
            var img = $scope.politico.imagemurl;

            facebook(img,
                'O meu voto no(a) ' + nome + ' ajudou a eleger ' + $scope.ajudoueleger + '!',
                'Quais candidatos a ' + cargo + ' você também ajudou a eleger?');
        }

        $scope.shareTwitter = function(){
            var cargo = $scope.politico.cargocandidatura;
            cargo = cargo.replace("Deputado", "Dep.");
            var nome = $scope.politico.nomecandidatura;
            window.open("https://twitter.com/home?status=" + encodeURI(
                'O meu voto em ' + nome + ' ajudou a eleger ' + $scope.ajudoueleger + '! quemelegi.repolitica.com.br'));
        }
    }
}