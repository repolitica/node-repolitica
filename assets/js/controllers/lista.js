'use strict';


function listaCtrl($scope, $location, $http, $routeParams, Server, Global, CrossData) {

    var param = {
        cidade:$routeParams["cidade"],
        estado:$routeParams["estado"],
        partido:$routeParams["partido"],
    };

    $scope.filtro = { eleitos:false, candidatosatuais:false };
    $scope.eleitoCand = "eleitoscandidatos"

    if($routeParams["filtro"]) $scope.eleitoCand = $routeParams["filtro"];
    if($routeParams["filtro"] && $routeParams["filtro"] == "cadastrados")
        $scope.filtro.cadastrados = true;

    var busca = $routeParams["busca"];
    if(busca) $scope.filtro.textobusca = busca;
    var estadoDefault = { nome: "Escolha o seu estado" };
    var cidadeDefault = { nome: "Escolha a sua cidade"};
    $scope.cidades = [ cidadeDefault ];
    if(!param.partido)
        $scope.partido = "Escolha um partido";
    else $scope.partido = param.partido;

    $scope.questoes = { gerais: [], federais: [], estaduais: [], municipais: []};
    $scope.questSel = "";

    // filtro de opiniao vindo da home
    var crossData = CrossData.get();
    CrossData.set({});

    if(crossData && crossData.questao){
        $scope.questao = crossData.questao;
        if($scope.questao.geral) $scope.questSel = "gerais";
        else $scope.questSel = "federais";
    }

    Server.getQuestoes({geralfederal:true}, function(qs){
        for(var i = 0; i < qs.length; i++){
            var q = qs[i];

            if($scope.questao && q._id == $scope.questao.id)
                q.in = true;

            if(q.geral) $scope.questoes.gerais.push(q);
            else $scope.questoes.federais.push(q);
        }
    });

    var buscaMais = { limit:20 };
    $scope.showMais = true;

    $scope.mostrarTudo = function(){
        if(!$scope.questao) return;
        delete $scope.questao;
        delete $scope.filtro.questaoid;
        delete $scope.filtro.resposta;
        $scope.efetuarBusca();
    }

    var busca = function(){
        // zerar parametros de paginação
        $scope.showMais = true;
        buscaMais = { limit:20 }
        $scope.politicos = [];
        $scope.buscaMais();
    }

    var buscaCount = 0;
    $scope.efetuarBusca = function(){
        // "produtor"
        buscaCount ++;
        setTimeout(function(){
            if(!$scope.buscando){
                // "consumidor"
                buscaCount --;
                if(buscaCount == 0) busca();
            }
        }, 1000);
    }

    $scope.changeEleitoCand = function(){
        $scope.filtro.recemeleitos = false;
        $scope.filtro.eleitos = false;
        $scope.filtro.candidatosatuais = false;
        $scope.filtro.cadastrados = false;

        if($scope.eleitoCand == "eleitos")
            $scope.filtro.eleitos = true;
        else if($scope.eleitoCand == "candidatos")
            $scope.filtro.candidatosatuais = true;
        else if($scope.eleitoCand == "cadastrados")
            $scope.filtro.cadastrados = true;
        else if($scope.eleitoCand == "recemeleitos")
            $scope.filtro.recemeleitos = true;

        $scope.efetuarBusca();
    }

    $scope.buscaMais = function(){
        $scope.buscando = true;

        // filtro de cargos
        $scope.filtro.cargos = [];
        if($scope.vereador) $scope.filtro.cargos.push("Vereador");
        if($scope.deputadoestadual) $scope.filtro.cargos.push("Deputado Estadual");
        if($scope.deputadodistrital) $scope.filtro.cargos.push("Deputado Distrital");
        if($scope.deputadofederal) $scope.filtro.cargos.push("Deputado Federal");
        if($scope.suplente) $scope.filtro.cargos.push("Suplente");
        if($scope.senador) $scope.filtro.cargos.push("Senador");
        if($scope.prefeito) $scope.filtro.cargos.push("Prefeito");
        if($scope.vicegovernador) $scope.filtro.cargos.push("Vice-governador");
        if($scope.governador) $scope.filtro.cargos.push("Governador");
        if($scope.vicepresidente) $scope.filtro.cargos.push("Vice-presidente");
        if($scope.presidente) $scope.filtro.cargos.push("Presidente");

        if($scope.estado.sigla) $scope.filtro.estado = $scope.estado.sigla;
        if($scope.cidade.id) $scope.filtro.cidade = $scope.cidade.id;
        if($scope.partido != "Escolha um partido") $scope.filtro.partido = $scope.partido;
        if($scope.questao) {
            $scope.filtro.questaoid = $scope.questao.id;
            $scope.filtro.resposta = $scope.questao.resposta;
        }

        $http.post('/politicos/find', { filtro: $scope.filtro, buscaMais:buscaMais })
            .success(function (response) {
                if (!response.error) {
                    delete $scope.filtro.estado;
                    delete $scope.filtro.cidade;
                    delete $scope.filtro.partido;

                    buscaMais = response.buscaMais;
                    $scope.showMais = true;
                    if(response.docs.length < 20) {
                        if(!$scope.filtro.questaoid)
                            $scope.showMais = false;
                        else{
                            if(buscaMais.semjustificativa)
                                $scope.showMais = false;
                            else{
                                buscaMais.semjustificativa = true;
                                delete buscaMais.pontos;
                            }
                        }
                    }

                    async.eachSeries(response.docs,function(pol, cb){
                        if($scope.filtro.questaoid && $scope.filtro.resposta == null &&
                            pol.questoes[$scope.questao.id].resposta == 0){
                            pol.hide = true;
                            cb();
                        }
                        else{
                            if(!pol.imagemurl) pol.imagemurl = "img/noimage.png";

                            if($scope.questao && $scope.questao.id && pol.questoes!= null && pol.questoes[$scope.questao.id] != null){
                                pol.justificativa = pol.questoes[$scope.questao.id].justificativa;
                                pol.resposta = pol.questoes[$scope.questao.id].resposta;
                            }

                            // *********** candidatura ************
                            var anoAtual = new Date().getFullYear();
                            if(pol.cargocandidatura){
                                var partido = pol.partidocandidatura;
                                var estado = pol.estadocandidatura;
                                if(pol.eleito && pol.anocandidatura == 2014)
                                    pol.textoCandidato = "Eleito ";
                                else pol.textoCandidato = "Candidato a ";
                                pol.textoCandidato += pol.cargocandidatura.toLowerCase();

                                if(pol.anocandidatura != null && anoAtual != pol.anocandidatura)
                                    pol.textoCandidato += " em " + pol.anocandidatura;

                                if(pol.numero) pol.textoCandidato += ", número " + pol.numero;
                                if(partido) pol.textoCandidato += ", " + partido;
                                if(estado) pol.textoCandidato += " - " + estado;
                            }
                            if(pol.textoCandidato)
                                pol.textoCandidato += "<br />";

                            // Governador - Rio de Janeiro, RJ - PMDB
                            pol.textoCargoLocal = "";
                            if(pol.cargo && pol.cargo != "Nenhum") pol.textoCargoLocal = pol.cargo;
                            if(pol.textoCargoLocal && (pol.cidade || pol.estado))
                                pol.textoCargoLocal += " - ";

                            var callback = function(){
                                var partido = pol.partido || pol.partido || "";
                                if(pol.textoCargoLocal && partido) pol.textoCargoLocal += " - ";
                                if(partido) pol.textoCargoLocal += partido;
                                pol.textoCargoLocal = pol.textoCargoLocal.replace(" -  - ", " - ");
                                cb();
                            }

                            Global.linkPolitico(pol, function(url){
                                pol.url = url;
                                if(pol.cidade && pol.cidade != -1 && pol.estado){
                                    Global.getCidade(pol.estado, pol.cidade, function(c){
                                        var cidade = "";
                                        if(c!=null) cidade = c.nome + ", ";
                                        pol.textoCargoLocal += cidade + pol.estado;
                                        callback();
                                    })
                                }
                                else {
                                    if(pol.estado) pol.textoCargoLocal += pol.estado
                                    callback();
                                }
                            });
                        }

                    }, function(err, results) {
                        if(!$scope.politicos)
                            $scope.politicos = response.docs;
                        else {
                            if(response.docs.length > 0){
                                // evitar politico repetido
                                for(var i = $scope.politicos.length -1; i >= 0; i--){
                                    var id = $scope.politicos[i]._id;
                                    if(response.docs.length == 0) {
                                        $scope.showMais = false;
                                        break;
                                    }

                                    for(var j = response.docs.length - 1; j >= 0; j--){
                                        if(response.docs[j]._id == id){
                                            response.docs.splice(j,1);
                                            break;
                                        }
                                    }
                                }
                                $scope.politicos = $scope.politicos.concat(response.docs);
                            }
                        }

                        fimBusca();
                    })
                }
                else {
                    fimBusca();
                    $scope.showMais = false;
                    //alert(response.error);
                }
            }).error(function (response) {
                fimBusca();
                $scope.showMais = false;
                //alert(response);
            });

    }

    var fimBusca = function(){
        $scope.buscando = false;
        if(buscaCount>0){
            buscaCount = 0;
            $scope.efetuarBusca();
        }
    }

    async.auto({
        getEstados: function(cb){
            Server.getEstados(function(estados){
                $scope.estados = estados;
                $scope.estados.splice(0, 0, estadoDefault);
                if(param.estado){
                    for(var i = 0; i < estados.length; i++){
                        if(estados[i].sigla == param.estado){
                            $scope.estado = estados[i];
                            break;
                        }
                    }

                    Global.loadCidades($scope.estado.sigla,function(res){
                        $scope.cidades = res;
                        $scope.cidades.splice(0, 0, cidadeDefault);

                        if(param.cidade){
                            var cid = param.cidade.replace(new RegExp("-", 'g'), ' ').toLowerCase();
                            cid = removeDiacritics(cid);
                            cid = cid.replace("'", '');
                            cid = cid.replace("´", '');
                            for(var i = 0; i < res.length; i++){
                                var c = removeDiacritics(res[i].nome.toLowerCase());
                                c = c.replace(new RegExp("-", 'g'), ' ');
                                c = c.replace("'", '');
                                c = c.replace("´", '');
                                if(c == cid){
                                    $scope.cidade = res[i];
                                    break;
                                }
                            }
                        }
                        else $scope.cidade = cidadeDefault;
                        cb();
                    });

                }
                else {
                    $scope.cidade = cidadeDefault;
                    $scope.estado = estadoDefault;
                    cb();
                }

            });
        },
        getPartidos: function(cb){
            Server.getPartidos(false, function(partidos){
                $scope.partidos = ["Escolha um partido"];
                for(var key in partidos)
                    $scope.partidos.push(key);
                cb();
            });
        }
    },$scope.efetuarBusca);

    $scope.changeEstado = function(){

        $scope.questoes.estaduais = [];
        Server.getQuestoes({estado:$scope.estado.sigla}, function(qs){
            for(var i = 0; i < qs.length; i++){
                if(qs[i].codigoestado) $scope.questoes.estaduais.push(qs[i]);
            }
        });

        $scope.cidade = cidadeDefault;
        $scope.efetuarBusca();
        Global.loadCidades($scope.estado.sigla,function(res){
            $scope.cidades = res;
            $scope.cidades.splice(0, 0, cidadeDefault);
            $scope.cidade = cidadeDefault;
        });
    }

    $scope.changeCidade = function(){
        $scope.questoes.municipais = [];
        Server.getQuestoes({cidade:$scope.cidade.id, estado:$scope.estado.sigla}, function(qs){
            for(var i = 0; i < qs.length; i++){
                if(qs[i].idcidade) $scope.questoes.municipais.push(qs[i]);
            }
        });
        $scope.efetuarBusca();
    }

    $scope.selecionarResposta = function(questao,resposta){
        if($scope.questao && $scope.questao.resposta == resposta &&
            $scope.questao.id == questao._id)
            return;

        if(!resposta) delete questao.resposta;
        else questao.resposta = resposta;

        questao.id = questao._id;
        $scope.questao = questao;
        $scope.efetuarBusca();
    }

    $scope.pergunte = function(url){
        window.location.href = "/" + url + "#!/pergunta";
    }
}