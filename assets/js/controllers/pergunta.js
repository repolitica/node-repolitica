'use strict';


function perguntaCtrl($scope, $location, $http, $sce, $window, CrossData, Global) {

    var info = CrossData.get();
    CrossData.set({});
    if(!info || !info.politico){
        Global.politico(function(pol){
            $scope.politico = pol;
            setIframe();
        });
    }
    else{
        $scope.politico = info.politico;
        setIframe();
    }

    var setIframe = function(){
        $http.post('/Alo/getAPUrl').success(function(data){

            $scope.iframeUrl = $sce.trustAsResourceUrl(data + "/" +
                $scope.politico.gabinete + "/widget#/repolitica");

        });
    }
}