'use strict';


function politicoCtrl($scope, $location, $http, $routeParams, $window, $anchorScroll, $sce, Server, Global, CrossData) {

    var polDiv = document.getElementById("politicodiv");
    var politico = JSON.parse(polDiv.innerHTML);

    var urlpolitico = politico.politicoid;
    var cidade = politico.cidade;
    var estado = politico.estado;
    var polid = politico.id;
    var gabinete = politico.gabinete;

    // vindo do AP
    var email = politico.email;
    var token = politico.token;

    $scope.widget = !!politico.widget;
    $scope.showCargoCidade = true;

    var redirect = function(){
        $window.location.href = "/#!/sem-politico";
    }

    var politicoView = function(response,r){
        if(response == "")
            redirect();
        else{
            if(!response.id) response.id = response._id;
            $scope.politico = response;
            $scope.nomecandidato = response.nomecandidatura || response.nomecompleto;
            var anoAtual = new Date().getFullYear();

            // *********** url para share/like ************
            var pTemp = response;
            $scope.urlShare = "https://www.repolitica.com.br" + Global.buildPoliticoUrl(pTemp);
            pTemp = null;

            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/pt_BR/all.js#xfbml=1&appId=327721127316035";
                fjs.parentNode.insertBefore(js, fjs);
            } (document, 'script', 'facebook-jssdk'));

            // *********** principal ************

            $scope.showCargoCidade = response.cargo ||
                response.partido || response.cidade || response.estado;
            if(response.cargo == "Nenhum") response.cargo = "";

            var partidoatual = response.partido;
            var estadoatual = response.estado || response.estadocandidatura;
            if(response.cargo && partidoatual)
                $scope.cargo = response.cargo + ", " + partidoatual;
            else if(response.cargo) $scope.cargo = response.cargo;
            else if(partidoatual) $scope.cargo = partidoatual;

            var cidade = "";
            var loadLocal = function(){
                if(cidade && response.estado)
                    $scope.local = cidade + " - " + response.estado;
                else if(cidade) $scope.local = cidade;
                else if(estadoatual) $scope.local = estadoatual;
                $scope.showBrCidade = $scope.local && $scope.cargo;
            }

            if(response.cidade && response.cidade != -1 && response.estado){
                Global.getCidade(response.estado, response.cidade, function(c){
                    cidade = c.nome;
                    loadLocal();
                })
            }
            else loadLocal();

            if(!response.imagemurl) $scope.imagemurl = "/img/noimage.png";
            else $scope.imagemurl = response.imagemurl;


            // *********** candidatura ************

            if(response.cargocandidatura){
                var partido = response.partidocandidatura;
                var estado = response.estadocandidatura;
                $scope.showCandidatura = true;
                $scope.textoCandidatura = "Candidato a " + response.cargocandidatura.toLowerCase();

                if(response.anocandidatura != null && anoAtual != response.anocandidatura)
                    $scope.textoCandidatura += " em " + response.anocandidatura;

                if(response.numero) $scope.textoCandidatura += ", número " + response.numero;
                if(partido) $scope.textoCandidatura += ", " + partido;
                if(estado) $scope.textoCandidatura += " - " + estado;


                if(response.cidadecandidatura && response.cidadecandidatura != -1 && estado)
                    Global.getCidade(estado, response.cidadecandidatura, function(c){
                        if(c!= null && c.nome != null)
                            response.cidadecandidatura = c.nome;
                    })
                else response.cidadecandidatura = "";
            }
            else{
                delete response.cidadecandidatura;
                delete response.estadocandidatura;
            }


            // *********** ideologia ************

            $scope.showEditarIdeologia = true;
            if(response.prioridades){
                $scope.prioridades = [];
                for(var i = 0 ; i < response.prioridades.length; i++){
                    if(!response.prioridades[i]) continue;
                    var prioridade = r.prioridades[response.prioridades[i].toString()];
                    $scope.prioridades.push({
                        index: i + 1,
                        prioridade: prioridade.prioridade,
                        texto: prioridade.texto
                    });
                }
                if($scope.prioridades.length > 0)
                    $scope.showEditarIdeologia = false;
            }

            if(response.economia != null){
                var enumEconomia = r.enumData["economia"][response.economia.toString()];
                $scope.economia = enumEconomia.tipo;
                $scope.descEconomia = enumEconomia.descricao;
                $scope.showEditarIdeologia = false;
            }

            if(response.sociedade != null){
                var enumSociedade = r.enumData["sociedade"][response.sociedade.toString()];
                $scope.sociedade = enumSociedade.tipo;
                $scope.descSociedade = enumSociedade.descricao;
                $scope.showEditarIdeologia = false;
            }

            if(response.radicalismo != null){
                var enumRadicalismo = r.enumData["radicalismo"][response.radicalismo.toString()];
                $scope.radicalismo = enumRadicalismo.tipo;
                $scope.descRadicalismo = enumRadicalismo.descricao;
                $scope.showEditarIdeologia = false;
            }


            // *********** opinioes ************

            if(response.questoes){

                var qlist = [];
                for(var id in response.questoes){
                    qlist.push(response.questoes[id]);
                }

                if(qlist.length != 0){
                    response.questoes = qlist;
                    for(var i = 0; i < response.questoes.length; i++){
                        response.questoes[i].respostaTexto =
                            r.enumData["questoes"][response.questoes[i].resposta.toString()];
                    }

                    $scope.questoes = _.groupBy(response.questoes, function(q){
                        if(!q.questao) return "null";
                        if(q.questao.idcidade) return "3";
                        if(q.questao.codigoestado) return "2";
                        if(q.questao.geral) return "0";
                        return "1";
                    })
                    delete $scope.questoes["null"];

                    if($scope.questoes["0"] == null)
                        $scope.questoes["0"] = [];

                    if($scope.questoes["1"] == null)
                        $scope.questoes["1"] = [];

                    if((response.estado || response.estadocandidatura) &&
                        $scope.questoes["2"] == null)
                        $scope.questoes["2"] = [];

                    if(((response.cidade && response.cidade != -1) ||
                        (response.cidadecandidatura && response.cidadecandidatura != -1))
                        && $scope.questoes["3"] == null)
                        $scope.questoes["3"] = [];

                    $scope.links = {
                        0: "opinioesgerais",
                        1: "opinioesfederais",
                        2: "opinioesestaduais",
                        3: "opinioesmunicipais"
                    }
                }
                else delete response.questoes;
            }


            // *********** atuacao ************

            if(response.urlexcelencias){
                $scope.percProjetos = (response.projetosirrelevantes / response.projetosapresentados) * 100;
                $scope.percProjetos = $scope.percProjetos.toString().split('.')[0].split(',')[0];
                var presTotal = response.presencas + response.faltasnaojustificadas + response.faltasjustificadas;
                $scope.percAssiduidade = (response.presencas / presTotal) * 100;
                $scope.percAssiduidade = $scope.percAssiduidade.toString().split('.')[0].split(',')[0];

                $scope.showOcorrencias = response.ocorrencias && response.ocorrencias.length > 0;
            }


            // *********** videos ************

            if(response.videos){
                if(response.videos.length == 0)
                    delete $scope.politico.videos;
                else
                    for(var i = 0; i < response.videos.length; i++){
                        var url = response.videos[i].url;
                        if(url.indexOf("youtube.com") > -1){
                            url = url.replace("http://", "");
                            url = url.replace("https://", "");
                            url = "//" + url.replace("watch?v=", "embed/");
                            response.videos[i].url = $sce.trustAsResourceUrl(url);
                        }
                        else if(url.indexOf("vimeo.com") > -1){
                            url = url.replace("http://", "");
                            url = url.replace("https://", "");
                            url = "//player.vimeo.com/video/" + url.split("/")[1];
                            response.videos[i].url = $sce.trustAsResourceUrl(url);
                        }
                        else response.videos[i].url = "";
                    }
            }


            // *********** historico ************

            if($scope.politico.partidos && $scope.politico.partidos.length > 0){
                var c = $scope.politico.partidos.length;
                if(!$scope.politico.partidos[c-1].anofim){
                    $scope.politico.partidos[c-1].anofim = "hoje";
                }
            }
            else delete $scope.politico.partidos;

            if($scope.politico.biografia)
                $scope.htmlBiografia =
                    $sce.trustAsHtml($scope.politico.biografia.replace(/\n/g,"<br/>"));

            // *********** propostas ************

            if($scope.politico.propostas && $scope.politico.propostas.length == 0)
                delete $scope.politico.propostas;


            // *********** dados pessoais ************

            $scope.showDadosPessoais = response.nomecompleto ||
                response.nascimentodia ||
                response.nascimentomes ||
                response.nascimentoano ||
                response.naturalestado ||
                ((response.naturalcidade && response.naturalcidade != -1) || response.naturalcidadestring) ||
                response.estadocivil ||
                response.ocupacao ||
                response.escolaridade;

            if(response.nascimentoano){
                var idade = anoAtual - response.nascimentoano;
                var dia = parseInt(response.nascimentodia);
                var mes = parseInt(response.nascimentomes);

                var hj = new Date();
                if(mes > hj.getMonth() + 1 || (mes == hj.getMonth() + 1 && dia > hj.getDate()))
                    idade--;

                if(mes<10) mes = "0" + mes;
                if(dia<10) dia = "0" + dia;
                var aniversario = dia + "/" +
                    mes + "/" +response.nascimentoano.toString().substring(2);
                $scope.idadeTexto = idade + " anos (" + aniversario + ")";
            };

            if(response.naturalcidade && response.naturalestado && response.naturalcidade != -1){
                Global.getCidade(response.naturalestado, response.naturalcidade, function(c){
                    if(!c || !c.nome)
                        $scope.naturalidade = response.naturalestado;
                    else
                        $scope.naturalidade = c.nome + " / " + response.naturalestado;
                })
            }
            else if(response.naturalcidadestring && response.naturalestado)
                $scope.naturalidade = response.naturalcidadestring + " / " + response.naturalestado;
            else if(response.naturalestado)
                $scope.naturalidade = response.naturalestado;


            // *********** perguntas ************

            if(!$scope.widget){
                $scope.showPergunta = false;
                $scope.showNaoCadastrado = false;
                $scope.showPerguntas = false;

                var url = "http://" + $location.host() + ":80" + "/widget/" + response.id;
                url = encodeURI(url).replace(/\//g, "%2F");
                url = encodeURI(url).replace(/:/g, "%3A");
                url = url.replace("www.","");
                //var url = "http://repolitica.com.br:80" + "/widget/" + response.id;

                $http.post('/Alo/getAPUrl').success(function(data){
                    $http.get(data + "/repolitica/stats?repoliticaUrl=" +
                            url).success(function(resp, status, headers, config){

                            if(resp.hasWidget){
                                $scope.showPergunta = true;
                                response.demandas = resp.demandas;
                                response.respondidos = resp.respondidos;

                                if(resp.respondidos > 0){
                                    $scope.showPerguntas = true;
                                    resp.lista = _.sortBy(resp.lista,function(r){ return r.dataCriacao; });
                                    var lista = [];
                                    for(var i = resp.lista.length - 1; i >= 0; i--) {
                                        if(resp.lista[i].posts.length == 0) continue;
                                        var l = resp.lista[i];
                                        l.htmlResposta = $sce.trustAsHtml(l.posts[l.posts.length-1].texto.replace(/(\r\n|\n|\r)/gm,"<br/>"));
                                        lista.push(l);
                                    }
                                    $scope.respostas = lista;

                                }
                            }
                            else
                                $scope.showNaoCadastrado = true;

                        });
                });

            }


            // *********** breadcrumb ************

            if(response.estadocandidatura && response.cidadecandidatura &&
                response.cidadecandidatura != -1)
                $scope.cidade = response.cidadecandidatura;
        }
    }


    // pega o político
    var getPolitico = function(r){

        var key = "";
        if(polid) key = polid;
        else{
            if(estado) key += estado;
            if(cidade) key += cidade;
            key += urlpolitico;
        }

        var dataPol = CrossData.getPerfil(key);
        if($scope.politico) {
            politicoView(dataPol,r);
            return;
        }

        $http.post('/politicos/getByPoliticoUrl', {
            urlpolitico: urlpolitico,
            cidade:cidade,
            estado:estado,
            perfil: true,
            id: polid,
            email: email,
            token: token,
            gabinete: gabinete,
        })
            .success(function (response) {
                if (!response.error) {
                    politicoView(response,r);
                    //CrossData.setPerfil(key,response);
                }
                else {
                    redirect();
                }
            }).error(function (response) {
                redirect();
            });
    }


    async.auto({
            prioridades: function(cb){
                Server.getPrioridades(function (data) {
                    cb('',data);
                });
            },
            enumData: function(cb){
                Server.getEnum(function (data) {
                    cb('',data);
                });
            }
        },
        function (err, r) {
            getPolitico(r);
        });


    // funnção para o anchor de html
    $scope.scrollTo = function(id) {
        $location.hash(id);
        $anchorScroll();
    }

    // breadcrumb

    $scope.listaEstado = function(){
        $window.location.href = "/#!/lista/" +  $scope.politico.estadocandidatura;
    }

    $scope.listaCidade = function(){
        $window.location.href = "/#!/lista/" +  $scope.politico.estadocandidatura + "/" +
            $scope.politico.cidadecandidatura.replace(new RegExp(" ", 'g'), '-');
    }

    $scope.listaPartido = function(){

        if(!$scope.politico.estadocandidatura) {
            $window.location.href = "/#!/lista/partido/" + $scope.politico.partido;
            return;
        }

        var c = $scope.politico.cidadecandidatura;
        if(!c) c = "partido"
        else c = c.replace(new RegExp(" ", 'g'), '-')

        $window.location.href = "/#!/lista/" +  $scope.politico.estadocandidatura + "/" +
            c + "/" + $scope.politico.partido;
    }

    $scope.cadastroPolitico = function(){
        $window.location.href = "/#!/cadastropolitico/" + $scope.politico.id || $scope.politico._id
    }
}