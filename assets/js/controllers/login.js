'use strict';


function loginCtrl($scope, $http, $location, $window, $routeParams, Server) {
    var backlink = $routeParams['b'];
    var hash = $routeParams['h'];

    Server.authenticated(function(id){
        if(id.user != null)
            $window.location.href = "/";
    })

    $scope.loading = false;

    $scope.login = function(){

        $scope.loading = true;
        var data = {
            username: $scope.email,
            password: $scope.password,
        }

        $http.post('/auth/process', data).
            success(function(data, status, headers, config) {
                if(!data.login){
                    $scope.error = data.message;
                }
                else {
                    if(!backlink)
                        $window.location.href = "/";
                    else {
                        if(hash) backlink += "#!/" + hash;
                        $window.location.href = backlink;
                    }

                }
                $scope.loading = false;
            }).
            error(function(data, status, headers, config) {
                $scope.error = "Erro no login";
                $scope.loading = false;
            });
    }

    $scope.voltar = function(){
        if(backlink) $window.location.href = backlink;
        else $window.history.back();
    }

    $scope.clearError = function(){
        $scope.error = "";
    }
}

function logoutCtrl($scope, $http, $location, $window) {

    $http.post('/auth/logout').success(function() {
        //$window.history.back();
    });
}

function resetPassCtrl($scope, $http, $location, $window, $routeParams){
    $scope.mostrar = false;

    var user ={
        userid:$routeParams["userid"],
        token:$routeParams["token"],
    }

    $http.post('/auth/tokenValido', user).
        success(function(data) {
            if(data.error){
                $scope.error = data.error;
            }
            else
            {
                $scope.mostrar = true;

                $scope.reset = function(){
                    $scope.error = "";

                    if(!$scope.password) {
                        $scope.error = "Senha inválida";
                        return;
                    }

                    user.password = $scope.password;

                    $http.post('/auth/resetarSenha', user).
                        success(function(data, status, headers, config) {
                            if(data.error){
                                $scope.error = data.error;
                            }
                            else
                                $window.location.href = "/#!/login";
                        }).
                        error(function(data, status, headers, config) {
                            $scope.error = "Um erro ocorreu. Tente novamente.";
                        });
                }
            }
        }).
        error(function(data) {
            $scope.error = "Um erro ocorreu";
        });

}

function enviarResetPassCtrl($scope, $http, $location, $window){
    $scope.enviando = false;
    $scope.enviar = function(){
        $scope.error = "";
        if(!$scope.email || $scope.email.length < 5){
            $scope.error = "Email inválido";
        }
        else{
            $scope.enviando = true;

            $http.post('/auth/emailNovaSenha', {email: $scope.email}).
                success(function(data, status, headers, config) {
                    if(data.error){
                        $scope.error = data.error;
                    }
                    else
                        $scope.msg = data.msg;
                    $scope.enviando = false;
                }).
                error(function(data, status, headers, config) {
                    $scope.error = "Um erro ocorreu. Tente novamente.";
                    $scope.enviando = false;
                });

        }
    }
}


function loginPoliticoCtrl($scope, $http, $location, $window, $sce, Server) {

    $http.post('/Alo/getAPUrl').success(function(data){

        $scope.iframeUrl =
            $sce.trustAsResourceUrl(data + "/repolitica#/login");

    });

    $scope.voltar = function(){
        $window.history.back();
    }
}