'use strict';

var checkLogin = function(scope, server, window, global, cb){
    server.authenticated(function(data){
        if(data.user == null){
            window.location.replace("/#!/login?b=" + window.location.pathname + "&h=" + window.location.hash.replace("#!/",""));
        }
        else {
            global.politico(cb);
            scope.user = data.user;
        }
    });
}

function templateCtrl($scope, $location, $http, $routeParams, $window, Server, Global, InputService) {
    var edicao = $routeParams["urlEdicao"];
    var pag = edicao;
    if(edicao.indexOf("opinioes") >=0) pag = "opinioes";
    $scope.templateUrl = "/partials/edicao/" + pag + ".html";

    var init = function(data){
        if(data.error) {
            console.log(data.error);
            $window.location.href = "#!/";
        }

        var bindPolitico = function(){
            if(!data.imagemurl) $scope.imagemurl = "/img/noimage.png";
            else $scope.imagemurl = data.imagemurl;
            $scope.politico = data;
        }

        var funcCtrl;
        switch(edicao){
            case "avaliacoes":
                funcCtrl = avaliacoesCtrl;
                break;
            case "candidatura":
                funcCtrl = edicaoCandidaturaCtrl;
                break;
            case "dadospessoais":
                funcCtrl = edicaoDadosPessoaisCtrl;
                break;
            case "historico":
                funcCtrl = edicaoHistoricoCtrl;
                break;
            case "ideologia":
                funcCtrl = edicaoIdeologiaCtrl;
                break;
            case "infoprincipais":
                funcCtrl = edicaoInfoPrincipaisCtrl;
                break;
            case "opinioesgerais":
            case "opinioesfederais":
            case "opinioesestaduais":
            case "opinioesmunicipais":
                funcCtrl = edicaoOpinioesCtrl;
                break;
            case "propostas":
                funcCtrl = edicaoPropostasCtrl;
                break;
            case "videos":
                funcCtrl = edicaoVideosCtrl;
                break;
        }
        bindPolitico();
        funcCtrl($scope, $location, $http, $routeParams, $window, Server, Global, edicao, InputService);
    }

    checkLogin($scope, Server, $window, Global, init);

    $scope.resetar = function(){
        $scope.politico = {};
        checkLogin($scope, Server, $window, Global, init);
    }
}

function avaliacoesCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {

}

function edicaoCandidaturaCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {

    if(!$scope.politico.cargo) $scope.politico.cargo = "Nenhum";
    var partidos = {};

    async.auto({
            estados: function(cb){
                Server.getEstados(function (data) {
                    cb('',data);
                });
            },
            partidos: function(cb){
                Server.getPartidos(false, function (ps){
                    $scope.partidos = [];
                    partidos = ps;

                    for(var key in ps)
                        $scope.partidos.push(key);
                    cb();
                });
            },
            cargos: function(cb){
                Server.getCargos(function (data) {
                    cb('',data);
                });
            },
            vices: function(cb){
                var cargo = $scope.politico.cargocandidatura;
                if($scope.politico.viceid &&
                    (cargo == "Presidente" || cargo == "Governador"|| cargo == "Prefeito"))
                    $http.post('/politicos/getById', { id:$scope.politico.viceid })
                        .success(function (response) {
                            if (!response.error) {
                                if(!response.imagemurl) response.imagemurl = "/img/noimage.png";
                                if(cargo == "Presidente")
                                    $scope.vicepresidente = response;
                                else if(cargo == "Governador")
                                    $scope.vicegovernador = response;
                                else if(cargo == "Prefeito")
                                    $scope.viceprefeito = response;
                            }
                            cb();
                        }).error(function (response) {
                            cb();
                        });
                else if(($scope.politico.suplente1id || $scope.politico.suplente2id) && cargo == "Senador"){
                    var ids = [$scope.politico.suplente1id, $scope.politico.suplente2id];
                    $http.post('/politicos/getById', { ids:ids })
                        .success(function (response) {
                            if (!response.error) {
                                for(var i = 0; i < response.length; i++){
                                    if(!response[i].imagemurl) response[i].imagemurl = "/img/noimage.png";
                                    if(response[i].id == $scope.politico.suplente1id)
                                        $scope.suplente1 = response[i];
                                    if(response[i].id == $scope.politico.suplente2id)
                                        $scope.suplente2 = response[i];
                                }
                            }
                            cb();
                        }).error(function (response) {
                            cb();
                        });

                }
                else cb();
            }
        },
        function (err, r) {
            $scope.$apply(function () {
                $scope.estados = r.estados;
                var estDefault = { "sigla" : "Escolha um estado" };
                if(!$scope.politico.estadocandidatura) $scope.politico.estadocandidatura = "Escolha um estado";
                $scope.estados.splice(0,0, estDefault);
                var cidCand = $scope.politico.cidadecandidatura;
                $scope.loadCidades();
                if(cidCand != null)
                    $scope.politico.cidadecandidatura = parseInt(cidCand);

                $scope.cargos = r.cargos;
                $scope.cargos.splice(0, 0, "Nenhum");
                   /*
                $scope.cargos.splice($scope.cargos.length, 0, "Outro");

                if(_.contains($scope.cargos, $scope.politico.cargocandidatura)){
                    $scope.showOutros = false;
                    $scope.politico.cargoOutro = "";
                }
                else{
                    $scope.showOutros = true;
                    $scope.politico.cargoOutro = $scope.politico.cargocandidatura;
                    $scope.politico.cargocandidatura = "Outro";
                }*/

                $scope.mudancaCargo();
                $scope.changeAnos();
            });
        });

    $scope.loadCidades = function(){
        Global.loadCidades($scope.politico.estadocandidatura,function(result){
            $scope.cidades = result;
            $scope.cidades.splice(0,0,{ "id": -1, "nome": "Escolha uma cidade" });
            $scope.politico.cidadecandidatura = -1;
        });
    }


    var salvarCargo = function(){

        if(($scope.politico.estadocandidatura && $scope.politico.estadocandidatura == "Escolha um estado") ||
            !$scope.mostrarlocal)
            delete $scope.politico.estadocandidatura

        if(($scope.politico.cidadecandidatura && $scope.politico.cidadecandidatura == "Escolha uma cidade") ||
            !$scope.mostrarcidade)
            delete $scope.politico.cidadecandidatura;

        if($scope.politico.cargocandidatura == "Nenhum") {
            $scope.politico.cargocandidatura = "";
            $scope.politico.partidocandidatura = "";
        }
        else if($scope.politico.cargocandidatura == "Outro") $scope.politico.cargocandidatura = $scope.politico.cargoOutro;
        delete $scope.politico.cargoOutro;

        $scope.politico.viceid = "";
        $scope.politico.suplente1id = "";
        $scope.politico.suplente2id = "";

        if($scope.politico.cargocandidatura == "Presidente" && $scope.vicepresidente != null  && $scope.vicepresidente.id != null)
            $scope.politico.viceid = $scope.vicepresidente.id;

        if($scope.politico.cargocandidatura == "Prefeito" && $scope.viceprefeito != null  && $scope.viceprefeito.id != null)
            $scope.politico.viceid = $scope.viceprefeito.id;

        if($scope.politico.cargocandidatura == "Governador" && $scope.vicegovernador != null  && $scope.vicegovernador.id != null)
            $scope.politico.viceid = $scope.vicegovernador.id;

        if($scope.politico.cargocandidatura == "Senador" && $scope.suplente1 != null  && $scope.suplente1.id != null)
            $scope.politico.suplente1id = $scope.suplente1.id;

        if($scope.politico.cargocandidatura == "Senador" && $scope.suplente2 != null  && $scope.suplente2.id != null)
            $scope.politico.suplente2id = $scope.suplente2.id;

        var cargo = $scope.politico.cargocandidatura;
        if(cargo.indexOf("Suplente") != -1) cargo = "Suplente";

        if($scope.politico.anocandidatura && $scope.politico.anocandidatura.toString() == new Date().getFullYear().toString())
            $scope.eleito = false;

        switch(cargo){
            case "Vice-presidente":
            case "Vice-governador":
            case "Suplente":
            case "Vice-prefeito":
                delete $scope.politico.numero;
                delete $scope.politico.coligacao;
                break;
        }

        switch(cargo){
            case "Vice-presidente":
            case "Presidente":
                $scope.politico.estadocandidatura = "";
            case "Suplente":
            case "Senador":
            case "Vice-governador":
            case "Governador":
            case "Deputado Estadual":
            case "Deputado Federal":
                $scope.politico.cidadecandidatura = -1;
                break;
        }
    }

    var validar = function(){

        if(!$scope.politico.anocandidatura){
            $scope.error = "Faltou o ano da candidatura.";
            return false;
        }
        if(!$scope.politico.cargocandidatura){
            $scope.error = "Faltou o cargo da candidatura.";
            return false;
        }
        if($scope.politico.numero && $scope.politico.partidocandidatura){
            var partido = partidos[$scope.politico.partidocandidatura];
            if(partido != null){

                switch($scope.politico.cargocandidatura){
                    case "Presidente":
                    case "Vice-presidente":
                    case "Governador":
                    case "Vice-governador":
                    case "Vice-prefeito":
                    case "Prefeito":
                        $scope.politico.numero = parseInt(partido.numero);
                        break;
                    default:
                        if($scope.politico.numero.toString().indexOf(partido.numero) != 0){
                            $scope.error = "O número deverá começar com " + partido.numero;
                            return false;
                        }
                }

            }
        }

        return true;

    }

    $scope.salvar = function(){
        if(!validar()) return;
        salvarCargo();
        Global.updatePolitico($scope.politico, $scope.user.id, function(){

            if(window.location.href.indexOf("widget") > 0){
                window.location.href = "#!/";
            }
            else
                Global.linkPolitico($scope.politico, function(url){
                    window.location.href = "/" + url;
                });
        }, function(e){ $scope.error=e.error;});
    }

    $scope.salvarproximo = function(){
        if(!validar()) return;
        salvarCargo();
        Global.updatePolitico($scope.politico, $scope.user.id, function(){

            if(window.location.href.indexOf("widget") > 0){
                window.location.href = "#!/ideologia";
            }
            else
                Global.linkPolitico($scope.politico, function(url){
                    window.location.href = "/" + url + "/#!/ideologia";
                });
            }, function(e){ $scope.error=e.error;});
    }

    $scope.changePartidos = function(){
    }

    $scope.changeAnos = function(){
        var anoAtual = new Date().getFullYear();
        if($scope.politico.anocandidatura == anoAtual){
            $scope.showEleito = false;
            $scope.politico.eleito = false;
        }
        else $scope.showEleito = true;
    }

    var indexAno = -1;
    var anos = [];
    anos.push(_.range(2014, 2002, -4));
    anos.push(_.range(2012, 2006, -4));
    $scope.mudancaCargo = function(){

        $scope.showOutros = $scope.politico.cargocandidatura == "Outro";

        var cargo = $scope.politico.cargocandidatura;
        var nIndex = 0
        if(cargo.indexOf("Suplente") != -1) cargo = "Suplente";

        $scope.mostrarpartido = true;
        $scope.mostrarnumero = true;
        $scope.mostrarlocal = true;
        switch(cargo){
            case "Nenhum":
                $scope.mostrarlocal = false;
                $scope.mostrarpartido = false;
                break;
            case "Presidente":
            case "Vice-presidente":
                $scope.mostrarlocal = false;
            case "Governador":
            case "Vice-governador":
                nIndex = 0;
                $scope.mostrarnumero = false;
                $scope.mostrarcidade = false;
                break;
            case "Senador":
            case "Suplente":
                nIndex = 0;
                $scope.numeromask = "___";
                $scope.numeromax = 3;
                $scope.mostrarcidade = false;
                break;
            case "Deputado Federal":
                nIndex = 0;
                $scope.numeromask = "____";
                $scope.numeromax = 4;
                $scope.mostrarcidade = false;
                break;
            case "Deputado Estadual":
                nIndex = 0;
                $scope.numeromask = "_____";
                $scope.numeromax = 5;
                $scope.mostrarcidade = false;
                break;
            case "Prefeito":
            case "Vice-prefeito":
                nIndex = 1;
                $scope.mostrarnumero = false;
                $scope.mostrarcidade = true;
                break;
            case "Vereador":
                nIndex = 1;
                $scope.numeromask = "_____";
                $scope.numeromax = 5;
                $scope.mostrarcidade = true;
                break;

        }

        if(indexAno != nIndex){
            if(indexAno != -1)
                $scope.politico.anocandidatura = "";
            $scope.anos = anos[nIndex];
            indexAno = nIndex;
        }

        switch(cargo){
            case "Vice-presidente":
            case "Vice-governador":
            case "Suplente":
            case "Vice-prefeito":
            case "Nenhum":
                $scope.mostrarDados = false;
                break;
            default:
                $scope.mostrarDados = true;
                break;
        }

    }

    $scope.buscarVicePrefeito = !($scope.politico.cargocandidatura == "Prefeito" && $scope.politico.viceid);
    $scope.buscarViceGovernador = !($scope.politico.cargocandidatura == "Governador" && $scope.politico.viceid);
    $scope.buscarVicePresidente = !($scope.politico.cargocandidatura == "Presidente" && $scope.politico.viceid);
    $scope.buscarSuplente1 = !($scope.politico.cargocandidatura == "Senador" && $scope.politico.suplente1id);
    $scope.buscarSuplente2 = !($scope.politico.cargocandidatura == "Senador" && $scope.politico.suplente2id);

    jQuery(function(){
        setAutcomplete('inputviceprefeito', {cargocandidatura:"Vice-prefeito"}, "", function(value, data){
            $scope.viceprefeito = {};
            $scope.viceprefeito = value;
            $scope.buscarVicePrefeito = false;
        });
    });

    jQuery(function(){
        setAutcomplete('inputvicegovernador', {cargocandidatura:"Vice-governador"}, "", function(value, data){
            $scope.vicegovernador = {};
            $scope.vicegovernador = value;
            $scope.buscarViceGovernador = false;
        });
    });

    jQuery(function(){
        setAutcomplete('inputvicepresidente', {cargocandidatura:"Vice-presidente"}, "", function(value, data){
            $scope.$apply(function(){
                $scope.vicepresidente = {};
                $scope.vicepresidente = value;
                $scope.buscarVicePresidente = false;
            });
        });
    });

    jQuery(function(){
        setAutcomplete('inputsuplente1', {cargocandidatura:"Suplente"}, "", function(value, data){
            $scope.$apply(function(){
                $scope.suplente1 = {};
                $scope.suplente1 = value;
                $scope.buscarSuplente1 = false;
            });
        });
    });
    jQuery(function(){
        setAutcomplete('inputsuplente2', {cargocandidatura:"Suplente"}, "", function(value, data){
            $scope.$apply(function(){
                $scope.suplente2 = {};
                $scope.suplente2 = value;
                $scope.buscarSuplente2 = false;
            });
        });
    });
}

function edicaoDadosPessoaisCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {

    async.auto({
            estados: function(cb){
                Server.getEstados(function (data) {
                    cb('',data);
                });
            },
        },
        function (err, r) {
            $scope.$apply(function () {
                $scope.estados = r.estados;
                var estDefault = { "sigla" : "Escolha um estado" };
                if(!$scope.politico.naturalestado) $scope.politico.naturalestado = "Escolha um estado";
                $scope.estados.splice(0,0, estDefault);
                $scope.loadCidades();
            });
        });

    $scope.loadCidades = function(){
        Global.loadCidades($scope.politico.naturalestado,function(result){
            $scope.cidades = result;
            $scope.cidades.splice(0,0,{ "id": -1, "nome": "Escolha uma cidade" });
            if(!$scope.politico.naturalcidade) $scope.politico.naturalcidade = -1;
        });
    }

    $scope.salvar = function(){
        if($scope.politico.nascimentoano && $scope.politico.nascimentoano < 1900){
            $scope.error = "Ano de nascimento inválido."
            return;
        }
        if($scope.politico.nascimentomes && ($scope.politico.nascimentomes < 1 || $scope.politico.nascimentomes > 12)){
            $scope.error = "Mês de nascimento inválido."
            return;
        }
        if($scope.politico.nascimentodia){
            if(!$scope.politico.nascimentomes){
                $scope.error = "Mês de nascimento inválido."
                return;
            }
            $scope.politico.nascimentomes = parseInt($scope.politico.nascimentomes);
            $scope.politico.nascimentodia = parseInt($scope.politico.nascimentodia);
            var mes = $scope.politico.nascimentomes;
            var dia = $scope.politico.nascimentodia;

            if(dia < 1 || dia > 31){
                $scope.error = "Dia de nascimento inválido."
                return;
            }

            if((mes == 4 || mes == 6 || mes == 9 || mes == 11) && dia >30){
                $scope.error = "Dia de nascimento inválido."
                return;
            }

            if(mes == 2 && dia > 29){
                $scope.error = "Dia de nascimento inválido."
                return;
            }
        }

        if($scope.politico.naturalestado && $scope.politico.naturalestado == "Escolha um estado")
            delete $scope.politico.naturalestado;

        Global.updatePolitico($scope.politico, $scope.user.id, function(){ window.location.href = "#!/"; });
    }
}

function edicaoHistoricoCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {
    Server.getPartidos(true, function(partidos){
        $scope.partidos = [];
        for(var key in partidos)
            $scope.partidos.push(key);
        if($scope.politico.partidos == null) $scope.politico.partidos = [];
    });
    $scope.anos = _.range(new Date().getFullYear(), 1840, -1);

    var validar = function(){

        if($scope.politico.biografia && $scope.politico.biografia.length > 1000){
            $scope.error = "Biografia deverá conter menos de 1000 caracteres.";
            return false;
        }

        var anosaida = false;
        for(var i = 0; i < $scope.politico.partidos.length; i++){
            var partido = $scope.politico.partidos[i];

            if(!partido.anoinicio){
                $scope.error = "Todos os partidos deverão ter um ano de entrada."
                return false;
            }

            if(!anosaida && !partido.anofim) anosaida = true;
            else if(anosaida && !partido.anofim) {
                $scope.error = "Político poderá ter somente um partido atual (marcado com hoje)"
                return false;
            }

            if(partido.anoinicio > partido.anofim){
                $scope.error = "O ano de entrada deverá ser anterior ao de saída."
                return false;
            }

        }
        $scope.politico.partidos = _.sortBy($scope.politico.partidos, function(partido){
            if(!partido.anofim) return 3000;
            return partido.anofim;
        });
        return true;
    }

    $scope.salvar = function(){
        if(!validar()) return;
        Global.updatePolitico($scope.politico, $scope.user.id,
            function(){ window.location.href = "#!/opinioesgerais"; },
            function(r){ if(r && r.error) $scope.error = r.error; });
    }

    $scope.salvarproximo = function(){
        if(!validar()) return;
        Global.updatePolitico($scope.politico, $scope.user.id,
            function(){ window.location.href = "#!/opinioesgerais"; },
            function(r){ if(r && r.error) $scope.error = r.error; });
    }

    $("#biografiaText").keyup(function () {
        var cmax = $("#rem_" + $(this).attr("id")).attr("title");

        if ($(this).val().length >= cmax) {
            $(this).val($(this).val().substr(0, cmax));
        }

        $("#rem_" + $(this).attr("id")).text(cmax - $(this).val().length);

    });
}

function edicaoIdeologiaCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {

    var enData = {};
    var prioData = {};

    var update = function(cb){
        if($scope.politico.prioridades.length == 2 &&
            $scope.politico.prioridades[0] == $scope.politico.prioridades[1] &&
            $scope.politico.prioridades[0] && $scope.politico.prioridades[1]){
            $scope.error = "As prioridades deverão ser diferentes";
            return;
        }

        if($scope.politico.economia == "") $scope.politico.economia = null;
        if($scope.politico.sociedade == "") $scope.politico.sociedade = null;
        if($scope.politico.radicalismo == "") $scope.politico.radicalismo = null;
        Global.updatePolitico($scope.politico, $scope.user.id, cb);
    }

    $scope.salvar = function(){
        update(function(){ window.location.href = "#!/"; });
    }

    $scope.salvarproximo = function(){
        update(function(){ window.location.href = "#!/historico"; });
    }

    $scope.changeEco = function(){
        if(!$scope.politico.economia) $scope.ecoText = " ";
        else $scope.ecoText = (enData["economia"][$scope.politico.economia].descricao || " ");
    }

    $scope.changeRad = function(){
        if(!$scope.politico.radicalismo) $scope.radText = " ";
        else $scope.radText = (enData["radicalismo"][$scope.politico.radicalismo].descricao || " ");
    }

    $scope.changeSoc = function(){
        if(!$scope.politico.sociedade) $scope.socText = " ";
        else $scope.socText = (enData["sociedade"][$scope.politico.sociedade].descricao || " ");
    }

    $scope.changePrio = function(i){
        if(!$scope.politico.prioridades[i]) $scope["prioText"+i] = " ";
        else $scope["prioText"+i] = (prioData[$scope.politico.prioridades[i]].texto || " ");
    }

    Server.getEnum(function (enumData) {
        enData = enumData;
        $scope.economia = [{k:"", value:"Não sei"}];
        for(var key in enumData["economia"]){
            var d = enumData["economia"][key];
            $scope.economia.push({k:key, value: d.tipo});
        }

        $scope.sociedade = [{k:"", value:"Não sei"}];
        for(var key in enumData["sociedade"]){
            var d = enumData["sociedade"][key];
            $scope.sociedade.push({k:key, value: d.tipo});
        }

        $scope.radicalismo = [{k:"", value:"Não sei"}];
        for(var key in enumData["radicalismo"]){
            var d = enumData["radicalismo"][key];
            $scope.radicalismo.push({k:key, value: d.tipo});
        }

        if($scope.politico.economia == null) $scope.politico.economia = "";
        else $scope.politico.economia = $scope.politico.economia.toString();

        if($scope.politico.sociedade == null) $scope.politico.sociedade = "";
        else $scope.politico.sociedade = $scope.politico.sociedade.toString();

        if($scope.politico.radicalismo == null) $scope.politico.radicalismo = "";
        else $scope.politico.radicalismo = $scope.politico.radicalismo.toString();

        $scope.changeEco();
        $scope.changeRad();
        $scope.changeSoc();
    });

    Server.getPrioridades(function(enumPrioridades){
        prioData = enumPrioridades;
        $scope.prioridades = [{k:"", value:"Não sei"}];
        for(var key in prioData){
            var d = prioData[key];
            $scope.prioridades.push({k:key, value: d.prioridade});
        }

        if($scope.politico.prioridades){
            for(var i = 0; i < $scope.politico.prioridades.length; i++){
                if($scope.politico.prioridades[i] == null) $scope.politico.prioridades[i] = "";
                else $scope.politico.prioridades[i] = $scope.politico.prioridades[i].toString();
            }
        } else $scope.politico.prioridades = ["", ""]

        for(var i = 0; i < $scope.politico.prioridades.length; i++)
            $scope.changePrio(i);
    });
}

function edicaoInfoPrincipaisCtrl($scope, $location, $http, $routeParams, $window, Server, Global, url, InputService) {

    var imgUpload;
    var imgData;
    $scope.getFile = function (file) {
        imgUpload = file;
        $scope.progress = 0;
        InputService.readAsDataUrl(file, $scope)
            .then(function(result) {
                //imgData = result;
                $scope.imagemurl = result;
            });
        InputService.readAsBinaryString(file, $scope)
            .then(function(result) {
                imgData = result;
                //$scope.imagemurl = result;
            });
    };

    $scope.$on("fileProgress", function(e, progress) {
        $scope.progress = progress.loaded / progress.total;
    });

    if(!$scope.politico.imagemurl) $scope.imagemurl = "/img/noimage.png";
    else $scope.imagemurl = $scope.politico.imagemurl;

    if(!$scope.politico.cargo) $scope.politico.cargo = "Nenhum";

    async.auto({
            partidos: function(cb){
                Server.getPartidos(true, function (data) {
                    cb('',data);
                });
            },
            estados: function(cb){
                Server.getEstados(function (data) {
                    cb('',data);
                });
            },
            cargos: function(cb){
                Server.getCargos(function (data) {
                    cb('',data);
                });
            },
        },
        function (err, r) {
            $scope.$apply(function () {
                $scope.partidos = r.partidos;
                $scope.estados = r.estados;
                $scope.estados.splice(0,0,{ "sigla": "", "nome": "" });
                $scope.cargos = r.cargos;
                $scope.cargos.splice(0, 0, "Nenhum");
                $scope.cargos.splice($scope.cargos.length, 0, "Outro");

                if(_.contains($scope.cargos, $scope.politico.cargo)){
                    $scope.showOutros = false;
                    $scope.politico.cargoOutro = "";
                }
                else{
                    $scope.showOutros = true;
                    $scope.politico.cargoOutro = $scope.politico.cargo;
                    $scope.politico.cargo = "Outro";
                }

                if(!$scope.politico.estado && $scope.politico.estadocandidatura){
                    $scope.politico.estado = $scope.politico.estadocandidatura;
                }

                if(!$scope.politico.cidade && $scope.politico.cidadecandidatura){
                    $scope.politico.cidade = $scope.politico.cidadecandidatura;
                }
                if($scope.politico.cidade)
                    $scope.loadCidades();
            });
        });

    $scope.loadCidades = function(){
        Global.loadCidades($scope.politico.estado,function(result){
            $scope.cidades = result;
            $scope.cidades.splice(0,0,{ "id": -1, "nome": "" });
        });
    }

    var salvar = function(){
        if($scope.politico.cargo == "Nenhum") $scope.politico.cargo = "";
        else if($scope.politico.cargo == "Outro") $scope.politico.cargo = $scope.politico.cargoOutro;
        delete $scope.politico.cargoOutro;
    }

    var postFile = function(cb){
        if(!$scope.politico.nomecandidatura){
            $scope.error = "O político precisa ter um nome.";
            return;
        }

        if(imgUpload && imgUpload.size > 1048576){
            $scope.error = "Imagem deverá ter menos de 1 MB.";
            return;
        }

        if($scope.saving) return;
        salvar();
        if(!imgUpload) return cb();
        $scope.saving = true;

        $http.post('/files/uploadFile', { file:imgUpload, politico: $scope.politico.politicourl, data:imgData })
            .success(function (response) {
                if (!response.error) {
                    $scope.politico.imagemurl = response.result.fileUrl;
                    cb();
                }
                else {
                    $scope.saving = false;
                    $scope.error = response.error;
                }
            }).error(function (response) {
                $scope.saving = false;
                $scope.error = "Um erro inexperado ocorreu ao salvar a imagem.";
            });
    }

    $scope.salvar = function(){
        postFile(function(){
            Global.updatePolitico($scope.politico, $scope.user.id, function(){
                if(window.location.href.indexOf("widget") > 0){
                    window.location.href = "#!/";
                }
                else
                    Global.linkPolitico($scope.politico, function(url){
                        window.location.href = "/" + url;
                    });
            },function(er){
                if(er && er.error)
                    $scope.error = er.error;
            });
        });
    }

    $scope.salvarproximo = function(){
        postFile(function(){
            Global.updatePolitico($scope.politico, $scope.user.id, function(){
                if(window.location.href.indexOf("widget") > 0){
                    window.location.href = "#!/candidatura";
                }
                else
                    Global.linkPolitico($scope.politico, function(url){
                        window.location.href = "/" + url + "/#!/candidatura";
                    });
            },function(er){
                if(er && er.error)
                    $scope.error = er.error;
            });
        });
    }

    $scope.mudancaCargo = function(){
        $scope.showOutros = $scope.politico.cargo == "Outro";
    }

}

function perguntaPoliticosCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {

}

function edicaoPropostasCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {

    if($scope.politico.propostas == null) $scope.politico.propostas = [];

    var validar = function(){

        for(var i = 0; i < $scope.politico.propostas.length; i++){
            if(!$scope.politico.propostas[i].titulo) {
                $scope.error = "Todas as propostas devem conter títulos";
                return false;
            }
        }

        return true;
    }

    $scope.salvar = function(){
        if(validar())
            Global.updatePolitico($scope.politico, $scope.user.id, function(){ window.location.href = "#!/"; });
    }

    $scope.salvarproximo = function(){
        if(validar())
            Global.updatePolitico($scope.politico, $scope.user.id, function(){ window.location.href = "#!/videos"; });
    }
}

function edicaoOpinioesCtrl($scope, $location, $http, $routeParams, $window, Server, Global, edicao) {

    var proxLink = "";
    var q = {};
    switch(edicao){
        case "opinioesgerais":
            $scope.tituloTema = "gerais";
            proxLink = "opinioesfederais";
            q.geral = true;
            break;
        case "opinioesfederais":
            $scope.tituloTema = "federais";
            if($scope.politico.estado || $scope.politico.estadocandidatura)
                proxLink = "opinioesestaduais";
            else
                proxLink = "propostas";
            q.geral = false;
            break;
        case "opinioesestaduais":
            $scope.tituloTema = "estaduais";

            if(($scope.politico.cidade && $scope.politico.cidade != -1) ||
                ($scope.politico.cidadecandidatura && $scope.politico.cidadecandidatura != -1))
                proxLink = "opinioesmunicipais";
            else
                proxLink = "propostas";
            q.estado = $scope.politico.estado || $scope.politico.estadocandidatura;
            break;
        case "opinioesmunicipais":
            $scope.tituloTema = "municipais";
            proxLink = "propostas";
            q.estado = $scope.politico.estado || $scope.politico.estadocandidatura;
            q.cidade = ($scope.politico.cidade && $scope.politico.cidade != -1) ||
                ($scope.politico.cidadecandidatura && $scope.politico.cidadecandidatura != -1);
            break;
    }

    Server.getQuestoes(q, function(qs){
        if(qs.length == 0) {
            window.location.href = "#!/" + proxLink;
            return;
        }

        $scope.respostas = {
            //  id:{resposta:0, justificativa:""}
        };
        if($scope.politico.questoes)
            for(var i in $scope.politico.questoes){
                $scope.respostas[i] = $scope.politico.questoes[i];
            }

        $scope.questoes = qs;
    });

    $scope.opiniaoPolitico = function(id,resposta){
        $("button").blur();
        if($scope.respostas[id] == null) $scope.respostas[id] = {resposta:0, justificativa:""};
        if($scope.respostas[id].resposta == resposta)
            $scope.respostas[id].resposta = 0;
        else $scope.respostas[id].resposta = resposta;
    }

    $scope.editarJustificativa = function(id){
        if($scope.respostas[id] == null) $scope.respostas[id] = {resposta:0, justificativa:""};
        $scope.respostas[id].showJustificativa = true;
    }

    var salvarRespostas = function(){
        // {idquestao,resposta,justificativa}
        $scope.politico.questoes = $scope.politico.questoes || {};

        for(var id in $scope.respostas){
            if($scope.politico.questoes[id])
                delete $scope.politico.questoes[id].showJustificativa;
            var novaResp = true;

            for(var i in $scope.politico.questoes){
                if(i == id){
                    novaResp = false;
                    $scope.politico.questoes[i].resposta = $scope.respostas[id].resposta;
                    $scope.politico.questoes[i].justificativa = $scope.respostas[id].justificativa;
                }
            }

            if(novaResp)
                $scope.politico.questoes[id] = {
                    resposta: $scope.respostas[id].resposta,
                    justificativa: $scope.respostas[id].justificativa
                };
        }
    }

    $scope.salvar = function(){
        salvarRespostas();
        Global.updatePolitico($scope.politico, $scope.user.id, function(){ window.location.href = "#!/"; });
    }

    $scope.salvarproximo = function(){
        salvarRespostas();
        Global.updatePolitico($scope.politico, $scope.user.id, function(){ window.location.href = "#!/" + proxLink; });
    }

}

function edicaoVideosCtrl($scope, $location, $http, $routeParams, $window, Server, Global) {

    if($scope.politico.videos == null) $scope.politico.videos = [];

    var fixUrl = function(){
        for(var i = 0; i < $scope.politico.videos.length; i++){
            var url = $scope.politico.videos[i].url;
            url = url.replace("https://", "");
            $scope.politico.videos[i].url = url;
        }
    }

    var validar = function(){
        for(var i = 0; i < $scope.politico.videos.length; i++){
            var url = $scope.politico.videos[i].url;
            var titulo = $scope.politico.videos[i].titulo;
            if(!titulo){
                $scope.error = "Todos os videos deverão ter títulos.";
                return false;
            }
            if(!url || (url.indexOf("youtube.com") == -1 && url.indexOf("vimeo.com") == -1 )){
                $scope.error = "Somente videos do youtube e do vimeo.";
                return false;
            }
        }
        return true;
    }

    $scope.salvar = function(){
        if(!validar()) return;
        fixUrl();
        Global.updatePolitico($scope.politico, $scope.user.id, function(){ window.location.href = "#!/"; });
    }

    $scope.salvarproximo = function(){
        if(!validar()) return;
        fixUrl();
        Global.updatePolitico($scope.politico, $scope.user.id, function(){ window.location.href = "#!/dadospessoais"; });
    }

}