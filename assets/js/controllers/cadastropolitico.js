'use strict';


function cadastroPoliticoCtrl($scope, $location, $http, $window, $sce, Server, $routeParams) {
    $scope.showEstado = true;
    $scope.politicoSelecionado = false;
    $scope.loading = false;

    $scope.voltar = function(){
        $window.history.back();
    }

    var apurl = ""
    async.auto({
            apurl: function(cb){
                $http.post('/Alo/getAPUrl').success(function(data){
                    apurl = data;
                    cb();
                });
            },
            estados: function(cb){
                Server.getEstados(function (data) {
                    cb('',data);
                });
            },
            cargos: function(cb){
                Server.getCargos(function (data) {
                    for(var i = data.length -1; i >=0; i--){
                        if(data[i] == "Prefeito" || data[i] == "Vice-prefeito" || data[i] == "Vereador")
                            data.splice(i,1);
                        if(data[i] == "Suplente") data[i] = "Suplente de Senador";
                    }
                    cb('',data);
                });
            },
        },
        function (err, r) {
            $scope.$apply(function () {
                $scope.estados = r.estados;
                $scope.estados.splice(0,0,{ "nome": "" });
                $scope.cargos = r.cargos;
                $scope.cargos.splice(0, 0, "Nenhum");
            });
        });

    var polid = $routeParams["polid"];
    if(polid){

    }

    $scope.changeCargo = function(){
        if($scope.cargocandidatura == "Presidente" || $scope.cargocandidatura == "Vice-presidente"){
            delete $scope.estado;
            $scope.showEstado = false;
        }
        else $scope.showEstado = true;
        $scope.buscarPolitico();
    }

    $scope.buscarPolitico = function(){
        delete $scope.politico
        $scope.msg = "";
        if($scope.numero == null || $scope.numero.length < 2) return;
        $scope.politicoSelecionado = false;

        var cargo = $scope.cargocandidatura;
        if(cargo == "Suplente de Senador") cargo = "Suplente";
        $scope.loading = true;

        $http.post('/politicos/buscaCadastro', {
            numero: $scope.numero,
            estado: $scope.estado,
            cargocandidatura: cargo
        }).success(function (response) {
                if (!response.error) {
                    $scope.msg = "";
                    delete $scope.politico;
                    if(response.length == 0) $scope.msg = "Candidato não cadastrado no Repolitica";
                    else if(response.length > 1) $scope.msg = "Foi encontrado mais de um candidato.";
                    else {
                        var pol = response[0];
                        if(!pol.id) pol.id = pol._id;

                        pol.textoCandidato = "";
                        if(pol.cargocandidatura && !pol.eleito){
                            pol.textoCandidato = "Candidato";
                            pol.textoCandidato += " a " + pol.cargocandidatura.toLowerCase();
                            if(pol.numero) pol.textoCandidato += ", N&ordm; " + pol.numero;
                            var anoAtual = new Date().getFullYear();
                            if(pol.anocandidatura && pol.anocandidatura != anoAtual) pol.textoCandidato += ", em " + pol.anocandidatura;
                        }
                        if(pol.textoCandidato)
                            pol.textoCandidato += "<br />";

                        pol.textoCargoLocal = "";
                        if(pol.cargo) pol.textoCargoLocal = pol.cargo;
                        if(pol.textoCargoLocal && pol.estado)
                            pol.textoCargoLocal += " - ";
                        if(pol.estado)
                            pol.textoCargoLocal += pol.estado;
                        $scope.politico = pol;
                        $scope.politicoSelecionado = true;

                        var url = $location.host() + "/widget/" + pol.id;
                        url = encodeURI(url).replace(/\//g, "%2F");
                        url = encodeURI(url).replace(/:/g, "%3A");
                        url = url.replace("www.","");

                        $scope.iframeUrl =
                            $sce.trustAsResourceUrl(apurl + "/repolitica#/registro/" +
                                encodeURI(pol.nomecandidatura) + "/" + encodeURI(pol.cargocandidatura) +
                                "/" + url);

                        setTimeout(function(){
                            $scope.$apply(function(){
                                $scope.loading = false;
                                $scope.politicoSelecionado = true;
                            });
                        }, 1000);
                    }
                }
                else {
                }
            }).error(function (response) {
            });
    }

    $scope.cadastrar = function(){
        if(!$scope.politico || !$scope.politico.id)
            $scope.error = "Por favor, indique o perfil de candidato adequadamente.";
        else if(!$scope.telefone)
            $scope.error = "O telefone será necessário para validação.";
        else if(!$scope.email)
            $scope.error = "Email inserido incorretamente";
        else if($scope.email != $scope.emailrepetido)
            $scope.error = "Os emails não são iguais.";
        else if(!$scope.password)
            $scope.error = "Senha inserida incorretamente.";
        else{
            $http.post('/auth/createPolitico', {
                email: $scope.email,
                password: $scope.password,
                politicoid: $scope.politico.id,
                nome:$scope.politico.nomecandidatura,
                telefone: $scope.telefone
            }).success(function(data, status, headers, config) {
                    if(data.created)
                        $window.location.href = "/#!/cadastropolitico/confirmacao";
                    else $scope.error = data.msg;
                }).
                error(function(data, status, headers, config) {
                    $scope.error = "Erro no cadastro"
                });
        }
    }

}