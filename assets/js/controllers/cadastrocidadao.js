'use strict';


function cadastroCidadaoCtrl($scope, $location, $http, $window) {
    $scope.teste = "Cadastro Cidadao";
    $scope.recebernovidades = true;

    $scope.cadastrar = function(){
        if(!$scope.nome){
            $scope.error = "Você deverá inserir o seu nome completo";
        }
        else if(!$scope.email){
            $scope.error = "Email inserido incorretamente";
        }
        else if($scope.email != $scope.emailrepetido){
            $scope.error = "Email deverá ser confirmado corretamente";
        }
        else if(!$scope.password){
            $scope.error = "Senha inserida incorretamente";
        }
        else{
            $http.post('/auth/create', {
                email: $scope.email,
                password: $scope.password,
                nome:$scope.nome,
                recebernovidades: $scope.recebernovidades
            }).success(function(data, status, headers, config) {
                    if(data.created)
                        $window.location.href = "/#!/cadastrocidadao/confirmacao";
                    else {
                        $scope.error = data.msg;
                    }
                }).
                error(function(data, status, headers, config) {
                    $scope.error = "Erro no cadastro"
                });
        }
    }

    $scope.clearError = function(){
        $scope.error = "";
    }

    $scope.voltar = function(){
        $window.history.back();
    }
}


function validarCadastroCtrl($scope, $location, $routeParams, $http, $window) {

    $scope.show = false;
    var user ={
        userid:$routeParams["userid"],
        token:$routeParams["token"],
    }
    var link = "";
    $http.post('/auth/validarEmail', user).success(function(data, status, headers, config) {
            if(data.error){
                $window.location.href = "/";
            }
            else {
                if(data.politicoLink)
                    link = data.politicoLink;
                $scope.show = true;
            }
        }).
        error(function(data, status, headers, config) {
            $window.location.href = "/";
        });

    $scope.irPerfil = function(){
        $window.location.href = link;
    }

}