'use strict';


function contatoCtrl($scope, $location, $http) {
    $scope.loading = false;
    $scope.enviar = function(){
        if(!$scope.nome){
            $scope.erro = "Nome inválido.";
        }
        else if(!$scope.email){
            $scope.erro = "Email inválido.";
        }
        else if(!$scope.mensagem || $scope.mensagem.length < 3){
            $scope.erro = "Mensagem inválida.";
        }
        else if($scope.mensagem.length > 10000){
            $scope.erro = "Desculpe, mensagem muito longa.";
        }
        else{
            $scope.loading = true;
            $scope.msg = "";
            $scope.erro = "";
            $http.post('/contato/enviarcontato', { nome: $scope.nome, email:$scope.email, mensagem:$scope.mensagem })
                .success(function (response) {
                    if (!response.error) {
                        $scope.msg = response.msg;
                        $scope.loading = false;
                    }
                    else {
                        $scope.erro = response.error;
                        $scope.loading = false;
                    }
                }).error(function(response){
                    $scope.loading = false;

                });
        }

    }
}