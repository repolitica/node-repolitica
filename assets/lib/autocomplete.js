

var autocompletion;
var root = location.host;
if (root.indexOf("https://") < 0){
    root = "https://" + root.replace("http://","");
}

var reEscape = new RegExp('(\\' + ['/', '.', '*', '+', '?', '|', '(', ')', '[', ']', '{', '}', '\\'].join('|\\') + ')', 'g');

var setAutcomplete = function (inputClass, params, mainClass, onselect) {
    if(!inputClass) inputClass = "inputBusca";

    var fnCustomFormat = function(value, data, currentValue) {
        var pattern = '(' + currentValue.replace(reEscape, '\\$1') + ')';
        var tostr = function(){
            return this.nome;
        }
        value.toString = tostr;

        return "<div><img align=\"absmiddle\" style=\"width:42px\" src=\"" + value.imagemurl + "\" />&nbsp;&nbsp;" +
            value.nome.replace(new RegExp(pattern, 'gi'), '<strong>$1<\/strong>') + '</div>';
    };

    var onselectDefault = function (value, data) {
        window.location = value.politicourl;
    };

    if(!onselect) onselect = onselectDefault;

    autocompletion = $('.' + inputClass).each(function () {

        var autoOpt = {
            serviceUrl: root + "/politicos/buscaRapida/",
            minChars: 3,
            zIndex: 9999,
            width: 350,
            delimiter: /(,|;)\s*/, // regex or character
            deferRequestBy: 300, //miliseconds
            noCache: false, //default is false, set to true to disable caching
            fnFormatResult: fnCustomFormat,
            // callback function:
            onSelect: onselect
        };

        if(params) autoOpt.params = params;
        if(mainClass) autoOpt.mainClass = mainClass;

        $(this).autocomplete(autoOpt);
    });
}