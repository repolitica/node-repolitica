require('newrelic');

/**
 * app.js
 *
 * Use `app.js` to run your app without `sails lift`.
 * To start the server, run: `node app.js`.
 * 
 * This is handy in situations where the sails CLI is not relevant or useful.
 *
 * For example:
 *   => `node app.js`
 *   => `forever start app.js`
 *   => `node debug app.js`
 *   => `modulus deploy`
 *   => `heroku scale`
 * 
 *
 * The same command-line arguments are supported, e.g.:
 * `node app.js --silent --port=80 --prod`
 */

// Ensure a "sails" can be located:
var sails;
try {
	sails = require('sails');
}
catch (e) {
	console.error('To run an app using `node app.js`, you usually need to have a version of `sails` installed in the same directory as your app.');
	console.error('To do that, run `npm install sails`');
	console.error('');
	console.error('Alternatively, if you have sails installed globally (i.e. you did `npm install -g sails`), you can use `sails lift`.');
	console.error('When you run `sails lift`, your app will still use a local `./node_modules/sails` dependency if it exists,');
	console.error('but if it doesn\'t, the app will run with the global sails instead!');
	return;
}


//AWS settings
//var config = require('./config/local');
require('aws-sdk').config.update({ accessKeyId: process.env.AWS_ACCESS_KEY_ID, secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY });

// Start server
sails.lift(null,function(){
    console.log("lifted");
    sails.config.session.store.generatebckp = sails.config.session.store.generate;
    sails.config.session.store.generate = function (req){
        //console.log("generate fn");
        //10.112.35.239
        //10.112.36
        //console.log(req.connection);
        try{
            //if(req.connection.remoteAddress) console.log("Remote Address: " + req.connection.remoteAddress);
            //if(req.url) console.log("URL: " + req.url);

            // rejeitar acesso
            var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
                || req.connection.remoteAddress;
            ip = ip.toString();
            if (ip.indexOf("202.46.48.") == 0 ||     // China
                ip.indexOf("202.46.49.") == 0 ||     // China
                ip.indexOf("202.46.50.") == 0 ||     // China
                ip.indexOf("202.46.61.") == 0 ||     // China
                ip.indexOf("202.46.62.") == 0 ||     // China
                ip.indexOf("202.46.63.") == 0 ||     // China
                ip.indexOf("180.76.6.") == 0 ||      // Baidu
                ip.indexOf("180.76.5.") == 0 ||      // Baidu
//                ip.indexOf("173.252.120.") == 0 ||   // Coréia do Sul
                ip == "37.58.100.156" ||             // Afeganistão
                ip == "144.76.185.173" ||            // Alemanha
                ip.indexOf("144.76.155") == 0 ||     // Alemanha
                ip.indexOf("144.76.113") == 0 ||     // Alemanha
                ip.indexOf("93.186.202") == 0 ||     // Alemanha
                ip == "148.251.75.46" ||             // Alemanha
                ip == "148.251.69.136" ||            // Alemanha
                ip == "144.76.163.48"                // Alemanha
                ){
                console.log("ignoring: " + ip);
                // make them wait a bit for a response (optional)
                ip = null;
//                setTimeout(function() {
//                    res.end();
//                }, 15000);
            }
            else if(req.url && req.url.indexOf("/cadastro/politico/") == -1 &&
                req.url.indexOf("/opiniao") == -1 &&
                req.url.indexOf("/Content/themes/") == -1 &&
                req.url.indexOf("/mural") == -1 &&
                req.url.indexOf("/perfil") == -1 &&
                req.url.indexOf("/contato/") == -1 &&
                req.url.indexOf(".jpg") == -1 &&
                req.url.indexOf(".png") == -1 &&
                req.url.indexOf(".gif") == -1 &&
                req.url.indexOf(".txt") == -1 &&
                req.url.indexOf(".css") == -1 &&
                req.url.indexOf(".ico") == -1 &&
                req.url.indexOf("/js/") == -1 &&
                req.url.indexOf("/lib/") == -1 &&
                req.url.indexOf("/staticdb/") == -1 &&
                req.url.indexOf("/partials/") == -1 &&
                req.url.indexOf("/candidatos/") == -1 &&
                req.url.indexOf("?_escaped_fragment_=") == -1 &&
                req.url.indexOf("/Profile/") == -1){
                //console.log("sessão criada: " + req.url + ": " + ip + "\n");
                sails.config.session.store.generatebckp(req);
            }
        }
        catch(ex){
            console.log(ex);
        }

        /*

         req.sessionID = uid(24);
         req.session = new Session(req);
         req.session.cookie = new Cookie(cookie);

         */
    }
    //console.log(sails.config.session.store);
    //var ob = {fn:sails.config.session.store.generate};
    //var JSONfn = require('./api/utils/JSONfn');
    //console.log(JSONfn.stringify(ob));
});

try{
    // somente em linux
    // https://github.com/lloyd/node-memwatch
    var memwatch = require('memwatch');
    memwatch.on('leak', function(info) {
        console.log(info);
    });
}
catch(ex){

}

