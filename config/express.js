/**
 * Configure advanced options for the Express server inside of Sails.
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#documentation
 */
function isFunction(functionToCheck) {
    var getType = {};
    return functionToCheck && getType.toString.call(functionToCheck) === '[object Function]';
}

var passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;

var rejectIp = function(req, res, next) {
    // rejeitar acesso
    var ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
        || req.connection.remoteAddress;
    ip = ip.toString();
    if (ip.indexOf("202.46.48.") == 0 ||     // China
        ip.indexOf("202.46.49.") == 0 ||     // China
        ip.indexOf("202.46.50.") == 0 ||     // China
        ip.indexOf("202.46.61.") == 0 ||     // China
        ip.indexOf("202.46.62.") == 0 ||     // China
        ip.indexOf("202.46.63.") == 0 ||     // China
        ip.indexOf("180.76.6.") == 0 ||      // Baidu
        ip.indexOf("180.76.5.") == 0 ||      // Baidu
        //ip.indexOf("173.252.120.") == 0 ||   // Coréia do Sul
        ip == "37.58.100.156" ||             // Afeganistão
        ip == "144.76.185.173" ||            // Alemanha
        ip.indexOf("144.76.155") == 0 ||     // Alemanha
        ip.indexOf("144.76.113") == 0 ||     // Alemanha
        ip.indexOf("93.186.202") == 0 ||     // Alemanha
        ip == "148.251.75.46" ||             // Alemanha
        ip == "148.251.69.136" ||            // Alemanha
        ip == "144.76.163.48"                // Alemanha
        ){
        ip = null;
        // make them wait a bit for a response (optional)
        setTimeout(function() {
            res.end();
        }, 15000);
    }
    else
        next();
};

module.exports.express = {
    loadMiddleware: function(app, defaultMiddleware, sails) {
        app.use(rejectIp);

        /*
        // for html5 mode
        app.get('(js|css)', function(req, res){
            console.log(req.url);
            if(req.url.indexOf("/js/") >= 0 ||
                req.url.indexOf("/css/") >= 0 ||
                req.url.indexOf("/img/") >= 0 ||
                req.url.indexOf("/partials/") >= 0 ||
                req.url.indexOf("/lib/") >= 0){
                res.sendfile('./assets' + req.url);
            }
        });
          */

        // Use the middleware in the correct order
        app.use(defaultMiddleware.startRequestTimer);
        app.use(defaultMiddleware.cookieParser);
        app.use(defaultMiddleware.session);

        // must use prerender service (hosted or running on server)
        //app.use(require('prerender-node'));

        app.use(defaultMiddleware.bodyParser);
        app.use(defaultMiddleware.handleBodyParserError);
        app.use(defaultMiddleware.compress);
        app.use(defaultMiddleware.methodOverride);
        app.use(defaultMiddleware.poweredBy);

        //custom
        app.use(passport.initialize());
        app.use(passport.session());

        app.use(defaultMiddleware.router);
        app.use(defaultMiddleware.www);
        app.use(defaultMiddleware.favicon);
        app.use(defaultMiddleware[404]);
        app.use(defaultMiddleware[500]);

    },

	// The middleware function used for parsing the HTTP request body.
	// (this most commonly comes up in the context of file uploads)
	//
	// Defaults to a slightly modified version of `express.bodyParser`, i.e.:
	// If the Connect `bodyParser` doesn't understand the HTTP body request 
	// data, Sails runs it again with an artificial header, forcing it to try
	// and parse the request body as JSON.  (this allows JSON to be used as your
	// request data without the need to specify a 'Content-type: application/json'
	// header)
	// 
	// If you want to change any of that, you can override the bodyParser with
	// your own custom middleware:
	// bodyParser: function customBodyParser (options) { ... },
	// 
	// Or you can always revert back to the vanilla parser built-in to Connect/Express:
	// bodyParser: require('express').bodyParser,
	// 
	// Or to disable the body parser completely:
	// bodyParser: false,
	// (useful for streaming file uploads-- to disk or S3 or wherever you like)
	//
	// WARNING
	// ======================================================================
	// Multipart bodyParser (i.e. express.multipart() ) will be removed
	// in Connect 3 / Express 4.
	// [Why?](https://github.com/senchalabs/connect/wiki/Connect-3.0)
	//
	// The multipart component of this parser will be replaced
	// in a subsequent version of Sails (after v0.10, probably v0.11) with:
	// [file-parser](https://github.com/mikermcneil/file-parser)
	// (or something comparable)
	// 
	// If you understand the risks of using the multipart bodyParser,
	// and would like to disable the warning log messages, uncomment:
	// silenceMultipartWarning: true,
	// ======================================================================



	// Cookie parser middleware to use
	//			(or false to disable)
	//
	// Defaults to `express.cookieParser`
	//
	// Example override:
	// cookieParser: (function customMethodOverride (req, res, next) {})(),



	// HTTP method override middleware
	//			(or false to disable)
	//
	// This option allows artificial query params to be passed to trick 
	// Sails into thinking a different HTTP verb was used.
	// Useful when supporting an API for user-agents which don't allow 
	// PUT or DELETE requests
	//
	// Defaults to `express.methodOverride`
	//
	// Example override:
	// methodOverride: (function customMethodOverride (req, res, next) {})()
};





/**
 * HTTP Flat-File Cache
 * 
 * These settings are for Express' static middleware- the part that serves
 * flat-files like images, css, client-side templates, favicons, etc.
 *
 * In Sails, this affects the files in your app's `assets` directory.
 * By default, Sails uses your project's Gruntfile to compile/copy those 
 * assets to `.tmp/public`, where they're accessible to Express.
 *
 * The HTTP static cache is only active in a 'production' environment, 
 * since that's the only time Express will cache flat-files.
 *
 * For more information on configuration, check out:
 * http://sailsjs.org/#documentation
 */
module.exports.cache = {

	// The number of seconds to cache files being served from disk
	// (only works in production mode)
	//maxAge: 31557600000
	maxAge: 3600
};
