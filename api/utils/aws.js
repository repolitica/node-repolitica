
var AWS = require('aws-sdk'),
    async = require('async'),
    fs = require('fs'),
    generatePassword = require('password-generator');
    //config = require('../../config/local');

exports.saveToCloud = function (politicourl, file, data, cb) {
    var originalFilename = file.name,
        mimeType = file.type,
        size = file.size,
        filename = politicourl + '_' + generatePassword(64, false, /\w/).toLowerCase(),
        fileUrl = 'https://' + process.env.S3_DOMAIN + '/' + process.env.S3_BUCKET + '/' + filename;

    async.auto({
        //readFile: function (cb) {
        //    fs.readFile(file.path, cb);
        //},
        saveFile: function (cb, r) {
            var s3 = new AWS.S3();
            var params = {
                ACL: 'public-read',
                Bucket: process.env.S3_BUCKET,
                Key: filename,
                Body: new Buffer(data, 'binary'),//r.readFile,
                ContentType: mimeType,
                ContentDisposition: 'inline; filename=' + originalFilename
            };
            s3.putObject(params, cb);
        }
    }, function (err) {
        cb(err, {
            fileUrl: fileUrl,
            mimeType: mimeType,
            size: size
        });
    });
};

exports.sendAWSEmails = function(source, toAddresses, ccAddresses, bccAddresses, subject, textBody, htmlBody, cb) {
    var ses = new AWS.SES({ region: process.env.AWS_REGION  });
    var params = {
        Source: source,
        Destination: {
            ToAddresses: toAddresses,
            CcAddresses: ccAddresses,
            BccAddresses: bccAddresses
        },
        Message: {
            Subject: {
                Data: subject
            },
            Body: {
                Text: {
                    Data: textBody
                },
                Html: {
                    Data: htmlBody
                }
            }
        },
        //ReplyToAddresses: replyToAddresses
    };
    ses.sendEmail(params, cb);
}