
var removeDiacritics = require('diacritics').remove;
var cidadesJson = require('../../assets/staticdb/cidades.json');

module.exports = {

    percentual: function(numero){
        var nString = numero.toString();
        if(nString.indexOf(".") == -1) return nString;

        var n = numero + 1E-5;
        n = parseInt(Math.floor(n * 10000));
        if(n < 10) return "0.0" + n + " %";
        if(n < 100) return "0." + n + " %";
        n = n.toString();
        if(n < 1000)
            return n[0] + "." + n[1] + n[2] + " %";
        return n[0] + n[1] + "." + n[2] + n[3] + " %";
    },

    separadorMilhar: function (x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    },

    buildPoliticoUrl: function (politico) {
        if(politico == null) return "";
        var url = "";
        if(politico.estadocandidatura) {
            url += "/" + politico.estadocandidatura;
            var cidade = politico.cidadecandidatura;
            if(cidade && cidade != -1 && cidadesJson[politico.estadocandidatura]) {
                var c = cidadesJson[politico.estadocandidatura][cidade];
                if(c && c.nome){
                    var cid = c.nome.toLowerCase();
                    cid = removeDiacritics(cid);
                    cid = cid.replace(new RegExp(" ", 'g'), '-');
                    url += "/" + cid;
                }
                else console.log(politico.politicourl + ": " + politico.estadocandidatura + " / " + cidade);
            }
        }

        return url + "/" + politico.politicourl;
    },
    // [ano][cargo][outros]
    // [14][20][000]
    pontos: function(politico){
        var pts = 0;
        if(politico.anocandidatura) pts += (politico.anocandidatura - 2000) * 100000;
        var cargo = politico.cargocandidatura || "";
        if(cargo.indexOf("Suplente")) cargo = "Suplente";

        switch(politico.cargocandidatura){
            case "Presidente": pts += 20000; break;
            case "Vice-presidente": pts += 19000; break;
            case "Governador": pts += 18000; break;
            case "Senador": pts += 17000; break;
            case "Vice-governador": pts += 16000; break;
            case "Deputado Federal": pts += 15000; break;
            case "Prefeito": pts += 14000; break;
            case "Deputado Distrital":
            case "Deputado Estadual": pts += 13000; break;
            case "Vereador": pts += 12000; break;
            case "Vice-prefeito": pts += 11000; break;
            case "Suplente":
            case "1º Suplente":
            case "2º Suplente": pts += 10000; break;
        }

        if(politico.economia) pts += 20;
        if(politico.sociedade) pts += 20;
        if(politico.radicalismo) pts += 20;
        if(politico.prioridades && politico.prioridades.length >= 1 && politico.prioridades[0]) pts += 20;
        if(politico.prioridades && politico.prioridades.length >= 2 && politico.prioridades[1]) pts += 20;

        if(politico.partidos && politico.partidos.length > 0) pts += 50;
        if(politico.biografia) pts += 50;

        if(politico.questoes){
            var opPts = 0;
            for(var id in politico.questoes){
                if(opPts >= 100){
                    opPts = 100;
                    break;
                }
                if(politico.questoes[id].resposta != 0) {
                    opPts += 10;
                    if(politico.questoes[id].justificativa)
                        opPts += 10;
                }
            }
            pts += opPts;
        }

        if(politico.propostas){
            var l = politico.propostas.length;
            if(l > 10) l = 10;
            pts += l * 10;
        }

        if(politico.videos){
            var l = politico.videos.length;
            if(l > 5) l = 5;
            pts += l * 2;
        }

        return pts;

    }
}