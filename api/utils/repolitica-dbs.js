var repoliticaUtils = require('../utils/repolitica-utils');
var mongoDb = require('../../node_modules/sails-mongo/node_modules/mongodb');
var ObjectID = mongoDb.ObjectID;
var async = require('async');
var _ = require('underscore');

// entre 2 pols.
var semelhanca = function(pol1 ,pol2){
    var ecoBase = 200;
    var radBase = 50;
    var socBase = 100;
    var qsBase = 100;
    var prio1 = 150;
    var prio2 = 100;
    var teste = 0;

    var maxPts = 0;
    var minPts = 0;

    var count = 0

    //---- economia
    if(pol1.economia != null && pol2.economia != null){
        var polResp = parseInt(pol1.economia);
        var usResp = parseInt(pol2.economia);
        var dif = Math.abs(polResp - usResp);
        teste += ecoBase * (1 - (dif / 2) );
        maxPts += ecoBase;
        minPts -= ecoBase;
        count++;
    }

    //---- radicalismo
    if(pol1.radicalismo != null && pol2.radicalismo != null){
        var polResp = parseInt(pol1.radicalismo);
        var usResp = parseInt(pol2.radicalismo);
        var dif = Math.abs(polResp - usResp);
        teste += radBase * (1 - dif);
        maxPts += radBase;
        minPts -= radBase;
        count++;
    }

    //---- sociedade
    if(pol1.sociedade != null && pol2.sociedade != null){
        var polResp = parseInt(pol1.sociedade);
        var usResp = parseInt(pol2.sociedade);
        var dif = Math.abs(polResp - usResp);
        teste += socBase * (1 - dif);
        maxPts += socBase;
        minPts -= socBase;
        count++;
    }

    //---- questoes
    var qs = false
    if(pol1.questoes && pol2.questoes)
        for(var id in pol1.questoes){
            qs = true;
            if(!pol2.questoes[id] || !pol2.questoes[id].resposta) continue;

            var polResp = 0;
            var dif = 0;
            var usResp = parseInt(pol2.questoes[id].resposta);
            if(pol1.questoes[id] != null){
                if(pol1.questoes[id].resposta != null)
                    polResp = parseInt(pol1.questoes[id].resposta);
                else
                    polResp = parseInt(pol1.questoes[id]);
                dif = Math.abs(polResp - usResp);
            }

            if(polResp == 0 || usResp == 0) continue;

            if(usResp == 1 || usResp == 4){
                if(dif < 1)
                    teste += qsBase * (1 - dif / 2);
                else if(dif > 2)
                    teste += qsBase * (1 / 2 - dif / 2);
                else teste += qsBase * (3 / 2 - dif);
            }
            // resposta moderada
            else{
                // resposta contrária
                if(dif > 1) teste += qsBase * (1 / 2 - dif / 2);
                else if (dif == 0) teste += qsBase;
                else if (usResp == 2 && polResp < 2 ||
                    usResp == 3 && polResp > 3)
                    teste += qsBase * (1 - dif / 2);
                else teste += qsBase * (1 - dif);
            }

            maxPts += qsBase;
            minPts -= qsBase;
        }

    //---- prioridades
    if(pol1.prioridades && pol2.prioridades){
        if(pol1.prioridades.length > 0 && pol1.prioridades[0] != null){
            var polResp = parseInt(pol1.prioridades[0]);
            var p = false;
            if(pol2.prioridades.length > 0 && pol2.prioridades[0] != null){
                var usResp = parseInt(pol2.prioridades[0]);
                if(polResp == usResp) teste += prio1;
                p = true
            }
            if(pol2.prioridades.length > 1 && pol2.prioridades[1] != null){
                var usResp = parseInt(pol2.prioridades[1]);
                if(polResp == usResp) teste += prio2;
                p = true
            }
            if(p){
                maxPts += prio1;
                count++;
            }
        }
        if(pol1.prioridades.length > 1 && pol1.prioridades[1] != null){
            var polResp = parseInt(pol1.prioridades[1]);
            var p = false;
            if(pol2.prioridades.length > 0 && pol2.prioridades[0] != null){
                var usResp = parseInt(pol2.prioridades[0]);
                if(polResp == usResp) teste += prio2;
                p = true
            }
            if(pol2.prioridades.length > 1 && pol2.prioridades[1] != null){
                var usResp = parseInt(pol2.prioridades[1]);
                if(polResp == usResp) teste += prio2;
                p = true
            }
            if(p){
                maxPts += prio2;
                count++;
            }
        }
    }


    /*if(count <= 1){
        var pp1 = pol1.partidocandidatura || pol1.partido;
        var pp2 = pol2.partidocandidatura || pol2.partido;

        if(pp1 == pp2) teste += 200;
        maxPts += 300;

        if(pol1.estado && pol2.estado){
            if(pol1.cidade != null || pol2.cidade != null){
                if(pol1.cidade == pol2.cidade && pol1.cidade != -1 && pol2.cidade != -1)
                    teste += 100;
                else teste -= 100
                maxPts += 100;
                minPts += 100;
            }
            if(pol1.estado == pol2.estado) teste += 100;
            maxPts += 100;
        }
        else {
            if(pol1.estadocandidatura == pol2.estadocandidatura)
                teste += 100;
            maxPts += 200;
        }
    }*/

    if(pol1.estadocandidatura == pol2.estadocandidatura){
        teste += 100;
        maxPts += 100;
    }
    else{
        var estDir = function(est){
            switch(est){
                case "PI":
                case "MA":
                case "CE":
                case "BA":
                case "RN":
                case "PB":
                case "SE":
                case "AM":
                case "PA":
                case "AP":
                case "TO":
                case "AL":
                    return -1;
                case "SP":
                case "SC":
                case "DF":
                case "PR":
                case "GO":
                    return 1;
            }
            return 0;
        }
        var est1 = estDir(pol1.estadocandidatura);
        var est2 = estDir(pol2.estadocandidatura);
        if(est1 == est2) teste *= 1;
        if(est1 + est2 == 0) teste *= 0.9;
        else teste *= 0.95;
    }

    if(maxPts - minPts < 700) return 0;

    var pts = (teste - minPts)/(maxPts - minPts);
    if(isNaN(pts)) pts = 0;

    return pts;
}

module.exports = {

    // grau x percentual de votos
    medrede:function(){

        Polconsolidados.native(function(err,collection){
            collection.find({anocandidatura: 2014}).toArray(function(err,ps){

                var pols = {};
                for (var k = 0; k < ps.length; k++){
                    pols[ps[k].polid] = ps[k];
                }

                Polsimilares.native(function(err,collection){
                    collection.find().toArray(function(err,sim){

                        var dict = {};

                        for(var j = 0; j < sim.length; j++){
                            var s = sim[j];

                            if(dict[s.id1] == null) dict[s.id1] = {grau: 0};
                            dict[s.id1].grau++;

                            if(dict[s.id2] == null) dict[s.id2] = {grau: 0};
                            dict[s.id2].grau++;
                        }

                        var arr = [];
                        for(var id in dict){
                            dict[id].nomecandidatura = pols[id].nomecandidatura;
                            dict[id].percentual = pols[id].percentual;
                            dict[id].polid = id;
                            arr.push(dict[id]);
                        }

                        arr = _.sortBy(arr,function(g){ return -g.percentual;});

                        console.log(arr);

                    });
                });

            });
        });

    },

    redesim:function(){

        var fs = require('fs');
        var gml = "graph\n[\n  directed 0";
        var ident = "\n    ";


        Polconsolidados.native(function(err,collection){
            collection.find({anocandidatura: 2014}).toArray(function(err,ps){

                var pols = {};
                for (var k = 0; k < ps.length; k++){
                    pols[ps[k].polid] = ps[k];
                }

                Polsimilares.native(function(err,collection){
                    collection.find().toArray(function(err,sim){

                        var dict = {};
                        var edges = "";
                        var cedge = 0;
                        var cnode = 0;

                        var i = 0;
                        for(var j = 0; j < sim.length; j++){
                            var s = sim[j];

                            if(dict[s.id1] == null) {
                                dict[s.id1] = i;
                                i++;
                            }
                            if(dict[s.id2] == null) {
                                dict[s.id2] = i;
                                i++;
                            }

                            var source = dict[s.id1];
                            var target = dict[s.id2];
                            edges += "\n  edge\n  [\n    source " + source + ident + "target " + target;
                            edges += "\n  ]";
                            cedge ++;
                        }

                        for(var id in dict){
                            gml += "\n  node\n  [\n    id " + dict[id] + ident + "label \"" + id + "\"";
                            gml += ident + "nome \"" + pols[id].nomecandidatura + "\"";
                            gml += ident + "percentual \"" + pols[id].percentual + "\"";
                            gml += "\n  ]";
                            cnode++;
                        }

                        gml += edges;
                        gml += "\n]";
                        fs.writeFile("repolitica.gml", gml);
                        console.log("edges: " + cedge);
                        console.log("nodes: " + cnode);

                    });
                });

            });
        });

    },

    polsimilares:function(){

        Polconsolidados.native(function(err,collection){
            collection.find({anocandidatura: 2014}).toArray(function(err,ps){
                var arrsem = []
                for(var i = 0; i < ps.length; i++){
                    var psem = { polid:ps[i].polid };
                    for(var j = i; j < ps.length; j++){
                        if(ps[i].polid == ps[j].polid) continue;
                        var pt = semelhanca(ps[i],ps[j]);
                        if(pt <= 0.9) continue;
                        arrsem.push({id1:ps[i].polid, id2:ps[j].polid, pt:pt});
                    }
                    if(i%1000 == 0)
                        console.log(arrsem.length);
                }
                console.log("total: " + arrsem.length);
                async.eachSeries(arrsem, function (doc, cb) {
                    Polsimilares.native(function(err,collection){
                        collection.insert(doc,cb);
                    });
                },function(){
                    console.log("fim!");
                })


            });
        });

    },

    consolidar: function(){

        Politicos.native(function(err,collection){
            collection.find({
                anocandidatura:2014,
                cargocandidatura:/Deputado/
            }).toArray(function(err,politicos){

                    console.log(politicos.length);
                    Votos.native(function(err,collection){
                        collection.find({
                            votos:{$gt:0},
                            cargo:/Deputado/
                        }).toArray(function(err,votos){

                                console.log(votos.length);

                                var cons = [];
                                for(var i = 0; i < politicos.length; i++){

                                    var pol = politicos[i];
                                    var idp = pol._id;
                                    for(var j = 0; j < votos.length; j++){
                                        var v = votos[j];
                                        if(v.idpolitico != idp) continue;

                                        var p = {
                                            nomecandidatura: pol.nomecandidatura,
                                            polid: idp,
                                            votos: v.votos,
                                            percentual: v.percentual,
                                            partido: pol.partido,
                                            anocandidatura: pol.anocandidatura,
                                            eleito: pol.eleito || v.eleito,
                                            cargocandidatura: pol.cargocandidatura,
                                            partidocandidatura: pol.partidocandidatura,
                                            estadocandidatura: pol.estadocandidatura,
                                            coligacao: pol.coligacao
                                        };

                                        if(pol.cargo != null)
                                            p.cargo = pol.cargo;
                                        if(pol.cidade != null)
                                            p.cidade = pol.cidade;
                                        if(pol.estado != null)
                                            p.estado = pol.estado;
                                        if(pol.economia != null)
                                            p.economia = pol.economia;
                                        if(pol.sociedade != null)
                                            p.sociedade = pol.sociedade;
                                        if(pol.radicalismo != null)
                                            p.radicalismo = pol.radicalismo;
                                        if(pol.prioridades != null)
                                            p.prioridades = pol.prioridades;
                                        if(pol.questoes != null)
                                            p.questoes = pol.questoes;

                                        cons.push(p);

                                        break;
                                    }
                                }

                                async.eachSeries(cons, function (doc, cb) {
                                    Polconsolidados.native(function(err,collection){
                                        console.log(doc.nomecandidatura);
                                        collection.insert(doc,cb);
                                    });
                                },function(){
                                    console.log("fim!");
                                })

                        });

                    });

                });
        });
    },

    usuariosnovidades: function(){
        Users.native(function(err,collection){
            collection.find({
                //​userid:{$exists:false},
                createdAt:{$exists:true, $gt:new Date("2014-10-02T00:00:00.000Z")},
                updatedAt:{$exists:true, $gt:new Date("2014-10-02T00:00:00.000Z")},
                recebernovidades: true
            }).toArray(function(err,users){
                    var us = [];
                    console.log("nome;email");
                    for(var i = 0; i < users.length; i++){
                        console.log(users[i].nome + ";" + users[i].email);
                    }
                });
        });
    },
    coligacoes: function(){
        var fs = require('fs');
        var estado = "TO";
        console.log(estado);
        fs.readFile('./assets/staticdb/coligacoes/' + estado.toLowerCase() + '.csv', "utf8", function (err, data) {
            if (err) throw err;
            data = data.split("\n");

            var coligacoes = {};
            var cargo = "";
            var ano = 2014;
            for(var i = 0; i < data.length; i++){
                if(data[i].indexOf("Subtotal") >= 0) cargo = "";
                if(data[i].indexOf("Deputado Federal") >= 0) cargo = "Deputado Federal";
                if(data[i].indexOf("Deputado Estadual") >= 0) cargo = "Deputado Estadual";
                if(data[i].indexOf("Deputado Distrital") >= 0) cargo = "Deputado Distrital";
                if(cargo == "") continue;
                var linha = data[i].split(';');
                if(linha.length < 3) continue;
                if(coligacoes[cargo] == null) coligacoes[cargo] = [];
                coligacoes[cargo].push(linha[2].replace(/"/g,"").replace(/\s/g,"").split("/"));
            }

            console.log(coligacoes);

            async.eachSeries(coligacoes["Deputado Estadual"], function (doc, cb) {
                Coligacoes.create({estado: estado, ano: ano, partidos: doc, cargo: "Deputado Estadual"}, function(err,col){
                    if(err) console.log(err);
                    cb();
                });
            });

            async.eachSeries(coligacoes["Deputado Federal"], function (doc, cb) {
                Coligacoes.create({estado: estado, ano: ano, partidos: doc, cargo: "Deputado Federal"}, function(err,col){
                    if(err) console.log(err);
                    cb();
                });
            });

        });

    },
    sitemap: function(){

        var sitemap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
        var ultTextBusca = "";
        var pag = 500;
        var buscaPols = function(){
            Politicos.native(function(err,collection){
                collection.find({textobusca:{$gt: ultTextBusca}},{limit: pag}).sort({textobusca: 1}).toArray(function(err,pols){

                    for(var i = 0; i < pols.length; i++){
                        var link = repoliticaUtils.buildPoliticoUrl(pols[i]);
                        sitemap += "\n<url><loc>http://www.repolitica.com.br" + link + "</loc></url>";
                    }
                    if(pols.length < pag)
                        fim();
                    else{
                        ultTextBusca = pols[pols.length - 1].textobusca;
                        console.log(ultTextBusca);
                        console.log("n: " + pols.length);
                        buscaPols();
                    }

                });
            });
        }

        var fim = function(){
            sitemap += "\n</urlset>";
            require('fs').writeFile("assets/sitemap.xml",sitemap);
            console.log("fim!!");
        }
        buscaPols();

    },
    prioridades: function(){
        var priosJson = require('../../assets/staticdb/priocid.json');
        var p = function(id,prio,cb){
            console.log(id);
            Politicos.findOne({or:[{politicianId:id},{politicianId2:id}]}).exec(function(err,pol){

                if(!pol) {
                    console.log("Não existe");
                    return cb();
                }
                if(pol.prioridades && pol.prioridades.length > 0 && pol.prioridades[0] != "") {
                    console.log("prioridade já estava preenchida");
                    return cb();
                }

                var prioid;
                switch(prio){
                    case "Assistencia": prioid = "0"; break;
                    case "Desenvolvimento": prioid = "1"; break;
                    case "Educacao": prioid = "2"; break;
                    case "Gestao": prioid = "3"; break;
                    case "Infraestrutura": prioid = "4"; break;
                    case "MeioAmbiente": prioid = "5"; break;
                    case "Politica": prioid = "6"; break;
                    case "Saude": prioid = "7"; break;
                    case "Seguranca": prioid = "8"; break;
                }
                if(!prioid) return cb();

                if(!pol.prioridades) pol.prioridades = [];
                pol.prioridades[0] = prioid;
                pol.save(function(err){
                    if(err) console.log(err);
                    console.log("prioridade salva");
                    cb();
                });

            });
        }

        console.log("inicio: prioridades");
        var polprios = [];
        for(var polid in priosJson){
            if(parseInt(polid) <= 270457 ) continue;
            polprios.push({id:polid, prio:priosJson[polid]});
        }

        async.eachSeries(polprios,function(pp,cb){
            p(pp.id,pp.prio,cb);
        },function(){
            console.log("fim: prioridades");
        })

    },
    limparbanco: function(){

        console.log("inicio");
        Sessions.destroy({session: /passport\":\{\}/}).
            limit(1).exec(function(err,r){
                console.log("sessões inuteis");
            });

        Testes.native(function(err,collection){
            collection.update({}, {$unset: {createdAt: "", updatedAt: ""}}, { multi: true },
                function(err,obj){
                    console.log("testes zip 1");
                }
            );
        });

        Testes.native(function(err,collection){
            collection.update({estado: ""}, {$unset: {estado: ""}}, { multi: true },
                function(err,obj){
                    console.log("testes unset estado");
                }
            );
        });

    },
    atualizareleitos: function(){
        console.log("rodando");

        Votos.find({eleito:true}).exec(function(err,vts){
            var ids = [];
            for(var i = 0; i < vts.length; i++){
                ids.push(new ObjectID(vts[i].idpolitico));
            }
            Politicos.native(function(err,collection){
                collection.update({_id: {$in:ids}}, {$set:{eleito:true}},{multi:true},
                    function(err,itens){
                        console.log(itens)});
            })
        });
    },

    testeEstatisticas: function(req,res){
        console.log("gerando...");

        var lastDate = "";
        var testes = [];
        var estats = function(){
            var governadores = {};
            var presidentes = {};
            var respostas = {totais:{}};
            var idsResp = {};
            var n = testes.length;
            for(var i = 0; i < n; i++){
                var t = testes[i];
                if(t.cargo == "Governador" && t.resultado != null && t.resultado[0] != null &&
                    t.resultado[0] != ""){
                    if(governadores[t.resultado[0]] == null)
                        governadores[t.resultado[0]] = 0;
                    governadores[t.resultado[0]]++;
                }
                if(t.cargo == "Presidente" && t.resultado != null && t.resultado[0] != null &&
                    t.resultado[0] != ""){
                    if(presidentes[t.resultado[0]] == null)
                        presidentes[t.resultado[0]] = 0;
                    presidentes[t.resultado[0]]++;
                }
                if(t.respostas != null){
                    for(var p in t.respostas){
                        idsResp[p] = true;
                        if(respostas.totais[p] == null) respostas.totais[p] = {};
                        if(respostas.totais[p][t.respostas[p]] == null) respostas.totais[p][t.respostas[p]] = 0;
                        respostas.totais[p][t.respostas[p]]+= 1;

                        // por estado
                        if(respostas[t.estado] == null) respostas[t.estado] = {};
                        if(respostas[t.estado][p] == null) respostas[t.estado][p] = {};
                        if(respostas[t.estado][p][t.respostas[p]] == null) respostas[t.estado][p][t.respostas[p]] = 0;
                        respostas[t.estado][p][t.respostas[p]]+=1;
                    }
                }

            }

            for(var est in respostas){
                var rest = respostas[est];
                for(var id in rest){
                    var total = 0;
                    for(var r in rest[id]) total+=rest[id][r];
                    for(var r in rest[id]){
                        console.log(rest[id][r]);
                        rest[id][r]/=total;
                    }
                }
            }

            var ids = [];

            var gs = [];
            for(var g in governadores){
                gs.push({id:g, n: governadores[g]})
            }
            gs = _.sortBy(gs,function(g){ return -g.n;});
            gs = _.first(gs,20);

            for(var i = 0; i < gs.length; i++){
                ids.push(new ObjectID(gs[i].id));
            }
            for(var p in presidentes){
                ids.push(new ObjectID(p));
            }

            var idsR = [];
            for(var t in idsResp){
                try{
                    var id = new ObjectID(t);
                    idsR.push(id);
                }
                catch(ex){}
            }

            Politicos.native(function(err,collection){
                collection.find({_id:{$in: ids}}).toArray(function(err,pols){
                    var govs = [];
                    for(var i = 0; i < gs.length; i++){
                        for(var j = 0; j < pols.length; j++){
                            if(pols[j]._id != gs[i].id) continue;
                            govs.push({
                                nome: pols[j].nomecandidatura,
                                estado: pols[j].estadocandidatura,
                                recomendacoes: gs[i].n
                            });
                            break;
                        }
                    }
                    console.log("Governadores");
                    console.log(govs);

                    var pres = [];
                    for(var p in presidentes){
                        for(var j = 0; j < pols.length; j++){
                            if(pols[j]._id != p) continue;
                            if(pols[j].cargocandidatura != "Presidente")
                            {
                                delete presidentes[p];
                                break;
                            }
                            pres.push({
                                nome: pols[j].nomecandidatura,
                                recomendacoes: presidentes[p]
                            });
                            break;
                        }
                    }
                    console.log("Presidentes");
                    console.log(pres);
                });
            });

            Questoes.native(function(err,collection){
                collection.find({_id:{$in: idsR}}).toArray(function(err,qs){
                    for(var es in respostas){
                        var respEst = respostas[es];
                        for(var r in respEst){
                            if(r == "economia"||
                                r == "radicalismo"||
                                r == "sociedade"||
                                r == "prioridade1"||
                                r == "prioridade2"){
                                if(r=="economia")
                                    for(var a in respEst[r]){
                                        switch(a){
                                            case "0": respEst[r][a] = "Direita - " + (respEst[r][a]*100) + "%"; break;
                                            case "1": respEst[r][a] = "Centro-Direita - " + (respEst[r][a]*100) + "%";break;
                                            case "2": respEst[r][a] = "Centro - " + (respEst[r][a]*100) + "%";break;
                                            case "3": respEst[r][a] = "Centro-Esquerda - " + (respEst[r][a]*100) + "%"; break;
                                            case "4": respEst[r][a] = "Esquerda - " + (respEst[r][a]*100) + "%"; break;
                                        }
                                    }
                                else if(r=="radicalismo")
                                    for(var a in respEst[r]){
                                        switch(a){
                                            case "0": respEst[r][a] = "Radical - " + (respEst[r][a]*100) + "%";  break;
                                            case "1": respEst[r][a] = "Meio Termo - " + (respEst[r][a]*100) + "%"; break;
                                            case "2": respEst[r][a] = "Moderado - " + (respEst[r][a]*100) + "%"; break;
                                        }
                                    }
                                else if(r=="sociedade")
                                    for(var a in respEst[r]){
                                        switch(a){
                                            case "0": respEst[r][a] = "Conservador - " + (respEst[r][a]*100) + "%";  break;
                                            case "1": respEst[r][a] = "Meio termo - " + (respEst[r][a]*100) + "%"; break;
                                            case "2": respEst[r][a] = "Liberal - " + (respEst[r][a]*100) + "%"; break;
                                        }
                                    }
                                else
                                    for(var a in respEst[r]){
                                        switch(a){
                                            case "0": respEst[r][a] = "Assistência - " + (respEst[r][a]*100) + "%";  break;
                                            case "1": respEst[r][a] = "Desenvolvimento - " + (respEst[r][a]*100) + "%"; break;
                                            case "2": respEst[r][a] = "Educação - " + (respEst[r][a]*100) + "%"; break;
                                            case "3": respEst[r][a] = "Gestão - " + (respEst[r][a]*100) + "%"; break;
                                            case "4": respEst[r][a] = "Infraestrutura - " + (respEst[r][a]*100) + "%"; break;
                                            case "5": respEst[r][a] = "Meio Ambiente - " + (respEst[r][a]*100) + "%"; break;
                                            case "6": respEst[r][a] = "Política - " + (respEst[r][a]*100) + "%"; break;
                                            case "7": respEst[r][a] = "Saúde - " + (respEst[r][a]*100) + "%"; break;
                                            case "8": respEst[r][a] = "Segurança - " + (respEst[r][a]*100) + "%"; break;
                                            case "9": respEst[r][a] = "Direitos Humanos - " + (respEst[r][a]*100) + "%"; break;
                                            case "10": respEst[r][a] = "Trabalho - " + (respEst[r][a]*100) + "%"; break;
                                        }
                                    }
                                continue;
                            }
                            for(var i = 0; i < qs.length; i++){
                                if(qs[i]._id.toString() != r) continue;
                                respEst[r].titulo = qs[i].titulo;
                                break;
                            }

                            for(var a in respEst[r]){
                                switch(a){
                                    case "0": respEst[r][a] = "Sem resposta - " + (respEst[r][a]*100) + "%";  break;
                                    case "1": respEst[r][a] = "Discorda - " + (respEst[r][a]*100) + "%"; break;
                                    case "2": respEst[r][a] = "Discorda Parcialmente - " + (respEst[r][a]*100) + "%"; break;
                                    case "3": respEst[r][a] = "Concorda Parcialmente - " + (respEst[r][a]*100) + "%";break;
                                    case "4": respEst[r][a] = "Concorda - " + (respEst[r][a]*100) + "%"; break;
                                }
                            }
                        }
                    }
                    console.log(respostas);
                });
            });
        }
        var teste = function(){
            var query = {};
            if(lastDate) query.dataInicio = {$lt: new Date(lastDate)};
            Testes.find(query).sort('dataInicio desc').limit(1000).exec(function(err, ts){
                testes = testes.concat(ts);
                lastDate = ts[ts.length-1].dataInicio;
                console.log(lastDate);
                if(ts.length == 1000) {
                    teste();
                }
                else{
                    estats();
                }
            })
        }
        teste();

    },

    politicosEstatisticas: function(req,res){
        console.log("estatisticas de politicos");

        var ultPontos;
        var pols = [];
        var estats = function(){
            // por partido
            // respostas[partido][id]
            var respostas = {};
            // para pegar as perguntas
            var idsResp = {};
            var n = pols.length;
            for(var i = 0; i < n; i++){

                var p = pols[i];
                var partido = p.partidocandidatura || p. partido;
                if(!partido) continue;
                if(respostas[partido] == null) respostas[partido] = {};

                if(p.questoes != null){
                    for(var q in p.questoes){
                        if(q == "") continue;
                        if(!p.questoes[q].resposta) continue;
                        idsResp[q] = true;

                        // por estado
                        if(respostas[partido][q] == null) respostas[partido][q] = {};
                        if(respostas[partido][q][p.questoes[q].resposta] == null) respostas[partido][q][p.questoes[q].resposta] = 0;
                        respostas[partido][q][p.questoes[q].resposta]+=1;
                    }
                }

                var ideologia = function(ideo){
                    if(p[ideo] != null){
                        if(respostas[partido][ideo] == null) respostas[partido][ideo] = {};
                        if(respostas[partido][ideo][p[ideo]] == null) respostas[partido][ideo][p[ideo]] = 0;
                        respostas[partido][ideo][p[ideo]]++;
                    }
                }
                ideologia("economia");
                ideologia("sociedade");
                ideologia("radicalismo");

                if(p.prioridades){

                    var prioridade = function(index){
                        if(p.prioridades[index] != ""){
                            var ideo = "prioridade" + (index + 1);
                            if(respostas[partido][ideo] == null) respostas[partido][ideo] = {};
                            if(respostas[partido][ideo][p.prioridades[index]] == null) respostas[partido][ideo][p.prioridades[index]] = 0;
                            respostas[partido][ideo][p.prioridades[index]]++;
                        }
                    }

                    if(p.prioridades.length >= 1) prioridade(0);
                    if(p.prioridades.length >= 2) prioridade(1);
                }
            }

            for(var ptds in respostas){
                var rp = respostas[ptds];
                for(var id in rp){
                    var total = 0;
                    for(var r in rp[id]) total+=rp[id][r];
                    for(var r in rp[id]){
                        rp[id][r]/=total;
                    }
                }
            }

            var idsR = [];
            for(var t in idsResp){
                try{
                    var id = new ObjectID(t);
                    idsR.push(id);
                }
                catch(ex){}
            }

            Questoes.native(function(err,collection){
                collection.find({_id:{$in: idsR}}).toArray(function(err,qs){
                    for(var ptd in respostas){
                        var respP = respostas[ptd];
                        for(var r in respP){
                            if(r == "economia"||
                                r == "radicalismo"||
                                r == "sociedade"||
                                r == "prioridade1"||
                                r == "prioridade2"){
                                if(r=="economia")
                                    for(var a in respP[r]){
                                        switch(a){
                                            case "0": respP[r][a] = "Direita - " + (respP[r][a]*100) + "%"; break;
                                            case "1": respP[r][a] = "Centro-Direita - " + (respP[r][a]*100) + "%";break;
                                            case "2": respP[r][a] = "Centro - " + (respP[r][a]*100) + "%";break;
                                            case "3": respP[r][a] = "Centro-Esquerda - " + (respP[r][a]*100) + "%"; break;
                                            case "4": respP[r][a] = "Esquerda - " + (respP[r][a]*100) + "%"; break;
                                        }
                                    }
                                else if(r=="radicalismo")
                                    for(var a in respP[r]){
                                        switch(a){
                                            case "0": respP[r][a] = "Radical - " + (respP[r][a]*100) + "%";  break;
                                            case "1": respP[r][a] = "Meio Termo - " + (respP[r][a]*100) + "%"; break;
                                            case "2": respP[r][a] = "Moderado - " + (respP[r][a]*100) + "%"; break;
                                        }
                                    }
                                else if(r=="sociedade")
                                    for(var a in respP[r]){
                                        switch(a){
                                            case "0": respP[r][a] = "Conservador - " + (respP[r][a]*100) + "%";  break;
                                            case "1": respP[r][a] = "Meio termo - " + (respP[r][a]*100) + "%"; break;
                                            case "2": respP[r][a] = "Liberal - " + (respP[r][a]*100) + "%"; break;
                                        }
                                    }
                                else
                                    for(var a in respP[r]){
                                        switch(a){
                                            case "0": respP[r][a] = "Assistência - " + (respP[r][a]*100) + "%";  break;
                                            case "1": respP[r][a] = "Desenvolvimento - " + (respP[r][a]*100) + "%"; break;
                                            case "2": respP[r][a] = "Educação - " + (respP[r][a]*100) + "%"; break;
                                            case "3": respP[r][a] = "Gestão - " + (respP[r][a]*100) + "%"; break;
                                            case "4": respP[r][a] = "Infraestrutura - " + (respP[r][a]*100) + "%"; break;
                                            case "5": respP[r][a] = "Meio Ambiente - " + (respP[r][a]*100) + "%"; break;
                                            case "6": respP[r][a] = "Política - " + (respP[r][a]*100) + "%"; break;
                                            case "7": respP[r][a] = "Saúde - " + (respP[r][a]*100) + "%"; break;
                                            case "8": respP[r][a] = "Segurança - " + (respP[r][a]*100) + "%"; break;
                                            case "9": respP[r][a] = "Direitos Humanos - " + (respP[r][a]*100) + "%"; break;
                                            case "10": respP[r][a] = "Trabalho - " + (respP[r][a]*100) + "%"; break;
                                        }
                                    }
                                continue;
                            }
                            for(var i = 0; i < qs.length; i++){
                                if(qs[i]._id.toString() != r) continue;
                                respP[r].titulo = qs[i].titulo;
                                break;
                            }

                            for(var a in respP[r]){
                                switch(a){
                                    case "0": respP[r][a] = "Sem resposta - " + (respP[r][a]*100) + "%";  break;
                                    case "1": respP[r][a] = "Discorda - " + (respP[r][a]*100) + "%"; break;
                                    case "2": respP[r][a] = "Discorda Parcialmente - " + (respP[r][a]*100) + "%"; break;
                                    case "3": respP[r][a] = "Concorda Parcialmente - " + (respP[r][a]*100) + "%";break;
                                    case "4": respP[r][a] = "Concorda - " + (respP[r][a]*100) + "%"; break;
                                }
                            }
                        }
                    }
                    console.log(respostas);
                });
            });
        }
        var findPols = function(){
            var query = {};
            if(ultPontos) query.pontos = {$lt: ultPontos};
            Politicos.find(query).sort('pontos desc').limit(1000).exec(function(err, ts){
                pols = pols.concat(ts);
                ultPontos = ts[ts.length-1].pontos || 0;
                if(ts.length == 1000) {
                    findPols();
                }
                else{
                    estats();
                }
            })
        }
        findPols();

    },

    resumoPoliticosEstatisticas: function(req,res){
        console.log("estatisticas de politicos");

        var ultTextobusca;
        var pols = [];
        var estats = function(){
            // respostas[id]
            var respostas = {};
            // para pegar as perguntas
            var idsResp = {};
            var n = pols.length;
            for(var i = 0; i < n; i++){

                var p = pols[i];

                if(p.questoes != null){
                    for(var q in p.questoes){
                        if(q == "") continue;
                        if(!p.questoes[q].resposta) continue;
                        idsResp[q] = true;

                        // por estado
                        if(respostas[q] == null) respostas[q] = {};
                        if(respostas[q][p.questoes[q].resposta] == null) respostas[q][p.questoes[q].resposta] = 0;
                        respostas[q][p.questoes[q].resposta]+=1;
                    }
                }

                var ideologia = function(ideo){
                    if(p[ideo] != null){
                        if(respostas[ideo] == null) respostas[ideo] = {};
                        if(respostas[ideo][p[ideo]] == null) respostas[ideo][p[ideo]] = 0;
                        respostas[ideo][p[ideo]]++;
                    }
                }
                ideologia("economia");
                ideologia("sociedade");
                ideologia("radicalismo");

                if(p.prioridades){

                    var prioridade = function(index){
                        if(p.prioridades[index] != ""){
                            var ideo = "prioridade" + (index + 1);
                            if(respostas[ideo] == null) respostas[ideo] = {};
                            if(respostas[ideo][p.prioridades[index]] == null) respostas[ideo][p.prioridades[index]] = 0;
                            respostas[ideo][p.prioridades[index]]++;
                        }
                    }

                    if(p.prioridades.length >= 1) prioridade(0);
                    if(p.prioridades.length >= 2) prioridade(1);
                }
            }

            for(var id in respostas){
                var total = 0;
                for(var r in respostas[id]) total+=respostas[id][r];
                for(var r in respostas[id]){
                    respostas[id][r]/=total;
                }
            }

            var idsR = [];
            for(var t in idsResp){
                try{
                    var id = new ObjectID(t);
                    idsR.push(id);
                }
                catch(ex){}
            }

            Questoes.native(function(err,collection){
                collection.find({_id:{$in: idsR}}).toArray(function(err,qs){

                    for(var r in respostas){
                        if(r == "economia"||
                            r == "radicalismo"||
                            r == "sociedade"||
                            r == "prioridade1"||
                            r == "prioridade2"){
                            if(r=="economia")
                                for(var a in respostas[r]){
                                    switch(a){
                                        case "0": respostas[r][a] = "Direita - " + (respostas[r][a]*100) + "%"; break;
                                        case "1": respostas[r][a] = "Centro-Direita - " + (respostas[r][a]*100) + "%";break;
                                        case "2": respostas[r][a] = "Centro - " + (respostas[r][a]*100) + "%";break;
                                        case "3": respostas[r][a] = "Centro-Esquerda - " + (respostas[r][a]*100) + "%"; break;
                                        case "4": respostas[r][a] = "Esquerda - " + (respostas[r][a]*100) + "%"; break;
                                    }
                                }
                            else if(r=="radicalismo")
                                for(var a in respostas[r]){
                                    switch(a){
                                        case "0": respostas[r][a] = "Radical - " + (respostas[r][a]*100) + "%";  break;
                                        case "1": respostas[r][a] = "Meio Termo - " + (respostas[r][a]*100) + "%"; break;
                                        case "2": respostas[r][a] = "Moderado - " + (respostas[r][a]*100) + "%"; break;
                                    }
                                }
                            else if(r=="sociedade")
                                for(var a in respostas[r]){
                                    switch(a){
                                        case "0": respostas[r][a] = "Conservador - " + (respostas[r][a]*100) + "%";  break;
                                        case "1": respostas[r][a] = "Meio termo - " + (respostas[r][a]*100) + "%"; break;
                                        case "2": respostas[r][a] = "Liberal - " + (respostas[r][a]*100) + "%"; break;
                                    }
                                }
                            else
                                for(var a in respostas[r]){
                                    switch(a){
                                        case "0": respostas[r][a] = "Assistência - " + (respostas[r][a]*100) + "%";  break;
                                        case "1": respostas[r][a] = "Desenvolvimento - " + (respostas[r][a]*100) + "%"; break;
                                        case "2": respostas[r][a] = "Educação - " + (respostas[r][a]*100) + "%"; break;
                                        case "3": respostas[r][a] = "Gestão - " + (respostas[r][a]*100) + "%"; break;
                                        case "4": respostas[r][a] = "Infraestrutura - " + (respostas[r][a]*100) + "%"; break;
                                        case "5": respostas[r][a] = "Meio Ambiente - " + (respostas[r][a]*100) + "%"; break;
                                        case "6": respostas[r][a] = "Política - " + (respostas[r][a]*100) + "%"; break;
                                        case "7": respostas[r][a] = "Saúde - " + (respostas[r][a]*100) + "%"; break;
                                        case "8": respostas[r][a] = "Segurança - " + (respostas[r][a]*100) + "%"; break;
                                        case "9": respostas[r][a] = "Direitos Humanos - " + (respostas[r][a]*100) + "%"; break;
                                        case "10": respostas[r][a] = "Trabalho - " + (respostas[r][a]*100) + "%"; break;
                                    }
                                }
                            continue;
                        }
                        for(var i = 0; i < qs.length; i++){
                            if(qs[i]._id.toString() != r) continue;
                            respostas[r].titulo = qs[i].titulo;
                            break;
                        }

                        for(var a in respostas[r]){
                            switch(a){
                                case "0": respostas[r][a] = "Sem resposta - " + (respostas[r][a]*100) + "%";  break;
                                case "1": respostas[r][a] = "Discorda - " + (respostas[r][a]*100) + "%"; break;
                                case "2": respostas[r][a] = "Discorda Parcialmente - " + (respostas[r][a]*100) + "%"; break;
                                case "3": respostas[r][a] = "Concorda Parcialmente - " + (respostas[r][a]*100) + "%";break;
                                case "4": respostas[r][a] = "Concorda - " + (respostas[r][a]*100) + "%"; break;
                            }
                        }
                    }
                    console.log(respostas);
                });
            });
        }
        var findPols = function(){
            var query = {};
            if(ultTextobusca) query.textobusca = {$gt: ultTextobusca};

            Politicos.native(function(err,collection){
                collection.find(query,{limit: 1000}).sort({textobusca: 1}).toArray(function(err,ps){
                    pols = pols.concat(ps);
                    if(ps.length == 0) return estats();
                    ultTextobusca = ps[ps.length-1].textobusca || "";
                    console.log(ultTextobusca);
                    if(ps.length == 1000) {
                        findPols();
                    }
                    else{
                        estats();
                    }

                })
            })
        }
        findPols();

    },

    inserirvotos: function(estado, cargo, turno2){
        var fs = require('fs');
        var ano = 2014;
        var turno = 1;
        var arq = './assets/staticdb/votos/' + cargo + " " + estado;
        if(turno2) arq += "1";
        fs.readFile(arq + '.csv', "utf8", function (err, data) {
            if (err) throw err;
            data = data.split("\n");
            console.log("começou");

            async.eachSeries(data, function (linha, cb) {
                var cels = linha.split(';');
                if(cels.length < 6) return cb();
                var numero = parseInt(cels[1]);
                if(isNaN(numero)) return cb();
                var eleito = cels[0].indexOf("*") == 0;
                var votos = parseInt(cels[4].replace(/\./g,""));
                var perc = parseFloat(cels[5].replace(",",".").replace(" %",""))/100;
                Politicos.findOne({
                    numero:numero,
                    cargocandidatura:cargo,
                    anocandidatura:ano,
                    estadocandidatura:estado
                }).exec(function(err,pol){
                        if(pol == null) {
                            console.log(numero + "-" + estado);
                            cb();
                        }
                        else{
                            var v = {
                                idpolitico:pol.id,
                                ano:ano,
                                //turno:turno,
                                votos: votos,
                                percentual: perc,
                                partido: pol.partidocandidatura,
                                cargo: cargo,
                                estado: estado
                            };

                            if(turno2) v.turno2 = eleito;
                            else v.eleito = eleito;

                            Votos.create(v, function(err,col){
                                if(err) console.log(err);
                                cb();
                            });
                        }
                    });
            },function(){
                console.log("terminou " + estado + " " + cargo)
            });
        });

    },

    inserirPresidente: function(turno){
        var fs = require('fs');
        var ano = 2014;
        fs.readFile('./assets/staticdb/votos/Presidente' + turno + '.csv', "utf8", function (err, data) {
            if (err) throw err;
            data = data.split("\n");
            console.log("começou");

            async.eachSeries(data, function (linha, cb) {
                var cels = linha.split(';');
                if(cels.length < 6) return cb();
                var numero = parseInt(cels[1]);
                if(isNaN(numero)) return cb();
                var turno2 = cels[0].indexOf("*") == 0;
                var votos = parseInt(cels[4].replace(/\./g,""));
                var perc = parseFloat(cels[5].replace(",",".").replace(" %",""))/100;
                Politicos.findOne({
                    numero:numero,
                    cargocandidatura:"Presidente",
                    anocandidatura:ano
                }).exec(function(err,pol){
                        if(pol == null) {
                            console.log(numero + "-" + estado);
                            cb();
                        }
                        else{
                            Votos.create({
                                idpolitico:pol.id,
                                ano:ano,
                                //turno:turno,
                                votos: votos,
                                percentual: perc,
                                partido: pol.partidocandidatura,
                                cargo: "Presidente",
                                turno2:turno2
                            }, function(err,col){
                                if(err) console.log(err);
                                cb();
                            });
                        }
                    });
            },function(){
                console.log("terminou Presidente")
            });
        });

    },

    consultaapemails: function(){

        var consultaap = function(collection, query, opts, cb){
            var MongoClient = mongoDb.MongoClient;
            MongoClient.connect(process.env.CLIENTDB_URL, function (err, mongodb) {
                console.log(mongodb);
                //mongoDb.collection(collection);//.find(query, opts).toArray(cb);
            });
        };

        var emails = {};
        var options = {
            "limit": 100,
            "sort": [
                ['dataEnviado', 'asc']
            ]
        };

        var dt;
        var f1 = function(){
            var query = {
                type: "mailinglist",
                bounce: true
            }
            if(dt != null) query.dataEnviado = {$gt:dt};
            consultaap("florianopesaro", query, options,function(err,res){
                var n = res.length;
                for(var i = 0; i < n; i++){
                    emails[res[i].email] = res[i].bounceType;
                }

                dt = res[n-1].dataEnviado;
                //if(n == 1000) f1();
            });
        }
        /*
         repoliticaDbs.consultaap("florianopesaro",{
         type: "emailcampanha",
         bounce: true
         }, options,function(err,res){
         for(var i = 0; i < res.length; i++){
         emails[res[i].email] = res[i].bounceType;
         }
         });
         */
        f1();
        for(var email in emails){
            console.log(email + ";" + emails[email]);
        }
    }
}