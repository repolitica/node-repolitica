
var crypto = require('crypto');

var encPass = function (password,salt) {
    if (!password) return '';
    return crypto.createHmac('sha1', salt).update(password).digest('hex');
}

module.exports = {
    authenticate: function (plainText, hashed_password, salt) {
        return encPass(plainText,salt) === hashed_password;
    },
    encryptPassword: function(pass){
        var salt = Math.round((new Date().valueOf() * Math.random())) + '';
        return {encryptedPass:encPass(pass,salt), salt:salt};
    },
}