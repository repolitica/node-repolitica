/**
 * AuthController
 *
 */
var passport = require('passport'),
    FacebookStrategy = require('passport-facebook').Strategy,
    auth = require('../utils/auth-utils'),
    ObjectID = require('../../node_modules/sails-mongo/node_modules/mongodb').ObjectID,
    aws = require('../utils/aws'),
    generatePassword = require('password-generator'),
    moment = require('moment'),
    globalConfig = require('../utils/globalconfig'),
    emailUtils = require('../utils/email'),
    repoliticaUtils = require('../utils/repolitica-utils');
var request = require('request');

module.exports = {

    // autenticações
    process: function(req, res){
        var email = req.body.username;
        var password = req.body.password;

        Users.findOne({email:email}).exec(function(err,user){

            var fLogin = function(){
                passport.authenticate('local', function(err, user, info) {
                    if ((err) || (!user)) {
                        return res.send({ message: "Email ou senha inválidos.", login: false });
                    }
                    req.logIn(user, function(err) {
                        if (err) res.send(err);
                        //http://speakingaword.blogspot.com.br/2013/08/authentication-in-angularjs-nodejs-and.html
                        res.cookie('user', JSON.stringify({'id': user.id}), { httpOnly: false } );
                        return res.send({ message: 'login successful', login: true });
                    });
                })(req, res);
            }

            if(!user || err)
                return res.send({ message: "Email ou senha inválidos.", login: false });
            if(user.politicoid && !user.politicovalidado)
                return res.send({ message: "Seu cadastro de político não foi validado.", login: false })
            if(!user.emailvalidado)
                return res.send({ message: "Você ainda não validou o seu e-mail.", login: false })

            // login no repolitica antigo:
            if(user.userid && !user.salt){
                request.post(
                    'http://' + process.env.OLD_URL + '/account/logon',
                    { form: { LoginEmail: email, LoginPassword: password} },
                    function (error, response, body) {
                        if (!error && response.statusCode == 200 &&
                            body.indexOf('validation-summary-errors') < 0){

                            var pass = auth.encryptPassword(password);
                            user.hashed_password = pass.encryptedPass;
                            user.salt = pass.salt;
                            user.save(function(){
                                fLogin();
                            })
                        }
                        else {
                            return res.send({ message: "Email ou senha inválidos.", login: false })
                        }
                    }
                );

            }
            else fLogin();
        });

    },
    /*
    facebook: function(req,res){
        var link = process.env.PROTOCOL + '://' + req.headers.host + '/facebook/';

        passport.use(new FacebookStrategy({
                clientID: process.env.FACE_ID,
                clientSecret: process.env.FACE_SECRET,
                callbackURL: link
            },
            function(accessToken, refreshToken, profile, done) {
                Users.findOrCreate(..., function(err, user) {
                    if (err) { return done(err); }
                    done(null, user);
                });
            }
        ));

    }, */
    logout: function (req,res){
        req.logout();
        res.clearCookie("user");
        res.send(true);
    },
    isauthenticated: function(req,res){
        var userid = "";
        if(req.session != null && req.session.passport != null &&
            req.session.passport.user != null)
            userid = req.session.passport.user;

        if(userid){

            Users.native(function(err, collection) {
                if (err) return res.serverError(err);
                collection.findOne({ _id: new ObjectID(userid)},function (err, user) {
                    // por segurança
                    if(user != null){
                        delete user.hashed_password;
                        delete user.salt;
                        delete user.createdAt;
                        delete user.updatedAt;
                    }
                    res.json({err: err, user: user, userid: userid});
                })
            });
        }
        else
            res.json({err: "Usuário não autenticado"});
    },

    // registros
    create: function(req,res){

        if(!req.body.email){
            return res.send({msg:"Email vazio", created:false});
        }

        if(!req.body.nome){
            return res.send({msg:"Nome vazio", created:false});
        }

        Users.findOne({
            email: req.body.email
        }).
            exec(function (err, user) {
                if (err || user == null) {
                    req.body.isAdmin = false;
                    Users.create(req.body).exec(function (err, user) {
                        if(!err) {


                            async.auto({
                                validacaoEmail: function(cb){

                                    var link = process.env.PROTOCOL + '://' + req.headers.host + '/#!/cadastrocidadao/confirmacao/' + user.id + '/' + user.token;
                                    var textoHtml = "Olá.<br/><br/>Alguém acaba de tentar se registrar no Repolítica usando o seu endereço de email. Se o usuário for você mesmo, use o link a seguir para confirmar o seu endereço de email e começar a usar o Repolítica.<br/>" +
                                        "<a href='" + link + "'>" + link + "</a><br/><br/>" +
                                        "Se não foi você, não se preocupe - não lhe enviaremos mais nenhum email e excluiremos o seu endereço caso ignore esta mensagem." +
                                        "<br/><br/>Obrigado!<br/><br/><a href='www.repolitica.com.br'>www.repolitica.com.br</a>";
                                    var texto = "Olá.\n\nAlguém acaba de tentar se registrar no Repolítica usando o seu endereço de email. Se o usuário for você mesmo, use o link a seguir para confirmar o seu endereço de email e começar a usar o Repolítica.\n" +
                                        link + "\n\n" +
                                        "Se não foi você, não se preocupe - não lhe enviaremos mais nenhum email e excluiremos o seu endereço caso ignore esta mensagem." +
                                        "\n\nObrigado!\n\nwww.repolitica.com.br";

                                    aws.sendAWSEmails('Repolitica <contato@repolitica.com.br>', [user.email],
                                        [],[],"Confirmação da sua conta no Repolítica", texto, textoHtml, cb)
                                },
                                avisoEmail: function(cb){

                                    var textoAviso = "Nome do cidadão: " + req.body.nome + "<br/>" +
                                        "email: " + req.body.email + "<br/>" +
                                        "receber novidades: " + (req.body.recebernovidades ? "Sim" : "não");

                                    aws.sendAWSEmails('Repolitica <contato@repolitica.com.br>', [process.env.EMAIL_NOVOS_POL],
                                        [],[],"Cadastro de: " + req.body.nome, textoAviso, textoAviso, cb)
                                },
                            },function(err,r){
                                if(err) console.log(err);
                                res.send({msg:"Usuário cadastrado", created:true});
                            });

                        }
                        else res.send({msg:err, created:false});
                    })
                } else {
                    res.send({msg:"Usuário com o mesmo email já existe", created:false});
                }
            });
    },
    createPolitico: function(req,res){
        if(!req.body.email || !req.body.politicoid || !req.body.password || !req.body.telefone){
            return res.send({msg:"Faltam informações", created:false});
        }

        Users.findOne({ email: req.body.email }).exec(
            function (err, user) {
                if (err || user == null) {
                    req.body.isAdmin = false;
                    req.body.politicovalidado = false;
                    Users.create(req.body).exec(function (err, user) {
                        if(!err) {

                            async.auto({
                                validacaoEmail: function(cb){
                                    var link = process.env.PROTOCOL + '://' + req.headers.host + '/#!/cadastropolitico/confirmacao/' + user.id + '/' + user.token;
                                    var textoHtml = "Olá.<br/><br/>Alguém acaba de tentar se registrar no Repolítica usando o seu endereço de email. Se o usuário for você mesmo, use o link a seguir para confirmar o seu endereço de email e começar a usar o Repolítica.<br/>" +
                                        "<a href='" + link + "'>" + link + "</a><br/><br/>" +
                                        "Se não foi você, não se preocupe - não lhe enviaremos mais nenhum email e excluiremos o seu endereço caso ignore esta mensagem." +
                                        "<br/><br/>Obrigado!<br/><br/><a href='www.repolitica.com.br'>www.repolitica.com.br</a>";
                                    var texto = "Olá.\n\nAlguém acaba de tentar se registrar no Repolítica usando o seu endereço de email. Se o usuário for você mesmo, use o link a seguir para confirmar o seu endereço de email e começar a usar o Repolítica.\n" +
                                        link + "\n\n" +
                                        "Se não foi você, não se preocupe - não lhe enviaremos mais nenhum email e excluiremos o seu endereço caso ignore esta mensagem." +
                                        "\n\nObrigado!\n\nwww.repolitica.com.br";

                                    aws.sendAWSEmails('Repolitica <contato@repolitica.com.br>', [user.email],
                                        [],[],"Confirmação da sua conta no Repolítica", texto, textoHtml, cb)
                                },
                                avisoEmail: function(cb){

                                    var textoAviso = "Nome do político: " + req.body.nome + "<br/>" +
                                        "id do político: " + req.body.politicoid + "<br/>" +
                                        "email: " + req.body.email + "<br/>" +
                                        "telefone: " + req.body.telefone + "<br/>";

                                    aws.sendAWSEmails('Repolitica <contato@repolitica.com.br>', [process.env.EMAIL_NOVOS_POL],
                                        [],[],"Cadastro de: " + req.body.nome, textoAviso, textoAviso, cb)
                                },
                            },function(err,r){
                                if(err) console.log(err);
                                res.send({msg:"Usuário cadastrado", created:true});
                            });
                        }
                        else res.send({msg:err, created:false});
                    })
                } else {
                    res.send({msg:"Usuário com o mesmo email já existe", created:false});
                }
            });
    },

    // confirmações
    validarEmail: function(req,res){
        var userid = req.param('userid');
        var token = req.param('token');

        if(!userid || !token) return res.json({error: "Link inválido"});

        var objId;
        try{
            objId = new ObjectID(userid);
        }
        catch(err){
            return res.json({error: "Link de usuário inválido"});
        }

        var query = { _id: objId, token: token };
        console.log(query);

        Users.native(function(err, collection) {

            collection.findOne(query, function (err, user) {
                if(err){
                    console.log(err);
                    return res.json({error: "Um erro ocorreu"});
                }

                if(!user) return res.json({error: "Usuário inválido"});
                if(user.tokenDataExpirado < new Date()) return res.json({error: 'Token expirado'});

                user.tokenDataExpirado = new Date();
                user.emailvalidado = true;

                collection.update({ _id: objId }, user, function(err,userCount){
                    if (err) {
                        console.log(err);
                        return res.json({ error: 'Um erro ocorreu' });
                    }
                    // passport espera o id dessa forma:
                    user.id = user._id;

                    if(!user.politicoid) {
                        req.logIn(user, function(err){
                            return res.json(user);
                        });
                    }
                    else{
                        var objId;
                        try{
                            objId = new ObjectID(user.politicoid);

                            Politicos.native(function(err, collection) {
                                if (err) return res.serverError(err);
                                collection.findOne({ _id: objId},function(err,politico){
                                    if(err) console.log(err);

                                    if(politico){
                                        var pol = politico;
                                        user.politicoLink = repoliticaUtils.buildPoliticoUrl(pol);
                                        pol = null;
                                    }
                                    req.logIn(user, function(){
                                        return res.json(user);
                                    });
                                });
                            });
                        }
                        catch(ex){
                            user.politicoLink = "/";
                            req.logIn(user, function(){
                                return res.json(user);
                            });
                        }
                    }
                });
            });

        });

    },

    // senha
    emailNovaSenha: function (req,res){
        var email = req.param('email');
        if(!email && !emailUtils.validate(email)) return res.json({error: "Email inválido"});

        email = email.toLowerCase();
        Users.findOne({ email: email }, function (err, user) {
            if (err) return res.json({ error: 'Falha de comunicação' });
            if (!user) return res.json({ error: 'Usuário não encontrado' });
            if(!user.emailvalidado)
                return res.send({ message: "Você ainda não validou o seu e-mail.", login: false })

            user.token = generatePassword(64, false, /\w/).toLowerCase();
            user.tokenDataExpirado = moment(new Date()).add(globalConfig.VALIDADE_TOKEN, 'days')._d;

            user.save(function (err) {
                if (err) {
                    console.log(err);
                    return res.json({ error: 'Falha de comunicação' });
                }
                var link = process.env.PROTOCOL + '://' + req.headers.host + '/#!/login/reset/' + user.id + '/' + user.token;
                var textBody = 'Clique no link abaixo para escolher uma nova senha.\n' + link;
                aws.sendAWSEmails("Repolitica<" + process.env.REMETENTE_SUPORTE + ">", [ user.email ], [], [], "Redefinir senha", textBody, textBody.replace(/\n/g, '<br/>'), function (err, data) {
                    res.json({msg: "Email enviado"});
                });
            });
        });

    },
    tokenValido: function (req,res){
        var userid = req.param('userid');
        var token = req.param('token');

        if(!userid || !token) return res.json({error: "Usuário inválido"});

        var objId;
        try{
            objId = new ObjectID(userid);
        }
        catch(err){
            return res.json({error: "Usuário inválido"});
        }

        Users.native(function(err, collection) {
            if (err) return res.serverError(err);
            collection.findOne({ _id: objId, token: token },function(err,user){

                if(err){
                    console.log(err);
                    return res.json({error: "Um erro ocorreu"});
                }
                if(!user) return res.json({error: "Usuário inválido"});
                if(user.tokenDataExpirado < new Date()) return res.json({error: 'Token expirado'});
                res.json();
            });
        });

    },
    resetarSenha: function (req,res){
        var userid = req.param('userid');
        var token = req.param('token');
        var password = req.param('password');

        if(!userid && !token) return res.json({error: "Usuário inválido"});
        if(!password) return res.json({error: "Senha inválida"});
        var objId;
        try{
            objId = new ObjectID(userid);
        }
        catch(err){
            return res.json({error: "Usuário inválido"});
        }

        Users.native(function(err, collection) {
            if (err) return res.serverError(err);
            collection.findOne({ _id: objId, token: token },function(err,user){

                if(err){
                    console.log(err);
                    return res.json({error: "Um erro ocorreu"});
                }
                if(!user) return res.json({error: "Usuário inválido"});
                if(user.tokenDataExpirado < new Date()) return res.json({error: 'Token expirado'});

                var pass = auth.encryptPassword(password);
                user.hashed_password = pass.encryptedPass;
                user.salt = pass.salt;

                // preparando para uma eventual validacao de email
                user.tokenDataExpirado = new Date();


                collection.update({_id: new ObjectID(objId)}, user,
                    function(err,itens){
                        if (err) {
                            console.log(err);
                            return res.json({ error: 'Um erro ocorreu' });
                        }
                        res.json();

                    });

            });
        });
    },

};

/**
 * Sails controllers expose some logic automatically via blueprints.
 *
 * Blueprints are enabled for all controllers by default, and they can be turned on or off
 * app-wide in `config/controllers.js`. The settings below are overrides provided specifically
 * for AuthController.
 *
 * NOTE:
 * REST and CRUD shortcut blueprints are only enabled if a matching model file
 * (`models/Auth.js`) exists.
 *
 * NOTE:
 * You may also override the logic and leave the routes intact by creating your own
 * custom middleware for AuthController's `find`, `create`, `update`, and/or
 * `destroy` actions.
 */

module.exports.blueprints = {

    // Expose a route for every method,
    // e.g.
    // `/auth/foo` =&gt; `foo: function (req, res) {}`
    actions: true,

    // Expose a RESTful API, e.g.
    // `post /auth` =&gt; `create: function (req, res) {}`
    rest: true,

    // Expose simple CRUD shortcuts, e.g.
    // `/auth/create` =&gt; `create: function (req, res) {}`
    // (useful for prototyping)
    shortcuts: true

};