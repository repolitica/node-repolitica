/**
 * PoliticosController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */
var ObjectID = require('../../node_modules/sails-mongo/node_modules/mongodb').ObjectID;
var db = require('../../node_modules/sails-mongo/node_modules/mongodb').Db;
var async = require('async');
var removeDiacritics = require('diacritics').remove;
var cidadesJson = require('../../assets/staticdb/cidades.json');
var repoliticaUtils = require('../utils/repolitica-utils');
var repoliticaDbs = require('../utils/repolitica-dbs');
var aws = require('../utils/aws');
var generatePassword = require('password-generator');
var moment = require('moment');
var request = require('request');

var cidadeUrl = function(estado,cidade){
    if(estado) {
        if(cidade && cidade != -1 && cidadesJson[estado]) {
            var cid = cidadesJson[estado][cidade].nome.toLowerCase();
            cid = removeDiacritics(cid);
            cid = cid.replace(new RegExp(" ", 'g'), '-');
            return cid;
        }
    }
}

var isAuthenticated = function(req, cb){
    var userid = req.session.passport.user || "";

    if(userid){

        Users.native(function(err, collection) {
            if (err) return cb({err:err});
            collection.findOne({ _id: new ObjectID(userid)},function(err,user){
                // por segurança
                if(user != null){
                    delete user.hashed_password;
                    delete user.salt;
                    delete user.createdAt;
                    delete user.updatedAt;
                }
                cb({err: err, user: user});
            });
        });
    }
    else cb({err: "Usuário não autenticado"});
}

var verPoliticoIndex = false;
var verPontos = false;
// verificações iniciais do site
// no v0 pode demorar um pouco
var initDb = function(){

    //repoliticaDbs.limparbanco();


    if(!verPontos){
        verPontos = true;
        async.auto({
            configs: function(cb){
                Configuracoes.find().exec(cb);
            }
        },function(err,r){
            if(err) console.log(err);
            else {
                var rodarPontos = true;

                if(r.configs && r.configs.length > 0)
                    for(var i = 0; i < r.configs.length; i++){
                        var conf = r.configs[0];
                        switch(conf.chave){
                            case "rodarPontos":
                                rodarPontos = conf.valor == "true";
                                break;
                        }
                    }

                if(rodarPontos){
                    var ultTextoBusca = "";
                    var buscaPols = function(){
                        var query = {};
                        if(ultTextoBusca) query.textobusca = { $gt: ultTextoBusca };
                        Politicos.native(function(err,collection){
                            collection.find(query,{limit: 100}).sort({textobusca: 1}).toArray(function(err,pols){
                                if(pols.length != 0) {
                                    async.eachSeries(pols,function(pol,cb){
                                        pol.pontos = repoliticaUtils.pontos(pol);
                                        collection.update({ _id: new ObjectID(pol._id) }, pol, function(err, p) {
                                            if (err || !p) console.log("erro no pol id: " + pol.id);
                                            cb();
                                        });
                                    },function(){
                                        if(pols.length > 0)
                                        {
                                            ultTextoBusca = pols[pols.length-1].textobusca;
                                            buscaPols();
                                        }
                                    })
                                }
                            });
                        });
                    };

                    Configuracoes.findOne({chave:"rodarPontos"}).exec(function(err,config){
                        if(err) return console.log(err);
                        if(config == null){
                            Configuracoes.create({chave:"rodarPontos", valor:false}).exec(buscaPols);
                        }
                        else {
                            config.valor = "false";
                            Configuracoes.update({chave:"rodarPontos"},config).exec(buscaPols);
                        }
                    });
                }
            }
        });

    }

    // verificar índices
    if(!verPoliticoIndex) return;
    politicoIndex = true;
    async.auto({
        politicos: function(cb){
            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);

                cb('',{collection:collection});
            });

        },
        textobusca: ['politicos', function(cb,r){
            r.politicos.collection.ensureIndex({"textobusca": 1}, {background: true}, cb);
        }],
        urlpolitico: ['politicos', function(cb,r){
            r.politicos.collection.ensureIndex({"urlpolitico": 1}, {background: true}, cb);
        }],
        cidade: ['politicos', function(cb,r){
            r.politicos.collection.ensureIndex({"pontos": -1}, {background: true}, cb);
        }],
        anocandidatura: ['politicos', function(cb,r){
            r.politicos.collection.ensureIndex({"anocandidatura": 1}, {background: true}, cb);
        }],
        cidadecandidatura: ['politicos', function(cb,r){
            r.politicos.collection.ensureIndex({"cidadecandidatura": 1}, {background: true}, cb);
        }],

        mudancas: function(cb){
            Mudancas.native(function(err, collection) {
                if (err) return res.serverError(err);

                cb('',{collection:collection});
            });

        },
        polid: ['mudancas', function(cb,r){
            r.mudancas.collection.ensureIndex({"politicoid": 1}, {background: true}, cb);
        }]
    },function(err,r){
        if (err) console.log(err);
    });

}

module.exports = {

    initdb:function(req,res){
        //repoliticaDbs.polsimilares();
        //repoliticaDbs.redesim();
        //repoliticaDbs.medrede();
        res.send("Olá!");
    },

    // perfil do político
    // otimizado com índices
    load: function(req, res, next){
        var id = req.param("widgetid");
        if(id){
            var p = {id:id, widget: true };
            var email = req.param("email");
            var token = req.param("token");
            var gabinete = req.param("gabinete");

            if(gabinete) p.gabinete = gabinete;

            Users.findOne({email:email, authToken:token}).exec(function(err,user){
                if(!user || err){
                    console.log("acesso do AP: " + (err || "usuário nao logado"));
                    console.log("email: " + email);
                    console.log("token: " + token);
                    console.log("gabinete: " + gabinete);
                }

                //user.authToken = generatePassword(64, false, /\w/).toLowerCase();
                //user.tokenDataExpirado = moment(new Date()).add('years', 1)._d;
                //user.save();

                req.logIn(user, function(err) {
                    if (err) {
                        console.log(err);
                    }
                    if(user)
                        //http://speakingaword.blogspot.com.br/2013/08/authentication-in-angularjs-nodejs-and.html
                        res.cookie('user', JSON.stringify({'id': user.id}), { httpOnly: false } );
                    user = null;
                    return res.view({politico: JSON.stringify(p)});
                });
            });
        }
        else{
            var estado = req.param('estado');
            var cidade = req.param('cidade');
            var politicoid = req.param('politicoid');

            if(politicoid == "favicon.ico") return next();

            var politico = { cidade:cidade, estado:estado, politicoid:politicoid };

            if(estado != null){
                var verPasta = estado.toLowerCase();

                if(verPasta.indexOf("admin") > 0) return res.send("no admin for you!");

                // regra para exclusão de rota -> o controlador atual não deverá tratar chamadas a estas pastas/controladores
                // pasta do assets
                if (verPasta == "img" || verPasta == "css" || verPasta == "fonts" || verPasta == "js"
                    || verPasta == "lib"  || verPasta == "partials"  || verPasta == "staticdb"
                    || verPasta == "sitemap" || verPasta == "robots.txt") {
                    return next();
                }
                // controllers do sails
                if (verPasta == "auth" || verPasta == "files" || verPasta == "politicos" || verPasta == "users"
                    || verPasta == "questoes" || verPasta == "content" || verPasta == "alo" || verPasta == "votos") {
                    return next();
                }
                // caso contrário, link de url antiga
                if(verPasta == "contato" && req.param('email')){
                    return next();
                }
            }
            if(politicoid == "robots.txt") return next();
            console.log(politicoid);
            if(politicoid.indexOf("google") == 0 && politicoid.indexOf(".html") > 0) return next();
            if(politicoid.indexOf("admin") > 0) return res.send("no admin for you!");
            if(politicoid.indexOf(".php") > 0) return res.send("no php for you!");
            if(politicoid.indexOf(".aspx") > 0) return res.send("no .net for you!");
            if(politicoid.indexOf(".xml") > 0) return next();
            if(req.headers['user-agent'] == null) return res.send("");

            var botAgent = req.headers['user-agent'].indexOf("http://www.google.com/bot.html") > 0 ||
                req.headers['user-agent'].indexOf("www.facebook.com") > 0;

            var sendHtmlBot = function(pol){
                var title = "Repolitica - " + pol.nomecandidatura;
                var description = "Tudo sobre " + pol.nomecandidatura;
                var fullUrl = 'http://' + req.get('host') + req.originalUrl;

                var ogTitle = pol.nomecandidatura;
                var ogDesc = "";
                if(!ogDesc){
                    var ano = new Date().getFullYear();
                    // se for candidato:
                    if(((pol.anocandidatura && pol.anocandidatura == ano) ||
                        !pol.anocandidatura) && pol.cargocandidatura){
                        ogTitle += " - Candidato(a) a " + pol.cargocandidatura;
                        if(pol.partidocandidatura) ogTitle += " pelo " + pol.partidocandidatura;
                        if(pol.estadocandidatura && pol.cargocandidatura != "Presidente"){
                            ogTitle += " - ";
                            if((pol.cidadecandidatura && pol.cidadecandidatura != -1) &&
                                (pol.cargocandidatura == "Vereador " || pol.cargocandidatura == "Prefeito" ||
                                    pol.cargocandidatura == "Vice-prefeito")){
                                if(cidadesJson[pol.estado][pol.cidade] &&
                                    cidadesJson[pol.estado][pol.cidade].nome)
                                    ogTitle += cidadesJson[pol.estado][pol.cidade].nome + ", ";
                            }
                            ogTitle += pol.estadocandidatura;
                        }
                    }
                    // se não for candidato
                    else if(pol.cargo){
                        ogTitle += " - " + pol.cargo;
                        if(pol.partido)
                            ogTitle += " pelo " + pol.partido;
                        if(pol.estado && pol.cargo != "Presidente"){
                            ogTitle += " - ";
                            if((pol.cidade && pol.cidade != -1) &&
                                (pol.cargo == "Vereador " || pol.cargo == "Prefeito" || pol.cargo == "Vice-prefeito")){
                                if(cidadesJson[pol.estado][pol.cidade] &&
                                    cidadesJson[pol.estado][pol.cidade].nome)
                                    ogTitle += cidadesJson[pol.estado][pol.cidade].nome + ", ";
                            }
                            ogTitle += pol.estado
                        }
                    }
                    else if(pol.anocandidatura && !pol.anocandidatura && pol.cargocandidatura){
                        ogTitle = " - Candidato(a) a " + pol.cargocandidatura + " em " + pol.anocandidatura;
                        if(pol.numerocandidatura) ogDesc += ", número " + pol.numerocandidatura;
                        if(pol.partidocandidatura) ogDesc += ", " + pol.partidocandidatura;
                    }
                    if(pol.biografia)
                        ogDesc = pol.biografia;
                    else
                        ogDesc = "Conheça as posições ideológicas, opiniões e história de " + pol.nomecandidatura + ".";
                }

                var ogimg = pol.imagemurl || "http://www.repolitica.com.br/img/fb.jpg";

                if(ogimg.indexOf("Content/themes") > 0
                    && ogimg.indexOf(process.env.OLD_URL) == -1)
                    ogimg = "http://" + process.env.OLD_URL + ogimg;

                var html = "<html><head>" +

                    "<meta property=\"og:title\" content=\"" + ogTitle + "\"/>" +
                    "<meta property=\"og:description\" content=\"" + ogDesc + "\"/>" +
                    "<meta property=\"og:image\" content=\"" + ogimg + "\"/>" +

                    "<title>" + title + "</title>" +
                    //http://googlewebmastercentral.blogspot.com.br/2009/02/specify-your-canonical.html
                    "<link rel=\"canonical\" href=\"" + fullUrl + "\" />" +
                    "<meta name=\"description\" content=\"" + description + "\" />" +
                    "</head></html>";
                pol = null;
                title = null;
                description = null;
                return res.send(html);
            }
            var sendHtmlPageBot = function(title,description){
                var fullUrl = 'http://' + req.get('host') + req.originalUrl;

                var html = "<html><head>" +

                    "<meta property=\"og:title\" content=\"" + title + "\"/>" +
                    "<meta property=\"og:description\" content=\"" + description + "\"/>" +

                    "<title>" + title + "</title>" +
                    //http://googlewebmastercentral.blogspot.com.br/2009/02/specify-your-canonical.html
                    "<link rel=\"canonical\" href=\"" + fullUrl + "\" />" +
                    "<meta name=\"description\" content=\"" + description + "\" />" +
                    "</head></html>";
                title = null;
                description = null;
                return res.send(html);
            }
            var returnNotFound = function(){
                if(!botAgent)
                    return res.redirect('/#!/sem-politico');
                else return res.status(404);
            }

            if((estado == "cadastro" && cidade == "politico") ||
                estado == "Profile" || estado == "fotos")
                return returnNotFound();
            if(estado == "#!")
                return res.redirect('/');

            // lista de candidatos
            // /candidatos/MS/nioaque
            if(estado == "candidatos" && cidade && politicoid){
                if(botAgent){
                    return sendHtmlPageBot("Repolítica - " + cidade, "Candidatos de " + cidade + " - " + politicoid);
                }
                else return res.redirect('/#!/lista/' + cidade + "/" + politicoid);
            }

            if(!botAgent){
                switch(politicoid){
                    case "votos": return next(); break;
                    case "teste": return res.redirect('/#!/teste'); break;
                    case "creditos":
                    case "sobre":
                    case "funcionamento": return res.redirect('/#!/quem-somos'); break;
                    case "midia": return res.redirect('/#!/na-midia'); break;
                    case "candidatos":
                    case "rankings":
                    case "busca": return res.redirect('/#!/lista'); break;
                    case "colabore":
                    case "contato": return res.redirect('/#!/contato'); break;
                    case "termos": return res.redirect('/#!/termos-uso'); break;
                    case "privacidade": return res.redirect('/#!/politicas-privacidade'); break;
                    case "esqueceu_senha": return res.redirect('/#!/login/reset'); break;
                    case "#!": return res.redirect('/'); break;
                }
            }
            else{
                switch(politicoid){
                    case "esqueceu_senha": return sendHtmlPageBot("Repolítica - recupere a senha", "Recupere a senha do seu login"); break;
                    case "teste": return sendHtmlPageBot("Repolítica - Teste", "Encontre políticos que pensam como você"); break;
                    case "como-usar": return sendHtmlPageBot("Repolítica - Como usar o teste", "Saiba como usar melhor o teste do Repolítica"); break;
                    case "creditos":
                    case "funcionamento":
                    case "sobre": return sendHtmlPageBot("Sobre o Repolítica", "Saiba mais sobre o Repolítica"); break;
                    case "na-midia":
                    case "midia": return sendHtmlPageBot("Repolítica - Na mídia", "Veja onde o Repolítica apareceu na mídia"); break;
                    case "candidatos":
                    case "lista":
                    case "rankings":
                    case "busca": return sendHtmlPageBot("Repolítica - Busca", "Busque políticos do Brasil inteiro"); break;
                    case "colabore":
                    case "contato": return sendHtmlPageBot("Repolítica - Contato", "Entre em contato com a equipe do Repolítica"); break;
                    case "termos-uso":
                    case "termos": return sendHtmlPageBot("Repolítica - Termos de uso", "Termos de uso do site Repolítica"); break;
                    case "politicas-privacidade":
                    case "privacidade": return sendHtmlPageBot("Repolítica - Politicas de Privacidade", "Políticas de privacidade do Repolítica"); break;
                }
            }

            initDb();
            politicoid = politicoid.replace("%23!", "");

            var returnPol = function(){
                if(!estado && !cidade){
                    Redirecionadores.findOne({urlpolitico : politicoid}).exec(function(err, red){
                        if(err) console.log(err);
                        if(!red) {
                            if(!botAgent)
                                return res.view({ politico: JSON.stringify(politico) });

                            // google bot
                            Politicos.native(function(err, collection) {
                                if (err) return res.serverError(err);
                                collection.findOne({ politicourl: politicoid},function(err,pol){
                                    if(pol == null || pol.nomecandidatura == null)
                                        return sendHtmlPageBot("Repolítica - " + politicoid, "A página " + politicoid + " não existe mais.");
                                    sendHtmlBot(pol);
                                });
                            });

                        }
                        else
                            // TODO: trecho de código redundante
                            Politicos.native(function(err, collection) {
                                if (err) return res.serverError(err);
                                collection.findOne({ _id: new ObjectID(red.idpolitico)},function(err,pol){
                                    if(err) console.log(err);

                                    if(pol && botAgent){
                                        sendHtmlBot(pol);
                                    }
                                    else{
                                        if(pol){
                                            politico.estado = pol.estadocandidatura;
                                            politico.cidade = cidadeUrl(pol.estadocandidatura, pol.cidadecandidatura);
                                            politico.politicoid = pol.politicourl;
                                            politico.id = pol._id;
                                        }

                                        res.view({ politico: JSON.stringify(politico) });
                                        pol = null;
                                    }
                                });
                            });

                    });
                }
                else {
                    if(!botAgent)
                        return res.view({ politico: JSON.stringify(politico) });

                    // para google bot

                    var query = { politicourl: politicoid };
                    var and = []

                    if(estado) query.estadocandidatura = estado.toUpperCase();
                    else and.push({$or:[
                        {estadocandidatura: ""},
                        {estadocandidatura: {$exists: false}}
                    ]});

                    if(cidade != null && estado){
                        var cTemp = cidade;
                        cidade = removeDiacritics(cTemp);
                        cTemp = null;
                        var find = '-';
                        var re = new RegExp(find, 'g');
                        cidade = cidade.replace(re, ' ').toLowerCase();

                        for(var id in cidadesJson[query.estado]){
                            if(cidade == removeDiacritics(cidadesJson[query.estado][id].nome.toLowerCase())){
                                query.cidadecandidatura = parseInt(id);
                                break;
                            }
                        }
                    }
                    else and.push({$or:[
                        {cidadecandidatura: -1},
                        {cidadecandidatura: {$exists: false}}
                    ]});

                    if(and.length > 0) query.$and = and;

                    Politicos.native(function(err, collection) {
                        if (err) return res.serverError(err);
                        collection.findOne(query,function(err,pol){
                            if(pol == null || pol.nomecandidatura == null) return res.status(404);
                            sendHtmlBot(pol);
                        });
                    });

                }
            }

            var antigo = false;
            switch(politicoid){
                case "opiniao":
                case "comentarios":
                case "completo":
                case "perfil":
                case "evolucao":
                case "propostas":
                case "mural":
                    antigo = true;
                    break;
            }
            if(estado == "contato" && botAgent){
                var aux = estado;
                estado = politicoid;
                politicoid = estado;
                antigo = true;
            }

            // compatibilidade reversa
            // no param estado vem a url do politico do site antigo
            if(estado && botAgent && antigo){
                Redirecionadores.findOne({urlpolitico : estado}).exec(function(err, red){
                    if(err) console.log(err);
                    if(!red) {
                        sendHtmlPageBot("Repolítica - " + estado, "A página " + estado + " não existe mais.");
                    }
                    else
                    // TODO: trecho de código redundante
                        Politicos.native(function(err, collection) {
                            if (err) return res.serverError(err);
                            collection.findOne({ _id: new ObjectID(red.idpolitico)},function(err,pol){
                                if(err) console.log(err);

                                if(pol)
                                    sendHtmlBot(pol);
                                else
                                    sendHtmlPageBot("Repolítica - " + estado, "A página " + estado + " não existe mais.");
                            });
                        });

                });
            }
            else {
                if(estado && antigo){
                    politicoid = estado;
                    estado = null;
                }
                returnPol();
            }
        }
    },

    find: function(req,res){

        initDb();

        var buscaMais = req.param('buscaMais');
        var anoAtual = new Date().getFullYear();

        var filtro = req.param('filtro');
        var query = {};

        if(filtro){
            var and = [];

            if(filtro.questaoid) {
                var obj = {};
                if(!filtro.resposta)
                    obj["questoes." + filtro.questaoid] = { $exists: true };
                else obj["questoes." + filtro.questaoid + ".resposta"] = filtro.resposta;
                if(!buscaMais.semjustificativa)
                    obj["questoes." + filtro.questaoid + ".justificativa"] = /./;
                else
                    obj["questoes." + filtro.questaoid + ".justificativa"] = "";
                and.push(obj);
            }

            if(filtro.textobusca) {
                var b = removeDiacritics(filtro.textobusca.toLowerCase());
                var regB;
                try{
                    regB = new RegExp("^" + b);
                }
                catch(ex){
                    regB = b;
                }
                and.push({textobusca: regB});
            }

            if(!!filtro.cargos && filtro.cargos.length > 0){
                for(var i = 0; i < filtro.cargos.length; i++){
                    if(filtro.cargos[i] == "Suplente"){
                        filtro.cargos.push("2º Suplente");
                        filtro.cargos.push("1º Suplente");
                        break;
                    }
                }
            }

            if(!!filtro.cargos && filtro.cargos.length > 0){

                var cargoOr = [];

                for(var i = 0; i < filtro.cargos.length; i++){
                    var cargo = filtro.cargos[i];

                    if(filtro.eleitos) cargoOr.push({cargo:cargo});
                    else if(filtro.recemeleitos) cargoOr.push({cargocandidatura:cargo});
                    else if(filtro.candidatosatuais) {
                        cargoOr.push({$or:[
                            { $and: [{ anocandidatura: anoAtual }, { cargocandidatura: cargo } ]},
                            { $and: [{ anocandidatura: null }, { cargocandidatura: cargo } ]}
                        ]});
                    }
                    else{
                        cargoOr.push({$or:[
                            { cargo: cargo },
                            { cargocandidatura: cargo }
                        ]});
                    }
                }
                and.push({$or:cargoOr});
                cargoOr = null;

            }
            else{

                if(filtro.eleitos) {

                    var or = [];
                    or.push({cargo: "Vereador"});
                    or.push({cargo: "Deputado Estadual"});
                    or.push({cargo: "Deputado Federal"});
                    or.push({cargo: "Deputado Distrital"});
                    or.push({cargo: "Prefeito"});
                    or.push({cargo: "Vice-governador"});
                    or.push({cargo: "Governador"});
                    or.push({cargo: "Senador"});
                    or.push({cargo: "Suplente"});
                    or.push({cargo: "1º Suplente"});
                    or.push({cargo: "2º Suplente"});
                    or.push({cargo: "Vice-presidente"});
                    or.push({cargo: "Presidente"});
                    and.push({ $or: or });
                    or = null;
                }
                else if(filtro.candidatosatuais) {
                    and.push({$or:[
                        { anocandidatura: anoAtual },
                        { $and: [{ anocandidatura: null }, { cargocandidatura: { $exists: true} } ]}
                    ]});
                }

            }

            if(filtro.cidade)
                if(!filtro.eleitos && !filtro.candidatosatuais){
                    var eOr = [];
                    eOr.push({cidade: filtro.cidade});
                    eOr.push({cidadecandidatura: filtro.cidade});
                    and.push({$or:eOr});
                    eOr = null;
                }
                else if(filtro.eleitos) and.push({cidade: filtro.cidade});
                else if(filtro.candidatosatuais) query.cidadecandidatura = filtro.cidade;

            if(filtro.estado)
                if(!filtro.eleitos && !filtro.candidatosatuais){
                    var eOr = [];
                    eOr.push({estado: filtro.estado});
                    eOr.push({estadocandidatura: filtro.estado});
                    and.push({$or:eOr});
                    eOr = null;
                }
                else if(filtro.eleitos) and.push({estado: filtro.estado});
                else if(filtro.candidatosatuais) query.estadocandidatura = filtro.estado;


            if(filtro.partido) {
                and.push({$or:[
                    { partido: filtro.partido },
                    { partidocandidatura: filtro.partido },
                ]});
            }

            if(filtro.cadastrados){
                and.push({gabinete: {$exists: true}});
            }

            if(filtro.recemeleitos){
                query.anocandidatura = 2014;
                query.eleito = true;
            }

            if(and.length > 0) query.$and = and;
            and = null;
        }

        var repetir = true;
        var lastDocs = [];
        var polBusca = function(){
            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);
                if(!buscaMais) buscaMais = {};
                if(buscaMais.pontos != null && buscaMais.pontos > 0) query.pontos = { $lte: buscaMais.pontos };
                if(buscaMais.pontos != null && buscaMais.pontos == 0) query.pontos = { $exists:false };
                if(buscaMais.textobusca) query.textobusca = { $gt: buscaMais.textobusca };

                collection.find(query, { limit:buscaMais.limit }).sort({pontos: -1}).toArray(function(err,docs){

                    var ultimo = {limit: buscaMais.limit };
                    if(buscaMais.semjustificativa) ultimo.semjustificativa = true;
                    if(!docs) docs = [];
                    docs = docs.concat(lastDocs);
                    if(docs.length > 0){

                        var ultPol = docs[docs.length - 1];
                        var count = 0;
                        if(!ultPol.pontos){
                            ultimo.textobusca = ultPol.textobusca;
                            ultimo.pontos = 0;

                            for(var i = docs.length - 1; i >= 0; i--){
                                if(docs[i].imagemurl && docs[i].imagemurl.indexOf("Content/themes") > 0
                                    && docs[i].imagemurl.indexOf(process.env.OLD_URL) == -1)
                                    docs[i].imagemurl = "http://" + process.env.OLD_URL + docs[i].imagemurl;
                            }
                        }
                        else{
                            ultimo.pontos = ultPol.pontos;
                            // busca quantos itens tem a mesma pontuação
                            for(var i = docs.length - 1; i >= 0; i--){
                                var doc = docs[i];
                                if(doc.pontos == ultimo.pontos) count++;
                                if(doc.imagemurl && doc.imagemurl.indexOf("Content/themes") > 0
                                    && doc.imagemurl.indexOf(process.env.OLD_URL) == -1)
                                    doc.imagemurl = "http://" + process.env.OLD_URL + doc.imagemurl;
                            }
                            count--;
                        }

                        if(count == 0) ultimo.limit = 20;
                        else ultimo.limit += count;
                    }

                    if(docs.length < buscaMais.limit && buscaMais.pontos > 0
                        && repetir){
                        repetir = false;
                        buscaMais.limit = 20;
                        buscaMais.pontos = 0;
                        lastDocs = docs;
                        polBusca();
                    }
                    else if(repetir && docs.length == 0){
                        repetir = false;
                        if(!query.$and || query.$and.length == 0)
                            res.json({docs:docs, buscaMais: ultimo});
                        else{
                            var busca = "";
                            var i = 0;
                            for(; i < query.$and.length; i++){
                                if(query.$and[i].textobusca){
                                    var busca = query.$and[i].textobusca.toString();
                                    if(busca.indexOf("/^") == 0){
                                        busca = busca.substring(2);
                                        busca = busca.substring(0, busca.length-1);
                                    }

                                    if(query.$and[i].textobusca == busca) busca = "";

                                    query.$and.splice(i,1);
                                    break;
                                }
                            }

                            if(!busca){
                                res.json({docs:docs, buscaMais: ultimo});
                                lastDocs = null;
                            }
                            else{
                                var listBusca = busca.split(" ");

                                for(var j = 0; j < listBusca.length; j++){
                                    var regB;
                                    try{
                                        regB = new RegExp(listBusca[j]);
                                    }
                                    catch(ex){
                                        regB = listBusca[j];
                                    }
                                    query.$and.push({textobusca: regB});
                                }

                                buscaMais.limit = 5;
                                polBusca();
                            }
                        }

                    }
                    else{
                        res.json({docs:docs, buscaMais: ultimo});
                        lastDocs = null;
                    }
                });
            });
        }
        polBusca();
    },

    update: function(req, res){
        var politico = req.param('politico');
        if(!politico) return res.json({ err: "politico inválido" });

        //########*******-------- validações ......... _____
        if(politico.biografia && politico.biografia.length > 1000)
            return res.json({ err: "Biografia com mais de 1000 caracteres" });
        if(politico.propostas && politico.propostas.length > 0){
            for(var i = 0; i < politico.propostas.length; i++){
                var prop = politico.propostas[i];
                if(!prop.titulo)
                    return res.json({ err: "Proposta sem título" });
                if(prop.titulo.length > 50)
                    return res.json({ err: "Título com mais de 50 caracteres" });
                if(prop.texto && prop.texto.length > 300)
                    return res.json({ err: "Texto com mais de 300 caracteres" });
            }
        }
        if(politico.opinioes){
            for(var key in politico.opinioes){
                var op = politico.opinioes[key];
                if(op.justificativa && op.justificativa.length > 300)
                    return res.json({ err: "Justificativa com mais de 300 caracteres" });
            }
        }
        if(!politico.nomecandidatura){
            return res.json({ err: "O político precisa ter um nome" });
        }
        delete politico.editores;

        async.auto({
            autenticacao: function(cb){
                isAuthenticated(req,function(obj){
                    if(obj.err) return res.json({ err: "usuário inválido" });
                    cb('', {id:obj.user._id , user:obj.user});
                });
            },
            blacklist: ['autenticacao', function(cb,r){
                if(!r.autenticacao) cb();
                else{
                    BlackList.find({email: r.autenticacao.user.email}).exec(function(err,emails){
                        if(!emails || emails.length == 0) cb();
                        else cb("Sem permissão para edição");
                    });
                }
            }],
            vices: function(cb){
                if(!politico.viceid && !politico.suplente1id && !politico.suplente2id)
                    cb();
                else if(politico.viceid){
                    Politicos.find({
                        cargocandidatura:politico.cargocandidatura,
                        viceid: politico.viceid
                    }).exec(function(err,pols){
                            if(err) console.log(err);
                            if(pols.length == 0) cb();
                            else if(pols.length == 1 && pols[0].id == politico._id) cb();
                            else cb("Vice já registrado para " + pols[0].nomecandidatura + ".");
                        });
                }
                else if(politico.suplente1id){
                    Politicos.find({
                        cargocandidatura:politico.cargocandidatura,
                        suplente1id: politico.suplente1id
                    }).exec(function(err,pols){
                            if(err) console.log(err);
                            if(pols.length == 0) cb();
                            else if(pols.length == 1 && pols[0].id == politico._id) cb();
                            else cb("Primeiro suplente já registrado para outro Senador.");
                        });
                }
                else if(politico.suplente2id){
                    Politicos.find({
                        cargocandidatura:politico.cargocandidatura,
                        suplente2id: politico.suplente2id
                    }).exec(function(err,pols){
                            if(err) console.log(err);
                            if(pols.length == 0) cb();
                            else if(pols.length == 1 && pols[0].id == politico._id) cb();
                            else cb("Segundo suplente já registrado para outro Senador.");
                        });
                }
            }
        },function(err,r){
            if(err) return res.json({error:err});
            upPol(r);
        });

        var upPol = function(r){
            var userid = r.autenticacao.id;
            var user = r.autenticacao.user;

            delete politico.vice;
            delete politico.suplente1;
            delete politico.suplente2;
            delete politico.id;

            politico.textobusca = politico.nomecandidatura + " " + (politico.nomecompleto || "");
            politico.textobusca = removeDiacritics(politico.textobusca.toLowerCase());

            // criação de entradas no banco na collection mudancas
            var id = politico._id;
            var polUpdate = function(){
                Politicos.native(function(err, collection) {
                    if (err) return res.serverError(err);
                    collection.findOne({ _id: new ObjectID(id)},function(err,politicoAntigo){
                        var modificacoes = [];

                        var adicionarMod = function(nomeProp, novovalor,idarray){
                            modificacoes.push({
                                politicoid: politicoAntigo._id,
                                usuarioid: userid,
                                datacriacao: new Date(),
                                nomepropriedade: nomeProp,
                                novovalor: JSON.stringify(novovalor),
                                idarray: idarray,
                                status:1
                            });
                        }

                        // modificações em relação ao antigo
                        for(var key in politicoAntigo){
                            if(key == "createdAt" || key == "updatedAt" || key == "constructor" ||
                                key == "datacriacao" ||
                                key == "textobusca" || key == "id" || key == "_id" ||
                                key == "acessos" || key == "editores") continue;

                            // TODO: modificação por array

                            if(politico[key] != null &&
                                JSON.stringify(politicoAntigo[key]) != JSON.stringify(politico[key])){
                                adicionarMod(key,politico[key]);
                            }
                        }

                        // novos atributos em relação ao antigo
                        for(var key in politico){
                            if(key == "createdAt" || key == "updatedAt" || key == "constructor" ||
                                key == "datacriacao" ||
                                key == "textobusca" || key == "id" || key == "_id" ||
                                key == "acessos" || key == "editores") continue;
                            if(politico[key] == -1) continue;
                            if(!politico[key]) continue;
                            if(politico[key] != null && politicoAntigo[key] == null){
                                adicionarMod(key,politico[key]);
                            }
                        }

                        async.eachSeries(modificacoes, function (doc, cb) {
                            Mudancas.create(doc, function(err,mud){
                                if(err) console.log(err);
                                cb();
                            });
                        });

                        politico.pontos = repoliticaUtils.pontos(politico);
                        delete politico._id;

                        collection.update({ _id: new ObjectID(id) }, politico, function(err, p) {

                            if (err) {
                                res.json({error:err});
                            }
                            else {

                                if(modificacoes.length == 0) return res.json(p);

                                var textoAviso = "Update de: " + politico.nomecandidatura + "<br/>"
                                    + "Usuário: ";
                                if(user.nome)
                                    textoAviso += user.nome + " - ";
                                textoAviso += user.email + "<br/><br/>";

                                for(var i = 0; i < modificacoes.length; i++){
                                    textoAviso += modificacoes[i].nomepropriedade + ": " + modificacoes[i].novovalor + "<br/>";
                                }

                                aws.sendAWSEmails('Repolitica <contato@repolitica.com.br>', [process.env.EMAIL_NOVOS_POL],
                                    [],[],"Update de: " + politico.nomecandidatura, textoAviso, textoAviso, function(){

                                    })
                                res.json(p);
                            }
                        });

                    });
                });

            }

            if(politico.questoes)
                for(var i in politico.questoes){
                    delete politico.questoes[i].questao;
                }

            if(politico.propostas){
                var inserts = [];
                var updates = [];
                var ids = [];

                for(var i = 0; i < politico.propostas.length; i++){
                    if(politico.propostas[i].id == null) inserts.push(politico.propostas[i]);
                    else {
                        updates.push(politico.propostas[i]);
                        ids.push(politico.propostas[i].id);
                    }
                }

                politico.propostas = ids;

                async.auto({
                    update:function(cb){
                        async.eachSeries(updates, function (doc, cb) {
                            Propostas.native(function(err, collection) {
                                if (err) return res.serverError(err);
                                collection.update({_id: new ObjectID(doc.id)},doc,cb);
                            });
                        });
                        cb();
                    },
                    insert:function(cb){
                        async.eachSeries(inserts, function (doc, cb) {
                            Propostas.create(doc).exec(function(err,q){
                                politico.propostas.push(q.id);
                                cb(err);
                            });
                        }, cb);

                        if(inserts.length == 0) cb();
                    }

                }, polUpdate);


            }
            else polUpdate();
        };

    },

    getById: function(req,res){
        var id = req.param('id');
        var ids = req.param('ids');

        if(!id && !ids) return res.json({error: "id inválido"});
        if(id && !ids) ids = [id];
        var objIds = [];
        for(var i = 0; i < ids.length; i++){
            try{
                objIds.push(new ObjectID(ids[i]));
            }
            catch(ex){ }
        }

        Politicos.find({_id: {$in: objIds}}).exec(function(err, politicos){
            if (err) res.json({error:err});
            else {
                if(ids.length == 1 && politicos[0]){
                    if(politicos[0].imagemurl && politicos[0].imagemurl.indexOf("Content/themes") > 0
                        && politicos[0].imagemurl.indexOf(process.env.OLD_URL) == -1)
                        politicos[0].imagemurl = "http://" + process.env.OLD_URL + politicos[0].imagemurl;
                    res.json(politicos[0]);
                }
                else res.json(politicos);
            }
        })
    },

    // alguma otimização de índices
    getByPoliticoUrl: function(req,res){
        initDb();
        var urlpolitico = req.param('urlpolitico');
        var cidade = req.param('cidade');
        var estado = req.param('estado');
        var perfil = req.param('perfil');
        var id = req.param('id');
        var email = req.param('email');
        var token = req.param('token');
        var gabinete = req.param('gabinete');

        var query = {};
        if(id) query._id = new ObjectID(id);
        else{
            query = { politicourl: urlpolitico };
            var and = []

            if(estado) query.estadocandidatura = estado.toUpperCase();
            else and.push({$or:[
                {estadocandidatura: ""},
                {estadocandidatura: {$exists: false}}
            ]});

            if(cidade != null && estado){

                cidade = removeDiacritics(cidade);
                var find = '-';
                var re = new RegExp(find, 'g');
                cidade = cidade.replace(re, ' ').toLowerCase();

                for(var id in cidadesJson[query.estado]){
                    if(cidade == removeDiacritics(cidadesJson[query.estado][id].nome.toLowerCase())){
                        query.cidadecandidatura = parseInt(id);
                        break;
                    }
                }
            }
            else and.push({$or:[
                {cidadecandidatura: -1},
                {cidadecandidatura: {$exists: false}}
            ]});

            if(and.length > 0) query.$and = and;
            and = null;
        }

        var result = function(err, politico) {
            if (err) res.json({error:err});
            else if(!politico) res.json({error:"Político inexistente"});
            else {

                async.auto({
                    questoes: function(cb){
                        var ids = [];
                        for(var id in politico.questoes){
                            if(id != "undefined"){
                                try{
                                    ids.push(new ObjectID(id));
                                }
                                catch(ex){}
                            }
                        }

                        Questoes.find({_id:{ $in:ids }}).exec(function(err, questoes){
                            var qs = []
                            for(var i = 0; i < questoes.length; i++){
                                if(!questoes[i].caducou)
                                    qs.push(questoes[i]);
                            }
                            ids = null;
                            cb('',qs);
                        });
                    },
                    propostas: function(cb){
                        var ids = _.map(politico.propostas,function(p){ return new ObjectID(p); })
                        Propostas.find({_id:{ $in:ids }}).exec(function(err, propostas){
                            ids = null;
                            cb('',propostas);
                            propostas = null;
                        });
                    },
                    vicesuplente: function(cb){
                        if((politico.cargocandidatura == "Presidente" || politico.cargocandidatura == "Governador" ||
                            politico.cargocandidatura == "Prefeito") && politico.viceid){

                            Politicos.native(function(err, collection) {
                                if (err) return res.serverError(err);
                                collection.findOne({ _id: new ObjectID(politico.viceid)},function(err,pol){
                                    if(pol!=null){
                                        pol.link = repoliticaUtils.buildPoliticoUrl(pol);
                                        if(!pol.imagemurl) pol.imagemurl = "/img/noimage.png";
                                        politico.vice = pol;
                                        pol = null;
                                    }
                                    cb();
                                });
                            });

                        }
                        else if(politico.cargocandidatura == "Senador" &&
                            (politico.suplente1id != null || politico.suplente2id != null)){
                            var ids = [];
                            if(politico.suplente1id) ids.push(new ObjectID(politico.suplente1id));
                            if(politico.suplente2id) ids.push(new ObjectID(politico.suplente2id));

                            Politicos.find({_id:{ $in:ids }}).exec(function(err,pols){
                                ids = null;
                                for(var i=0; i < pols.length; i++){
                                    var pol = pols[i];
                                    pols[i].link = repoliticaUtils.buildPoliticoUrl(pol);
                                    pol = null;
                                    if(!pols[i].imagemurl) pols[i].imagemurl = "/img/noimage.png";
                                    if(politico.suplente1id == pols[i].id) politico.suplente1 = pols[i];
                                    if(politico.suplente2id == pols[i].id) politico.suplente2 = pols[i];
                                }
                                cb();
                            });

                        }
                        else cb();
                    },
                    titulardovice: function(cb){
                        if(!perfil) return cb();

                        var query = {};
                        var cargo = politico.cargocandidatura || "";
                        if(cargo.indexOf("Suplente") != -1) cargo = "Suplente";
                        var cargoTitular = "";

                        switch(cargo){
                            case "Vice-presidente":
                            case "Vice-governador":
                            case "Vice-prefeito":
                                query.viceid = politico._id.toString();
                                cargoTitular = cargo.replace("Vice-" , "");
                                break;
                            case "Suplente":
                                query.or = [
                                    {suplente1id: politico._id.toString()},
                                    {suplente1id: politico._id.toString()}
                                ];
                                cargoTitular = "Senador";
                                break;
                            default:
                                return cb();
                                break;
                        }

                        Politicos.findOne(query).exec(function(err,pol){
                            if(err || pol == null) return cb();
                            politico.anocandidatura = pol.anocandidatura;
                            politico.numero = pol.numero;
                            politico.coligacao = pol.coligacao;
                            politico.titular = {
                                imagemurl: pol.imagemurl || "/img/noimage.png",
                                nomecandidatura: pol.nomecandidatura,
                                link: repoliticaUtils.buildPoliticoUrl(pol),
                                cargo: cargoTitular
                            };
                            pol = null;
                            cb();
                        });
                    },
                    votos: function(cb){
                        if(!perfil) return cb();

                        Votos.native(function(err, collection) {
                            if (err) return res.serverError(err);

                            collection.find({idpolitico: politico._id.toString() }).toArray(function(err, votos){
                                politico.ultvotos = [];
                                for(var i = votos.length - 1; i >= 0; i--){
                                    if(votos[i].ano == 2014 && votos[i].votos != null){
                                        var v = {};
                                        v.votos = repoliticaUtils.separadorMilhar(votos[i].votos);
                                        v.votosperc = repoliticaUtils.percentual(votos[i].percentual);
                                        if(votos[i].eleito) v.eleitostring = " - eleito";
                                        else if(votos[i].turno2) v.eleitostring = " - primeiro turno";
                                        else v.eleitostring = " - não eleito";
                                        politico.ultvotos.push(v);
                                    }
                                }
                                cb();
                            });
                        });
                    },
                    mudancas: function(cb){
                        if(!perfil) return cb();
                        Mudancas.native(function(err,collection){
                            collection.aggregate([
                                { "$match": { "politicoid": politico._id.toString() } },
                                { "$group": { "_id": '$usuarioid' } }
                            ],function(err,docs) {
                                if(err) return cb();
                                var ids = [];
                                for(var i = docs.length-1; i >= 0; i--){
                                    if(docs[i]._id == "admin") continue;
                                    ids.push(new ObjectID(docs[i]._id));
                                }

                                cb('', ids);
                            });
                        });
                    },
                    editores: ['mudancas', function(cb, r){
                        if(!r.mudancas) return cb();

                        Users.native(function(err, collection) {
                            if (err) return cb(err);
                            collection.find({_id:{ $in: r.mudancas }}).toArray(function(err, usuarios){
                                cb('',usuarios);
                            });

                        });
                    }]

                }, function (err, r) {
                    if(err) console.log(err);
                    if(r.editores){
                        politico.editores = [];

                        for(var i = 0; i < r.editores.length; i++){
                            if(!r.editores[i] || !r.editores[i].nome) continue;
                            r.editores[i].nome = r.editores[i].nome.trim();
                            var nomeArr = r.editores[i].nome.split(" ");
                            var nome = "";
                            if(nomeArr.length <=2)
                                nome = r.editores[i].nome;
                            else{
                                nome = nomeArr[0];
                                for(var j = 1; j < nomeArr.length - 1; j++){
                                    if(nomeArr[j].length > 3) nomeArr[j] = nomeArr[j].substring(0,1) + ".";
                                    nome += " " + nomeArr[j];
                                }
                                nome += " " + nomeArr[nomeArr.length - 1];
                            }

                            politico.editores.push({
                                nome: nome,
                                //email: r.editores[i].email
                            });
                            nomeArr = null;
                        }
                    }

                    if(politico.questoes){
                        var qs = {};
                        for(var i in politico.questoes){

                            for(var j = 0; j < r.questoes.length; j++){
                                var q = r.questoes[j];
                                if(i == q.id){
                                    politico.questoes[i].questao = q;
                                    qs[i] = politico.questoes[i];
                                    break;
                                }
                            }
                        }
                        politico.questoes = qs;

                    }

                    if(politico.propostas)
                        for(var i = 0; i < politico.propostas.length; i++){

                            for(var j = 0; j < r.propostas.length; j++){
                                var q = r.propostas[j];
                                if(politico.propostas[i] == q.id){
                                    politico.propostas[i] = q;
                                    break;
                                }
                            }

                        }

                    if(!politico.cidade) politico.cidade = -1;
                    if(!politico.naturalcidade) politico.naturalcidade = -1;
                    if(!politico.cidadecandidatura) politico.cidadecandidatura = -1;

                    if(politico.imagemurl && politico.imagemurl.indexOf("Content/themes") > 0
                        && politico.imagemurl.indexOf(process.env.OLD_URL) == -1)
                        politico.imagemurl = "http://" + process.env.OLD_URL + politico.imagemurl;

                    res.json(politico);
                });
            }
        };

        var polNative = function(){
            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);
                collection.findOne(query,function(err,pol){

                    var update = {$inc: { acessos: 1 }};

                    if(gabinete && !pol.gabinete){
                        update.$set = {
                            gabinete: gabinete,
                            dataCadastro: new Date()
                        };
                    }

                    if(perfil && pol){
                        collection.update({_id: new ObjectID(pol._id)}, update,
                            function(err,itens){});
                    }
                    result(err,pol);
                });
            });
        }

        if(!cidade && !estado && !id){
            Redirecionadores.findOne({urlpolitico : urlpolitico}).exec(function(err, red){
                if(red) query = { _id: new ObjectID(red.idpolitico)};
                polNative();
            });
        }
        else polNative()
    },

    // uso de cargocandidatura é raro
    buscaRapida: function(req, res){
        initDb();
        var busca = req.param('query');
        var limit = req.param('limit') || 5;
        var cargocandidatura = req.param('cargocandidatura');
        var estadocandidatura = req.param('estadocandidatura');

        if(!busca || busca.length < 3) return res.json({query:busca, suggestions:[]});

        var b = removeDiacritics("^" + busca).toLowerCase();
        var regB;
        try{
            regB = new RegExp(b);
        }
        catch(ex){
            regB = b;
        }

        var query = { textobusca: regB };
        if(estadocandidatura) query.estadocandidatura = estadocandidatura;
        if(cargocandidatura && cargocandidatura == "Suplente")
            query.cargocandidatura =  new RegExp(cargocandidatura);
        else if(cargocandidatura) query.cargocandidatura = cargocandidatura;

        Politicos.find(query).sort('pontos desc').limit(limit).exec(function(err, politicos){
            if(!politicos || politicos.length == 0) return res.json({query:busca, suggestions:[]});

            var pols = [];

            for(var i = 0; i < politicos.length; i++){
                var pol = {};
                var pTemp = politicos[i];
                pol.politicourl = repoliticaUtils.buildPoliticoUrl(pTemp);
                pTemp = null;
                pol.nome = politicos[i].nomecandidatura;
                pol.partido = politicos[i].partido;
                pol.estado = politicos[i].estado;
                pol.id = politicos[i].id;
                pol.imagemurl = politicos[i].imagemurl || "/img/noimage.png";
                if(pol.imagemurl && pol.imagemurl.indexOf("Content/themes") > 0
                    && pol.imagemurl.indexOf(process.env.OLD_URL) == -1)
                    pol.imagemurl = "http://" + process.env.OLD_URL + pol.imagemurl;

                pols.push(pol);

            }
            res.json({query:busca, suggestions:pols});
        });
    },

    coligacoes:function(req,res){
        var estado = req.param('estado');
        var cargo = req.param('cargo');
        if(!cargo || !estado) return res.json([]);
        Coligacoes.find({estado:estado, cargo:cargo}).exec(function(err, colgs){
            if(err) return res.json([]);
            res.json(colgs);
        })
    },

    // para o teste
    buscaSimples: function(req, res){
        initDb();
        var query = req.param('query');
        if(!query) return res.json([]);

        var ultTexto = req.param('ultTexto') || "";
        var limit = req.param('limit') || 250;
        var pols = [];

        var fimBusca = function(){
            res.json(pols);
            pols = null;
        }

        var buscarPols = function(){
            if(ultTexto) query.textobusca = {$gt:ultTexto};
            /*
            if(query.cargocandidatura == "Deputado Estadual") query.pontos = {$gt: 1413000};
            if(query.cargocandidatura == "Deputado Distrital") query.pontos = {$gt: 1413000};
            if(query.cargocandidatura == "Deputado Federal") query.pontos = {$gt: 1415000};
              */
            var fPol = function(err, politicos){
                if(!politicos || politicos.length == 0) fimBusca();
                else{
                    ultTexto = politicos[politicos.length-1].textobusca;
                    for(var i = 0; i < politicos.length; i++){
                        var pol = politicos[i];
                        politicos[i].politicolink = repoliticaUtils.buildPoliticoUrl(pol);
                        pol = null;
                        politicos[i].imagemurl = politicos[i].imagemurl || "/img/noimage.png";
                        if(politicos[i].imagemurl && politicos[i].imagemurl.indexOf("Content/themes") > 0
                            && politicos[i].imagemurl.indexOf(process.env.OLD_URL) == -1)
                            politicos[i].imagemurl = "http://" + process.env.OLD_URL + politicos[i].imagemurl;
                        //delete politicos[i].textobusca;
                        delete politicos[i].datacriacao;
                        delete politicos[i].tituloeleitor;
                        delete politicos[i].acessos;
                        delete politicos[i].dataCadastro;
                        delete politicos[i].pontos;
                        delete politicos[i].viceid;
                        delete politicos[i].suplente1id;
                        delete politicos[i].suplente2id;
                        delete politicos[i].coligacao;
                        delete politicos[i].partidos;
                        delete politicos[i].biografia;
                        delete politicos[i].propostas;
                        delete politicos[i].videos;
                        delete politicos[i].nomecompleto;
                        delete politicos[i].nascimentodia;
                        delete politicos[i].nascimentomes;
                        delete politicos[i].nascimentoano;
                        delete politicos[i].naturalcidade;
                        delete politicos[i].naturalestado;
                        delete politicos[i].estadocivil;
                        delete politicos[i].ocupacao;
                        delete politicos[i].escolaridade;
                        delete politicos[i].naomostrarnomesiguais;
                        delete politicos[i].politicianId;
                        delete politicos[i].updatedAt;
                        delete politicos[i].imagemurl2;
                    }
                    var cP = politicos.length;
                    pols = pols.concat(politicos);
                    politicos = null;

                    if(cP < limit || req.param('limit')) fimBusca();
                    else buscarPols();
                }
            };

            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);
                collection.find(query,{limit:limit}).sort({textobusca: 1}).toArray(fPol)
            });
        }
        buscarPols();
    },

    // busca para cadastro de políticos
    buscaCadastro: function(req,res){
        var estado = req.param('estado');
        var partido = req.param('partido');
        var numero = req.param('numero');
        var cargocandidatura = req.param('cargocandidatura');

        var query = {};
        if(estado) query.estadocandidatura = estado;
        if(partido) query.partidocandidatura = partido;
        if(numero) query.numero = parseInt(numero);
        if(cargocandidatura) query.cargocandidatura = cargocandidatura;

        Politicos.find(query).exec(function(err, pols){
            if(err) return res.json({error:err});

            if(pols.length > 1){
                // pegar o candidato com a candidatura mais recente
                var anoAtual = 0;
                var is = [];
                for(var i = 0; i < pols.length; i++){
                    if(pols[i].anocandidatura == anoAtual)
                        is.push(i);
                    else if(pols[i].anocandidatura > anoAtual){
                        anoAtual = pols[i].anocandidatura;
                        is = [i];
                    }
                }

                if(is.length == 1) pols = [pols[is[0]]];
            }

            if(pols.length == 1) {
                var p = pols[0];
                pols[0].link = repoliticaUtils.buildPoliticoUrl(p);
                p = null;
            }
            res.json(pols);
        })
    },

    ultimosCadastrados: function(req,res){
        var estado = req.param('estado') || "SP";
        estado = estado.toUpperCase();
        var limit = req.param('limit') || 5;
        if(limit <= 0) limit = 5;

        Politicos.native(function(err, collection) {
            if (err) return res.serverError(err);

            collection.find({gabinete: {$exists: true}, estadocandidatura: estado},{limit:limit}).sort({dataCadastro: -1}).toArray(function(err,politicos){

                for(var i = 0; i < politicos.length; i++){
                    if(politicos[i].imagemurl && politicos[i].imagemurl.indexOf("Content/themes") > 0
                        && politicos[i].imagemurl.indexOf(process.env.OLD_URL) == -1)
                        politicos[i].imagemurl = "http://" + process.env.OLD_URL + politicos[i].imagemurl;
                    var p = politicos[i];
                    politicos[i].url = repoliticaUtils.buildPoliticoUrl(p);
                    p = null;
                }

                res.json({err: err, politicos:politicos})

            })

        });


    },

    // métodos para o admin
    addalias:function(req,res){
        var alias = req.param("alias");
        var id = req.param("id");
        Redirecionadores.create({idpolitico:id, urlpolitico:alias}).exec(function(err, red) {
            if (err) res.json({error:err});
            else res.json(red);
        });
    },

    mudancas: function(req,res){

        isAuthenticated(req,function(obj){
            if(obj.err || !obj.user.isAdmin) return res.json({ err: "usuário inválido" });

            var data = req.param('data');
            var q = {};
            if(data != null) q.datacriacao = {$lt: new Date(data)};
            Mudancas.find(q).sort('datacriacao desc').limit(500).exec(function(err,ms){
                res.json(ms);
            });
        });

    },

    lista: function(req,res){
        var lista = req.param("lista");
        if(!lista) return res.json([]);
        Politicos.native(function(err,collection){
            if (err) return res.serverError(err);
            var ids = [];
            for(var i = 0; i < lista.length; i++){
                ids.push(new ObjectID(lista[i]));
            }

            collection.find({_id:{$in: ids}}).toArray(function (err, us) {
                ids = null;
                res.json(us);
            })
        });

    },

    remove: function(req,res){
        isAuthenticated(req,function(obj){
            if(obj.err || !obj.user.isAdmin) return res.json({ err: "usuário inválido" });

            var id = req.param('id');

            if(!id) return res.json({ err: "politico inválido" });

            Politicos.native(function(err,collection){
                collection.remove({ _id: new ObjectID(id), $isolated: 1 },function(err,n){
                    res.json(n);
                });
            });
        });

    },

    create: function(req, res){

        isAuthenticated(req,function(obj){
            if(obj.err || !obj.user.isAdmin) return res.json({ err: "usuário inválido" });

            var politico = req.param('politico');

            if(!politico) return res.json({ err: "politico inválido" });

            politico.datacriacao = new Date();
            politico.textobusca = politico.nomecandidatura + " " + (politico.nomecompleto || "");
            politico.textobusca = removeDiacritics(politico.textobusca.toLowerCase());

            Politicos.create(politico).exec(function(err, politico) {
                if (err) res.json({error:err});
                else res.json(politico);
            });
        });
    },

    conflitosAgrupados: function(req,res){
        isAuthenticated(req,function(obj){
            if(obj.err || !obj.user.isAdmin) return res.json({ err: "usuário inválido" });

            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);
                async.auto({
                    group1: function(cb){

                        collection.aggregate(
                            [
                                {
                                    $group: {
                                        _id : { estado: '$estadocandidatura', cidade: '$cidadecandidatura', politicourl: '$politicourl' },
                                        total : { $sum : 1 }
                                    }
                                },
                                {
                                    $match: { total: { $gt: 1 } }
                                },
                                { $limit: 20 }
                            ], function(err, result){
                                if (err) return console.log(err);
                                cb('',result);
                            }
                        );

                    },
                    group2: function(cb){

                        collection.aggregate(
                            [
                                {
                                    $group: {
                                        _id : { politicourl: '$politicourl' },
                                        total : { $sum : 1 }
                                    }
                                },
                                {
                                    $match: {
                                        total: { $gt: 1 },
                                        estadocandidatura: {$exists:false},
                                        cidadecandidatura: {$exists:false}
                                    }
                                },
                                { $limit: 20 }
                            ], function(err, result){
                                if (err) return console.log(err);
                                cb('',result);
                            }
                        );

                    },
                },function(err,r){
                    var pols = r.group1.concat(r.group2);
                    res.json(pols);
                });
            });
        });
    },

    nomesiguais: function(req,res){
        isAuthenticated(req,function(obj){
            if(obj.err || !obj.user.isAdmin) return res.json({ err: "usuário inválido" });

            var nome = req.param('nome');
            var estado = req.param('estado');

            if(!nome){
                Politicos.native(function(err, collection) {
                    if (err) return res.serverError(err);
                    collection.aggregate(
                        [
                            {
                                $match: {
                                    naomostrarnomesiguais: { $exists: false }
                                }
                            },
                            {
                                $group: {
                                    _id : {
                                        nomecompleto: '$nomecompleto',
                                        //estado: '$estado'
                                    },
                                    tituloeleitor: { $push: "$tituloeleitor" },
                                    total : { $sum : 1 }
                                }
                            },
                            {
                                $match: {
                                    //total: { $gt: 1 }
                                    total: 2
                                }
                            },
                            { $limit: 10000 }
                        ], function(err, result){
                            if (err) return console.log(err);

                            var nomes = [];
                            for(var i = 0; i < result.length; i++){

                                if(result[i].tituloeleitor[0] &&
                                    result[i].tituloeleitor[1] &&
                                    result[i].tituloeleitor[0] !=
                                    result[i].tituloeleitor[1])
                                continue;

                                var nome = result[i]._id.nomecompleto;
                                if(result[i]._id.estado)
                                    nome = result[i]._id.estado + "/" + nome;
                                nomes.push(
                                    {
                                        nome: nome,
                                        total: result[i].total
                                    }
                                );
                            }

                            res.json({err: err, nomes:nomes});
                        }
                    );
                });
            }
            else{

                Politicos.native(function(err, c) {

                    var query = { nomecompleto:nome, naomostrarnomesiguais:{$exists:false} };
                    if(estado) query.estado = estado;
                    //else query.estado = { $exists: false };

                    c.find(query).toArray(function(err,politicos){

                        for(var i = 0; i < politicos.length; i++){
                            if(politicos[i].imagemurl && politicos[i].imagemurl.indexOf("Content/themes") > 0
                                && politicos[i].imagemurl.indexOf(process.env.OLD_URL) == -1)
                                politicos[i].imagemurl = "http://" + process.env.OLD_URL + politicos[i].imagemurl;
                        }
                        res.json({err: err, politicos:politicos});
                    });
                })
            }
        });
    },

    semcidadenatal: function(req,res){
        isAuthenticated(req,function(obj){
            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);

                collection.find({naturalcidadestring:{$exists:true},
                        naturalcidade:{$exists:false}},
                    {limit: 40}).toArray(function(err,politicos){

                        for(var i = 0; i < politicos.length; i++){
                            if(politicos[i].imagemurl && politicos[i].imagemurl.indexOf("Content/themes") > 0
                                && politicos[i].imagemurl.indexOf(process.env.OLD_URL) == -1)
                                politicos[i].imagemurl = "http://" + process.env.OLD_URL + politicos[i].imagemurl;
                        }

                        res.json({err: err, politicos:politicos})

                    })

            });
        })
    },
    semestados: function(req,res){
        isAuthenticated(req,function(obj){
            if(obj.err || !obj.user.isAdmin) return res.json({ err: "usuário inválido" });

            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);
                var or = [
                    {cargo: "Governador"},
                    {cargo: "Senador"},
                    {cargo: "Vice-governador"},
                    {cargo: "Deputado Estadual"},
                    {cargo: "Deputado Federal"},
                    {cargo: "Deputado Distrital"},
                ];

                collection.find({estado:{$exists:false}, $or:or},
                    {limit: 300}).toArray(function(err,politicos){

                        for(var i = 0; i < politicos.length > 0; i++){
                            if(politicos[i].imagemurl && politicos[i].imagemurl.indexOf("Content/themes") > 0
                                && politicos[i].imagemurl.indexOf(process.env.OLD_URL) == -1)
                                politicos[i].imagemurl = "http://" + process.env.OLD_URL + politicos[i].imagemurl;
                        }

                        res.json({err: err, politicos:politicos})

                    })

            });
        });
    },

    conflitos: function(req,res){

        isAuthenticated(req,function(obj){
            if(obj.err || !obj.user.isAdmin) return res.json({ err: "usuário inválido" });

            var estado = req.param('estado');
            var cidade = req.param('cidade');
            var urlpolitico = req.param('urlpolitico');

            if(!urlpolitico) return res.json({});

            var query = { $and:[{politicourl: urlpolitico}] };
            if(!cidade) {
                query.$and.push({
                    $or:[
                        {cidadecandidatura: -1},
                        {cidadecandidatura:{$exists:false} },
                    ]
                });
            }
            else query.$and.push({cidadecandidatura:parseInt(cidade)});

            if(!estado) {
                query.$and.push({
                    $or:[
                        {estadocandidatura: -1},
                        {estadocandidatura:{$exists:false} },
                    ]
                });
            }
            else query.$and.push({estadocandidatura:estado});

            Politicos.native(function(err, collection) {
                if (err) return res.serverError(err);

                collection.find(query).toArray(function(err,politicos){

                    for(var i = 0; i < politicos.length > 0; i++){
                        if(politicos[i].imagemurl && politicos[i].imagemurl.indexOf("Content/themes") > 0
                            && politicos[i].imagemurl.indexOf(process.env.OLD_URL) == -1)
                            politicos[i].imagemurl = "http://" + process.env.OLD_URL + politicos[i].imagemurl;
                    }

                    res.json({err: err, politicos:politicos})

                })

            });
        });
    },

};
