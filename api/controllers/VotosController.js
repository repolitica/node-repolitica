/**
 * VotosController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling requests.
 */

module.exports = {


    idpolitico: function (req, res) {
        var id = req.param('id');
        if(!id) return res.json({});
        Votos.findOne({idpolitico:id}).exec(function(err,voto){
            res.json(voto);
        })
    }

};
