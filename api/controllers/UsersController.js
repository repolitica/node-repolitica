var ObjectID = require('../../node_modules/sails-mongo/node_modules/mongodb').ObjectID;
var auth = require('../utils/auth-utils');

module.exports = {

    lista: function(req,res){
        var lista = req.param("lista");
        if(!lista) return res.json([]);
        Users.native(function(err,collection){
            if (err) return res.serverError(err);
            var ids = [];
            for(var i = 0; i < lista.length; i++){
                ids.push(new ObjectID(lista[i]));
            }

            collection.find({_id:{$in: ids}}).toArray(function (err, us) {
                ids = null;
                res.json(us);
            })
        });

    },

    blacklist: function(req,res){
        BlackList.find().exec(function(err,emails){
            res.json(emails);
        })
    },

    addblacklist: function(req,res){
        var email = req.param("email");
        BlackList.create({email:email}).exec(function(err,bl){
            if(err) return res.json({error:err});
            if(!bl) return res.json({error:"blacklist não criado"});
            res.json();
        });
    },

    inicioTeste: function(req,res){
        var testeObj = req.param("teste");
        if(!testeObj) return res.json({error: "teste nao criado"});
        if(req.session != null && req.session.passport != null &&
            req.session.passport.user != null)
            testeObj.idUsuario = req.session.passport.user;
        testeObj.dataInicio = new Date();
        Testes.create(testeObj).exec(function(err,teste){
            if(err) return res.json({error:err});
            if(!teste) return res.json({error:"teste nao criado"});

            res.json({id:teste.id || teste._id});
        });

    },

    fimTeste: function(req,res){
        var testeObj = req.param("teste");
        if(!testeObj) return res.json({error: "teste vazio"});
        var id = testeObj.id;
        if(!id) return res.json({error: "teste vazio"});

        Testes.findOne({id:id}).exec(function(err,teste){
            if(err) return res.json({error:err});
            if(!teste) return res.json({error:"teste nao encontrado"});

            teste.respostas = testeObj.respostas;
            // salvar só o primeiro para economizar banco
            if(testeObj.resultado)
                teste.resultado = [testeObj.resultado[0]];
            testeObj = null;
            teste.save();
            res.json();
        });

    },

    compartilhouTeste: function(req,res){
        var testeObj = req.param("teste");
        if(!testeObj) return res.json({error: "teste vazio"});
        var id = testeObj.id;
        if(!id) return res.json({error: "teste vazio"});

        Testes.findOne({id:id}).exec(function(err,teste){
            if(err) return res.json({error:err});
            if(!teste) return res.json({error:"teste nao encontrado"});

            if(testeObj.facebook)
                teste.facebook = testeObj.facebook;
            if(testeObj.twitter)
                teste.twitter = testeObj.twitter;

            teste.save();
        });

    },

    consultaTeste: function(req,res){
        var data = req.param("data");

        var d0 = new Date(data.ano + "-" + data.mes + "-" + data.dia);
        var d1 = new Date(data.ano + "-" + data.mes + "-" + (parseInt(data.dia) + 1));

        Testes.native(function(err, collection) {
            if (err) return res.serverError(err);
            var q = {dataInicio: {$gt: d0, $lt: d1}};
            collection.find(q).toArray(function (err, testes) {
                var total = testes.length;
                // segundos
                var tempo = 0;
                var respostas = 0;
                var testesFinalizados = 0;
                var sharesFB = 0;
                var sharesTwitter = 0;
                var estados = {};
                var cargos = {};

                for(var i = 0; i < total; i++){
                    var teste = testes[i];
                    if(teste.respostas != null){
                        tempo += teste.updatedAt - teste.dataInicio;
                        testesFinalizados++;
                        for(var k in teste.respostas){
                            respostas++;
                        }

                        if(teste.facebook) sharesFB++;
                        if(teste.twitter) sharesTwitter++;
                    }

                    if(!estados[teste.estado]) estados[teste.estado] = 1;
                    else estados[teste.estado]++;

                    if(!cargos[teste.cargo]) cargos[teste.cargo] = 1;
                    else cargos[teste.cargo]++;
                }

                res.json({
                    data: d0,
                    total:total,
                    tempo: tempo,
                    respostas: respostas,
                    fim: testesFinalizados,
                    fb: sharesFB,
                    twitter: sharesTwitter,
                    estados: estados,
                    cargos: cargos
                });
            })
        });

    },

     /*
    login: function (req, res) {

        Users.findOneByEmail(req.body.email).exec(function (err, user) {
            if (err) res.json({ error: 'DB error' }, 500);

            if (user) {

                if (auth.authenticate(req.body.password, user.password)) {
                    // password match
                    req.session.user = user.id;
                    res.json(user);
                } else {
                    // invalid password
                    if (req.session.user) req.session.user = null;
                    res.json({ error: 'Invalid password' }, 400);
                }
            } else {
                res.json({ error: 'User not found' }, 404);
            }
        });
    }
          */

};