module.exports = {

    homepage: function(req, res, next){

        var url = require('url');
        var url_parts = url.parse(req.url, true);
        var query = url_parts.query;
        if (req.headers.host.indexOf('quemelegi.repolitica.com.br') >= 0) {
            return res.view('home/quemelegi',{
                image: "https://www.repolitica.com.br/img/compartilhamento-site.png",
                title: "Repolítica - Quem Elegi",
                sitename: "Repolítica - Quem Elegi",
                description: "Repolitica - Saiba quem você ajudou a eleger."
            });
        }

        if(query && query["_escaped_fragment_"] == '/teste')
            return res.view({
                image: "https://www.repolitica.com.br/img/compartilhamento-teste.jpg",
                title: " Teste de compatibilidade política",
                sitename: "Repolítica",
                description: "Responda 20 perguntas e descubra quais os candidatos mais compatíveis com você."
            });
        else
            return res.view({
                image: "https://www.repolitica.com.br/img/compartilhamento-site.png",
                title: "Repolítica",
                sitename: "Repolítica",
                description: "A maior enciclopédia colaborativa sobre políticos brasileiros. Encontre candidatos compatíveis com você, descubra o que cada um pensa sobre os temas mais polêmicos e pesquise tudo sobre os candidatos, desde vereadores do interior do Piauí a presidentes e ex-presidentes."
            });
    },

    quemelegi: function(req, res, next){

        var url = require('url');
        var url_parts = url.parse(req.url, true);
        var query = url_parts.query;
        return res.view({
            image: "https://www.repolitica.com.br/img/compartilhamento-site.png",
            title: "Repolítica - Quem Elegi",
            sitename: "Repolítica - Quem Elegi",
            description: "Repolitica - Saiba quem você ajudou a eleger."
        });
    }

};
