
var aws = require('../utils/aws');

exports.uploadFile = function (req, res) {

    var data = req.param('data')
    var file = req.param('file')
    var pol = req.param('politico')

    if (file != null) {
        aws.saveToCloud(pol, file, data, function (err, result) {
            res.json({ err: err, result: result });
        });
    }
    else {
        res.json({err:"no file sent"});
    }
};