var async = require('async');

module.exports = {
    create: function(req,res){
        res.json("forbidden");
    },

    home: function(req,res){

        Questoes.find({or: [ {idcidade: null,codigoestado:null},{idcidade: "",codigoestado:""} ]}).exec(function(err,qs){
            var questoes = [];

            for(var i = 0; i < qs.length; i++){
                if(!qs[i].caducou)  questoes.push(qs[i]);
            }

            res.json(questoes);
        });
    },

    tipo: function(req, res){
        var estado = req.param('estado');
        var cidade = req.param('cidade');
        var geral = req.param('geral');
        var geralfederal = req.param('geralfederal');
        if(!geral) geral = false;

        var query = {};
        query.$and = [];

        if(!estado){
            query.$and.push({
                $or: [
                    {codigoestado: ""},
                    {codigoestado: null}
                ]
            })
        }
        else query.$and.push({codigoestado: estado});

        if(!cidade){
            query.$and.push({
                $or: [
                    {idcidade: ""},
                    {idcidade: null}
                ]
            })
        }
        else query.$and.push({idcidade: cidade});

        if(!cidade && !estado){
            if(!geralfederal)
                query.$and.push({geral: geral});
        }

        Questoes.native(function(err, collection) {
            if (err) return res.serverError(err);
            collection.find(query).toArray(function(err,qs){
                var questoes = [];

                for(var i = 0; i < qs.length; i++){
                    if(!qs[i].caducou) questoes.push(qs[i]);
                }

                res.json(questoes);
            });
        });
    },

    teste: function(req, res){
        var estado = req.param('estado');

        async.auto({
            questoes: function(cback){
                Questoes.native(function(err, collection) {
                    if (err) return res.serverError(err);

                    var query = {codigoestado: {$exists: false}, idcidade: {$exists: false}};
                    if(estado) query.geral = true;

                    collection.find(query).toArray(
                        function(err,qs){
                            var questoes = [];

                            for(var i = 0; i < qs.length; i++){
                                if(!qs[i].caducou) questoes.push(qs[i]);
                            }
                            cback('', questoes);
                            questoes = null;
                            qs = null;
                        });
                });
            },
            federais: ['questoes',function(cb,r){
                var qs = [];
                if(!r.questoes) cb('',[]);
                else{
                    for(var i = 0; i < r.questoes.length; i++){
                        if(!r.questoes[i].geral) qs.push(r.questoes[i]);
                    }
                    cb('',qs);
                    qs = null;
                }
            }],
            gerais: ['questoes',function(cb,r){
                var qs = [];
                for(var i = 0; i < r.questoes.length; i++){
                    if(r.questoes[i].geral) qs.push(r.questoes[i]);
                }
                cb('',qs);
                qs = null;
            }],
            estaduais: function(cback){
                if(!estado) cback();
                else
                    Questoes.native(function(err, collection) {
                        if (err) return res.serverError(err);
                        collection.find({codigoestado: estado}).toArray(
                            function(err,qs){
                                var questoes = [];

                                for(var i = 0; i < qs.length; i++){
                                    if(!qs[i].caducou) questoes.push(qs[i]);
                                }

                                cback('', questoes);
                                questoes = null;
                                qs = null;
                            });
                    });
            },

        },function(err,r){

            /*
            var estaduais = [];
            if(r.estaduais && r.estaduais.length > 0){
                var nEst = 6;
                if(r.estaduais.length < nEst) nEst = r.estaduais.length;
                var es = _.range(r.estaduais.length);
                while(estaduais.length != nEst){
                    var selQ = _.random(0, es.length - 1);
                    var q = r.estaduais[es[selQ]];

                    estaduais.push(q);
                    es.splice(selQ,1);
                }
            }

            var federais = [];
            if(r.federais && r.federais.length > 0){
                var n = 6;
                if(r.federais.length < n) n = r.federais.length;
                var fs = _.range(r.federais.length);
                while(federais.length != n){
                    var selQ = _.random(0, fs.length - 1);
                    var q = r.federais[fs[selQ]];

                    federais.push(q);
                    fs.splice(selQ,1);
                }
            }

            var qs = federais.concat(estaduais);

            var nGerais = 12 - qs.length;

            if(nGerais < 0) nGerais = 0;
            if(nGerais > r.gerais.length) nGerais = r.gerais.length;

            var gerais = [];
            if(r.gerais && r.gerais.length > 0){
                var n = nGerais;
                if(r.gerais.length < n) n = r.federais.length;
                var es = _.range(r.gerais.length);
                while(gerais.length != n){
                    var selQ = _.random(0, es.length - 1);
                    var q = r.gerais[es[selQ]];

                    gerais.push(q);
                    es.splice(selQ,1);
                }
            }

            res.json(qs.concat(gerais));
             */

            var qs = [];
            if(r.estaduais && r.estaduais.length > 0){
                qs = r.estaduais;
                r.estaduais = null
            }
            if(r.federais && r.federais.length > 0){
                qs = qs.concat(r.federais);
                r.federais = null;
            }
            if(r.gerais && r.gerais.length > 0){
                qs = qs.concat(r.gerais);
                r.gerais = null
            }
            res.json(qs);
            qs = null;
            r = null;
        })
    },

    resultadoTeste: function(req, res){
        var estado = req.param('estado');
        var gerais = req.param('gerais');
        var federais = req.param('federais');

        Questoes.native(function(err, collection) {
            if (err) return res.serverError(err);
            var query = {};
            if(estado){
                if(gerais)
                    query.$or = [
                        {geral: true},
                        {codigoestado: estado},
                    ];
                else query.codigoestado = estado;
            }
            else if(federais){
                if(gerais)
                    query.codigoestado = {$exists: false};
                else{
                    query.codigoestado = {$exists: false};
                    query.geral = false;
                }
            }
            else query.geral = true;

            collection.find(query).toArray(function(err,qs){

                var federais = [];
                var gerais = [];
                var estaduais = {};
                estaduais[estado] = []

                for(var i = 0; i < qs.length; i++){
                    if(qs[i].caducou) continue;
                    if(qs[i].codigoestado) estaduais[estado].push(qs[i]);
                    else if(qs[i].geral) gerais.push(qs[i]);
                    else federais.push(qs[i]);
                }

                res.json({federais:federais, gerais: gerais, estaduais:estaduais});
                estaduais = null;
                federais = null;
                gerais = null;
                qs = null;
            });
        });
    },
}