var aws = require('../utils/aws'),
    email = require('../utils/email');
    //config = require('../../config/local');

module.exports = {
    enviarcontato:function(req,res){
        var nome = req.param('nome');
        var useremail = req.param('email');
        var mensagem = req.param('mensagem');

        if(!useremail || !email.validate(useremail))
            return res.send({error:"Insira um e-mail válido para contato"});

        var texto = "Contato de: <b>" + nome + "</b> (" + useremail + ")<br/><br/>Mensagem:<br/>" + mensagem;
        aws.sendAWSEmails('Repolitica <contato@repolitica.com.br>', [process.env.EMAIL_CONTATO],
            [],[],"Contato de: " + nome, texto, texto, function(error){
                if(error) {
                    console.log(error);
                    return res.send({error:"Ocorreu um erro inesperado"});
                }
                res.send({msg:"E-mail enviado"});
            })
    },
    novidadesquemelegi:function(req,res){
        var useremail = req.param('email');

        if(!useremail || !email.validate(useremail))
            return res.send({error:"Insira um e-mail válido"});

        var texto = "Este email quer receber novidades do Quem Elegi:<br/>" + useremail;
        aws.sendAWSEmails('Repolitica <contato@repolitica.com.br>', [process.env.EMAIL_CONTATO],
            [],[],"Novidades - Quem Elegi - " + useremail, texto, texto, function(error){
                if(error) {
                    console.log(error);
                    return res.send({error:"Ocorreu um erro inesperado"});
                }
                res.send({msg:"E-mail cadastrado"});
            })
    }
}