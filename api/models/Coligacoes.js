module.exports = {

    attributes: {

        nome: 'string',
        estado: 'string',
        cargo: 'string',
        partidos: 'array',
        ano: 'integer',

    }

};
