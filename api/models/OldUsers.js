
module.exports = {

    attributes: {

        isAdmin: 'boolean',
        imagemurl: 'string',
        nome: {
            type: 'string'
        },
        recebernovidades: 'boolean',
        emailvalidado: { type: 'boolean', defaultsTo:false },

        // para políticos
        politicoid: 'string',
        politicovalidado: 'boolean',
        telefone: 'string',

        tokenDataExpirado: {
            type: 'date'
        },
        token: {
            type: 'string'
        },

        // token de login usado no AP
        authToken: 'string',

        email: {
            type: 'string'
        },
        hashed_password: {
            type: 'string'
        },
        salt: {
            type: 'string'
        },
        toJSON: function() {
            var obj = this.toObject();
            delete obj.hashed_password;
            delete obj.salt;
            return obj;
        }
    },

};
