/**
 * Questoes
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

    attributes: {

        codigoestado: 'string',
        idcidade: 'string',
        titulo: 'string',
        // se eh federal ou geral
        geral: 'boolean',

        caducou: 'boolean',

    }

};
