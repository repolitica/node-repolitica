/**
 * Propostas
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

    attributes: {

        titulo: 'string',
        texto: 'string',

    }

};
