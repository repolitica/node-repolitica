/**
 * Politicos
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

// custom validation
// http://www.shanison.com/2014/07/11/sails-js-model-validation/

// todos os itens dos arrays deverão ter id próprio, caso não seja array de ids

module.exports = {

    attributes: {

        // lower de nomecandidatura + nomecompleto
        textobusca: { type: 'string' , required:true },
        politicourl: { type: 'string', required: true },
        datacriacao:{ type: 'date', required: true },
        tituloeleitor: 'string',
        acessos: 'integer',

        // AP
        gabinete: 'string',
        dataCadastro: 'date',

        // sistema de pontos para ordenação
        pontos: { type: 'integer'},

        // *************** Resumo

        // deverá ser unica
        imagemurl: 'string',
        nomecandidatura: {
            type: 'string',
            required: true
        },
        cargo: 'string',
        partido: { type: 'string', defaultsTo:""},
        //id
        // sem cidade => -1
        cidade: { type: 'integer', defaultsTo:-1 },
        // codigo
        estado: { type: 'string', defaultsTo:"" },

        // *************** candidatura

        // dados da última candidatura
        anocandidatura: 'integer',
        eleito: 'boolean',
        cargocandidatura: 'string',
        partidocandidatura: 'string',
        estadocandidatura: { type: 'string', defaultsTo:"" },
        cidadecandidatura:  { type: 'integer', defaultsTo:-1 },
        viceid: 'string',
        suplente1id: 'string',
        suplente2id: 'string',
        numero: 'integer',
        coligacao: 'string',

        // *************** Ideologia

        // consultar enum.json
        economia: 'integer',
        sociedade: 'integer',
        radicalismo: 'integer',
        // array de inteiros
        // consultar prioridades.json
        prioridades: 'array',

        // *************** Historico

        // { partido,anoinicio,anofim }
        partidos: 'array',
        biografia: 'string',

        // *************** Opinioes

        // {id:{resposta,justificativa}, ... }
        // resposta é um inteiro. Ver enum.json
        questoes: 'json',

        // *************** Atuacao

        urlexcelencias: 'string',
        projetosapresentados: 'integer',
        projetosirrelevantes: 'integer',
        presencas: 'integer',
        faltasnaojustificadas: 'integer',
        faltasjustificadas: 'integer',
        // string - html do transparencia
        ocorrencias: 'array',

        // *************** Avaliacoes

        // ids das avaliacoes
        avaliacoes: 'array',

        // *************** Propostas

        // ids
        propostas: 'array',

        // *************** Videos

        // {url,titulo}
        videos: 'array',

        // *************** Dados pessoais

        nomecompleto: 'string',
        nascimentodia: 'integer',
        nascimentomes: 'integer',
        nascimentoano: 'integer',
        // id de cidade
        naturalcidade: 'integer',
        // codigo do estado
        naturalestado: 'string',
        estadocivil: 'string',
        ocupacao: 'string',
        escolaridade: 'string',


        // *************** Para admin

        naomostrarnomesiguais: 'boolean'

    }

};
