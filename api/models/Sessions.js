

module.exports = {

    attributes: {

        session:{ type:'string', required: true },
        expires:{ type:'date', required: true }

    }

};