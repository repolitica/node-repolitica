/**
 * Users
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

var auth = require('../utils/auth-utils'),
    moment = require('moment'),
    generatePassword = require('password-generator'),
    globalConfig = require('../utils/globalconfig');

module.exports = {

    beforeValidate: function (attrs, next) {

        // garantir que seja único
        Users.findOne({
            email: attrs.email
        }).exec(function (err, user) {
            // Error handling
            if (err || user == null) {
                var pass = auth.encryptPassword(attrs.password);
                attrs.hashed_password = pass.encryptedPass;
                attrs.salt = pass.salt;
                delete attrs.password;

                // preparando para uma eventual validacao de email
                attrs.token = generatePassword(64, false, /\w/).toLowerCase();
                //attrs.tokenDataExpirado = moment(new Date()).add('days', globalConfig.VALIDADE_TOKEN);
                attrs.tokenDataExpirado = moment(new Date()).add('years', 1)._d;
                if(attrs.politicoid)
                    attrs.politicovalidado = true;

                next();
            } else {
                next();
            }
        });
    },

    schema: true,

    attributes: {

        isAdmin: 'boolean',
        imagemurl: 'string',
        nome: {
            type: 'string',
            required: true
        },
        recebernovidades: 'boolean',
        emailvalidado: { type: 'boolean', defaultsTo:false },

        // para políticos
        politicoid: 'string',
        politicovalidado: 'boolean',
        telefone: 'string',

        tokenDataExpirado: {
            type: 'date',
            required: true
        },
        token: {
            type: 'string',
            required: true
        },

        // token de login usado no AP
        authToken: 'string',

        email: {
            type: 'string',
            unique: true,
            required: true
        },
        hashed_password: {
            type: 'string',
            required: true
        },
        salt: {
            type: 'string'
        },
        toJSON: function() {
            var obj = this.toObject();
            delete obj.hashed_password;
            delete obj.salt;
            return obj;
        }
    },

};
