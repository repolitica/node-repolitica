

module.exports = {

    attributes: {

        idUsuario:{ type:'string' },

        dataInicio:{ type:'date' },
        dataFim:{ type:'date' },

        facebook: 'boolean',
        twitter: 'boolean',

        cargo: 'string',
        estado: 'string',

        respostas: 'json',

        resultado: 'array'
    }

};