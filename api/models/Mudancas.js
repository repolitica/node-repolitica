/**
 * Mudancas
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 *
 */

module.exports = {

    attributes: {

        politicoid:{ type:'string', required: true },
        usuarioid:{ type:'string', required: true },
        datacriacao:{ type:'date', required: true },
        nomepropriedade:{ type:'string', required: true },
        novovalor:{ type:'json', required: true },
        idarray:'string',
        // ver enum.json
        status:{ type:'integer', required: true },
        datasubstituido:{ type:'date' },
        // id da mudanca
        substituidopor:{ type:'date' },
        datarejeitado:{ type:'date' },
        dataaprovacao:{ type:'date' },

    }

};
