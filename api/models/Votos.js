

module.exports = {

    attributes: {

        idpolitico:{ type:'string' },

        ano:{ type:'integer' },
        turno:{ type:'integer' },

        votos: 'integer',
        votosnominais: 'integer',
        votoslegenda: 'integer',

        percentual: 'float',
        eleito: 'boolean',
        turno2: 'boolean',

        partido: 'string',
        cargo: 'string',
        estado: 'string',
    }

};